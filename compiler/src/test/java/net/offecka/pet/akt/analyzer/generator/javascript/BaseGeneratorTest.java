package net.offecka.pet.akt.analyzer.generator.javascript;

import net.offecka.pet.akt.analyzer.Analyzer;
import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.errors.ErrorDescription;
import net.offecka.pet.akt.generator.Unit;
import net.offecka.pet.akt.generator.javascript.DefaultJavaScriptGenerator;
import net.offecka.pet.akt.generator.javascript.DefaultJavaScriptGenerator.ReaderWriter;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;
import static net.offecka.pet.akt.analyzer.syntax.NameFactory.NAME_FACTORY;
import static net.offecka.pet.akt.errors.ErrorContext.errorContext;
import static net.offecka.pet.akt.errors.Location.NO_LOCATION;

public abstract class BaseGeneratorTest {
  protected ReaderWriter generate(final String fqn, final String path) throws IOException {
    return generate(new String[]{fqn, path}).iterator().next();
  }

  protected List<ReaderWriter> generate(final String... fqnPaths) throws IOException {
    List<ReaderWriter> rws = new ArrayList<>();

    if (fqnPaths.length % 2 != 0) {
      throw new IllegalArgumentException("Bad param count: " + fqnPaths.length);
    }

    Name entryPoint = null;
    Map<Name, ReaderWriter> map = new HashMap<>();
    for (int i = 0; i < fqnPaths.length; i += 2) {
      String fqn = fqnPaths[i];
      Name name = NAME_FACTORY.create(fqn, NO_LOCATION);

      String path = fqnPaths[i + 1];

      ReaderWriter rw = new ReaderWriter(
        new InputStreamReader(
          this.getClass().getClassLoader().getResourceAsStream(path), UTF_8
        ),
        new StringWriter()
      );

      rws.add(rw);
      map.put(name, rw);

      if (entryPoint == null) {
        entryPoint = name;
      }
    }

    try (
      Unit unit = new DefaultJavaScriptGenerator(entryPoint, map);
    ) {
      new Analyzer().compile(null, unit);
    }

    return rws;
  }

  protected String javascript(final String path) {
    String result = null;
    try (
      Scanner s = new Scanner(this.getClass().getClassLoader().getResourceAsStream(path), UTF_8);
    ) {
      result = s.useDelimiter("\\A").next();
    }
    return result;
  }

  protected List<String> messages() {
    return errorContext().errors().stream().map(ErrorDescription::formattedMessage).collect(Collectors.toList());
  }
}
