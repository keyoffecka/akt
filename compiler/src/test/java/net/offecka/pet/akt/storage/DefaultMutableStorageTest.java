package net.offecka.pet.akt.storage;

import net.offecka.pet.akt.analyzer.syntax.Function;
import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.analyzer.syntax.defaults.DefaultFunction;
import net.offecka.pet.akt.analyzer.syntax.defaults.PredefinedType;
import net.offecka.pet.akt.storage.exceptions.ExistedInChild;
import net.offecka.pet.akt.storage.exceptions.ExistsInStorage;
import net.offecka.pet.akt.storage.exceptions.NotFound;
import net.offecka.pet.akt.storage.exceptions.StorageException;
import org.testng.annotations.Test;

import static net.offecka.pet.akt.analyzer.syntax.NameFactory.NAME_FACTORY;
import static net.offecka.pet.akt.errors.Location.NO_LOCATION;
import static net.offecka.pet.akt.events.NoPublisher.NO_PUBLISHER;
import static net.offecka.pet.akt.storage.FinderType.LOCAL;
import static net.offecka.pet.akt.storage.Visibility.PRIVATE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class DefaultMutableStorageTest {
  @Test
  public void putShouldStoreItems() throws Exception {
    MutableStorage storage = new DefaultMutableStorage();

    Type type = new PredefinedType(storage, NAME_FACTORY.create("MyType", NO_LOCATION), NO_PUBLISHER);

    Function calc = new DefaultFunction(storage, NAME_FACTORY.create("calc", NO_LOCATION), type, null);

    storage.put(PRIVATE, calc.name(), calc);

    assertThat(storage.find(LOCAL, calc.name()).item).isSameAs(calc);
  }

  @Test
  public void findShouldReturnItem() throws StorageException {
    MutableStorage root = new DefaultMutableStorage();
    MutableStorage parent = root.create();
    MutableStorage child = parent.create();

    TestItem i1 = new TestItem();
    TestItem i2 = new TestItem();
    TestItem i3 = new TestItem();

    Name a = NAME_FACTORY.create("a", NO_LOCATION);
    Name b = NAME_FACTORY.create("b", NO_LOCATION);
    Name c = NAME_FACTORY.create("c", NO_LOCATION);

    root.put(PRIVATE, a, i1);
    parent.put(PRIVATE, b, i2);
    child.put(PRIVATE, c, i3);

    Item r1 = child.find(LOCAL, a).item(Item.class);
    Item r2 = child.find(LOCAL, b).item(Item.class);
    Item r3 = child.find(LOCAL, c).item(Item.class);

    assertThat(r1).isSameAs(i1);
    assertThat(r2).isSameAs(i2);
    assertThat(r3).isSameAs(i3);

    r1 = parent.find(LOCAL, a).item(Item.class);
    r2 = parent.find(LOCAL, b).item(Item.class);
    Throwable th1 = catchThrowable(() -> parent.find(LOCAL, c));

    assertThat(r1).isSameAs(i1);
    assertThat(r2).isSameAs(i2);
    assertThat(th1).isInstanceOf(NotFound.class).hasMessage("[c] not found");

    r1 = root.find(LOCAL, a).item(Item.class);
    Throwable th2 = catchThrowable(() -> root.find(LOCAL, b));
    th1 = catchThrowable(() -> root.find(LOCAL, c));

    assertThat(r1).isSameAs(i1);
    assertThat(th2).isInstanceOf(NotFound.class).hasMessage("[b] not found");
    assertThat(th1).isInstanceOf(NotFound.class).hasMessage("[c] not found");
  }

  @Test
  public void putShouldFailIfItemExists() throws StorageException {
    MutableStorage root = new DefaultMutableStorage();
    MutableStorage parent = root.create();
    MutableStorage child = parent.create();

    TestItem i1 = new TestItem();
    TestItem i2 = new TestItem();
    TestItem i3 = new TestItem();

    Name a = NAME_FACTORY.create("a", NO_LOCATION);
    Name b = NAME_FACTORY.create("b", NO_LOCATION);
    Name c = NAME_FACTORY.create("c", NO_LOCATION);

    root.put(PRIVATE, a, i1);
    parent.put(PRIVATE, b, i2);
    child.put(PRIVATE, c, i3);

    Throwable th1 = catchThrowable(() -> child.put(PRIVATE, c, i3));
    Throwable th2 = catchThrowable(() -> child.put(PRIVATE, b, i2));
    Throwable th3 = catchThrowable(() -> child.put(PRIVATE, a, i1));

    assertThat(th1).isInstanceOf(ExistsInStorage.class).hasMessage("[c] exists");
    assertThat(th2).isInstanceOf(ExistsInStorage.class).hasMessage("[b] exists");
    assertThat(th3).isInstanceOf(ExistsInStorage.class).hasMessage("[a] exists");

    th1 = catchThrowable(() -> parent.put(PRIVATE, c, i3));
    th2 = catchThrowable(() -> parent.put(PRIVATE, b, i2));
    th3 = catchThrowable(() -> parent.put(PRIVATE, a, i1));

    assertThat(th1).isInstanceOf(ExistedInChild.class).hasMessage("[c] exists");
    assertThat(th2).isInstanceOf(ExistsInStorage.class).hasMessage("[b] exists");
    assertThat(th3).isInstanceOf(ExistsInStorage.class).hasMessage("[a] exists");

    th1 = catchThrowable(() -> root.put(PRIVATE, c, i3));
    th2 = catchThrowable(() -> root.put(PRIVATE, b, i2));
    th3 = catchThrowable(() -> root.put(PRIVATE, a, i1));

    assertThat(th1).isInstanceOf(ExistedInChild.class).hasMessage("[c] exists");
    assertThat(th2).isInstanceOf(ExistedInChild.class).hasMessage("[b] exists");
    assertThat(th3).isInstanceOf(ExistsInStorage.class).hasMessage("[a] exists");
  }
}

class TestItem implements Item {}
