package net.offecka.pet.akt.analyzer.generator.javascript;

import net.offecka.pet.akt.generator.javascript.DefaultJavaScriptGenerator.ReaderWriter;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;

import static net.offecka.pet.akt.errors.ErrorContext.errorContext;
import static net.offecka.pet.akt.errors.ErrorContext.newErrorContext;
import static org.assertj.core.api.Assertions.assertThat;

public class GeneratorTest extends BaseGeneratorTest {
  @BeforeMethod
  public void setUp() {
    newErrorContext();
  }

  @Test
  public void testHelloWorld() throws Exception {
    ReaderWriter rw = generate("HelloWorld", "HelloWorld.akt");

    assertThat(errorContext().errors()).hasSize(0);

    assertThat(rw.writer.toString()).isEqualTo(javascript("HelloWorld.js"));
  }

  @Test
  public void testAny() throws Exception {
    ReaderWriter rw = generate("any", "any.akt");

    assertThat(errorContext().errors()).hasSize(0);

    assertThat(rw.writer.toString()).isEqualTo(javascript("any.js"));
  }

  @Test
  public void testImport() throws Exception {
    List<ReaderWriter> rws = generate(
      "net.offecka.pet.akt.examples.namespaceB", "net/offecka/pet/akt/examples/namespaceB.akt",
      "net.offecka.pet.akt.examples.namespaceA", "net/offecka/pet/akt/examples/namespaceA.akt"
    );

    assertThat(errorContext().errors()).hasSize(0);

    assertThat(rws.get(1).writer.toString()).isEqualTo(
      javascript("net/offecka/pet/akt/examples/namespaceA.js")
    );
    assertThat(rws.get(0).writer.toString()).isEqualTo(
      javascript("net/offecka/pet/akt/examples/namespaceB.js")
    );
  }

  @Test
  public void testVarShadow() throws IOException {
    generate("varshadow", "varshadow.akt");

    assertThat(messages()).containsExactlyInAnyOrder(
      "varshadow at 5:15 - [var1] exists at 21:1",
      "varshadow at 8:5 - [var2] exists at 11:3",
      "varshadow at 12:3 - [var1] exists at 21:1",
      "varshadow at 15:5 - [var2] exists at 11:3",
      "varshadow at 22:16 - [var1] exists at 21:1"
    );
  }

  @Test
  public void testReturns() throws IOException {
    generate("returns", "returns.akt");

    assertThat(messages()).containsExactlyInAnyOrder(
      "returns at 28:10 - [Str] cannot be cast to [Void]",
      "returns at 29:3 - Flow never reaches the statement",
      "returns at 34:3 - Flow never reaches the statement",
      "returns at 39:17 - [Str] cannot be cast to [Void]",
      "returns at 40:5 - Flow never reaches the statement",
      "returns at 47:5 - Flow never reaches the statement",
      "returns at 52:2 - [Void] cannot be cast to [Str]",
      "returns at 56:1 - [Void] cannot be cast to [Str]",
      "returns at 61:1 - [Void] cannot be cast to [Str]",
      "returns at 68:1 - [Void] cannot be cast to [Str]",
      "returns at 74:1 - [Void] cannot be cast to [Str]",
      "returns at 77:10 - [Int] cannot be cast to [Str]",
      "returns at 78:3 - Flow never reaches the statement",
      "returns at 83:3 - Flow never reaches the statement",
      "returns at 88:12 - [Int] cannot be cast to [Str]",
      "returns at 89:5 - Flow never reaches the statement",
      "returns at 97:5 - Flow never reaches the statement",
      "returns at 104:12 - [Int] cannot be cast to [Str]",
      "returns at 106:3 - Flow never reaches the statement",
      "returns at 113:3 - Flow never reaches the statement",
      "returns at 119:14 - [Int] cannot be cast to [Str]",
      "returns at 121:5 - Flow never reaches the statement",
      "returns at 131:5 - Flow never reaches the statement",
      "returns at 143:3 - Flow never reaches the statement",
      "returns at 152:5 - Flow never reaches the statement",
      "returns at 162:5 - Flow never reaches the statement",
      "returns at 179:3 - Flow never reaches the statement",
      "returns at 186:5 - Flow never reaches the statement"
    );
  }

  @Test
  public void testIf() throws IOException {
    ReaderWriter rws = generate("if", "if.akt");
    assertThat(errorContext().errors()).hasSize(0);

    assertThat(rws.writer.toString()).isEqualTo(
      javascript("if.js")
    );
  }

  @Test
  public void testIfErrors() throws IOException {
    generate("if-errors", "if-errors.akt");

    assertThat(messages()).containsExactlyInAnyOrder(
      "if-errors at 4:1 - [Void] cannot be cast to [Str]",
      "if-errors at 11:12 - [Str] cannot be cast to [Void]",
      "if-errors at 20:11 - [Void] cannot be cast to [Str]",
      "if-errors at 22:1 - [Void] cannot be cast to [Str]",
      "if-errors at 28:11 - [Void] cannot be cast to [Str]",
      "if-errors at 37:1 - [Void] cannot be cast to [Str]",
      "if-errors at 43:5 - Flow never reaches the statement",
      "if-errors at 54:5 - Flow never reaches the statement",
      "if-errors at 57:5 - Flow never reaches the statement",
      "if-errors at 69:3 - Flow never reaches the statement"
    );
  }

  @Test
  public void testNamespaceBlocks() throws IOException {
    ReaderWriter rws = generate("ns-blocks", "ns-blocks.akt");
    assertThat(errorContext().errors()).hasSize(0);

    assertThat(rws.writer.toString()).isEqualTo(
      javascript("ns-blocks.js")
    );
  }

  @Test
  public void testNamespaceErrors() throws IOException {
    generate("ns-errors", "ns-errors.akt");

    assertThat(messages()).containsExactlyInAnyOrder(
      "ns-errors at 2:9 - [Void] cannot be cast to [Nothing]",
      "ns-errors at 3:10 - [Str] cannot be cast to [Nothing]",
      "ns-errors at 3:3 - Flow never reaches the statement",
      "ns-errors at 6:10 - [Str] cannot be cast to [Nothing]",
      "ns-errors at 7:9 - [Void] cannot be cast to [Nothing]",
      "ns-errors at 7:3 - Flow never reaches the statement",
      "ns-errors at 9:9 - [Void] cannot be cast to [Nothing]",
      "ns-errors at 10:10 - [Str] cannot be cast to [Nothing]",
      "ns-errors at 10:3 - Flow never reaches the statement",
      "ns-errors at 12:9 - [Void] cannot be cast to [Nothing]",
      "ns-errors at 13:10 - [Str] cannot be cast to [Nothing]",
      "ns-errors at 13:3 - Flow never reaches the statement"
    );
  }

  @Test
  public void testGarbage() throws IOException {
    generate("garbage", "garbage.akt");

    assertThat(messages()).containsExactlyInAnyOrder(
      "garbage at 1:1 - Unrecognized content"
    );
  }

  @Test
  public void testWithGarbage() throws IOException {
    generate("with-garbage", "with-garbage.akt");

    assertThat(messages()).containsExactlyInAnyOrder(
      "with-garbage at 3:1 - Unrecognized content"
    );
  }

  @Test
  public void testNothing() throws IOException {
    generate("nothing", "nothing.akt");

    assertThat(messages()).containsExactlyInAnyOrder(
      "nothing at 3:8 - [Nothing] not found",
      "nothing at 4:8 - [Nothing] not found"
    );
  }

  @Test
  public void testLambda() throws IOException {
    generate("lambda", "lambda.akt");

    assertThat(messages()).containsExactlyInAnyOrder(
    );
  }
}
