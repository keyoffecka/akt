package net.offecka.pet.akt.expression;

import net.offecka.pet.akt.expression.OperatorDescriptor.OperatorDescriptors;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static net.offecka.pet.akt.expression.ExpressionNodeTest.ExpressionBuilder.exp;
import static org.assertj.core.api.Assertions.assertThat;

public class ExpressionNodeTest {

  @Test
  public void testExpression() throws CannotEvaluate {
    Expression e = exp()
      .not().o().n(5).add().o().n(2).add().n(3).c().mul().n(4).c().is().n(25).bitXor().n(-1L)
      .and().n(5).notEqualsTo().n(4)
      .build();

    Bool b = (Bool) e.evaluate();

    assertThat(b.bool).isTrue();

    e = exp()
      .n(6).div().n(2).mul().o().n(1).add().n(2).c()
      .build();

    Number n = (Number) e.evaluate();

    assertThat(n.number).isEqualTo(9);

    e = exp()
      .not().o().n(5).add().o().n(2).add().n(3).c().mul().n(4).c() // !(5+(2+3)*4)
      .build();

    n = (Number) e.evaluate();

    assertThat(n.number).isEqualTo(0xFFFF_FFFF_FFFF_FFE6L);
  }

  public static class ExpressionBuilder {
    private final List<Element> elements = new ArrayList<>();

    public static ExpressionBuilder exp() {
      return new ExpressionBuilder();
    }

    private ExpressionBuilder() {
    }

    public Expression build() {
      return new Expression(elements);
    }

    public ExpressionBuilder o() {
      elements.add(new Op(OperatorDescriptors.OPEN));
      return this;
    }

    public ExpressionBuilder c() {
      elements.add(new Op(OperatorDescriptors.CLOSE));
      return this;
    }

    public ExpressionBuilder n(final long number) {
      elements.add(new Number(number));
      return this;
    }

    public ExpressionBuilder b(final boolean bool) {
      elements.add(new Bool(bool));
      return this;
    }

    public ExpressionBuilder not() {
      elements.add(new Op(OperatorDescriptors.NOT));
      return this;
    }

    public ExpressionBuilder plus() {
      elements.add(new Op(OperatorDescriptors.PLUS));
      return this;
    }

    public ExpressionBuilder minus() {
      elements.add(new Op(OperatorDescriptors.MINUS));
      return this;
    }

    public ExpressionBuilder mul() {
      elements.add(new Op(OperatorDescriptors.MUL));
      return this;
    }

    public ExpressionBuilder div() {
      elements.add(new Op(OperatorDescriptors.DIV));
      return this;
    }

    public ExpressionBuilder mod() {
      elements.add(new Op(OperatorDescriptors.MOD));
      return this;
    }

    public ExpressionBuilder add() {
      elements.add(new Op(OperatorDescriptors.ADD));
      return this;
    }

    public ExpressionBuilder sub() {
      elements.add(new Op(OperatorDescriptors.SUB));
      return this;
    }

    public ExpressionBuilder bitAnd() {
      elements.add(new Op(OperatorDescriptors.BIT_AND));
      return this;
    }

    public ExpressionBuilder bitOr() {
      elements.add(new Op(OperatorDescriptors.BIT_OR));
      return this;
    }

    public ExpressionBuilder bitXor() {
      elements.add(new Op(OperatorDescriptors.BIT_XOR));
      return this;
    }

    public ExpressionBuilder and() {
      elements.add(new Op(OperatorDescriptors.AND));
      return this;
    }

    public ExpressionBuilder or() {
      elements.add(new Op(OperatorDescriptors.OR));
      return this;
    }

    public ExpressionBuilder equalsTo() {
      elements.add(new Op(OperatorDescriptors.EQUALS_TO));
      return this;
    }

    public ExpressionBuilder notEqualsTo() {
      elements.add(new Op(OperatorDescriptors.NOT_EQUALS_TO));
      return this;
    }

    public ExpressionBuilder is() {
      elements.add(new Op(OperatorDescriptors.IS));
      return this;
    }

    public ExpressionBuilder isNot() {
      elements.add(new Op(OperatorDescriptors.IS_NOT));
      return this;
    }
  }

  private static class Op implements Operator {
    private final OperatorDescriptor descriptor;

    private Op(final OperatorDescriptor descriptor) {
      this.descriptor = descriptor;
    }

    @Override
    public OperatorDescriptor descriptor() {
      return descriptor;
    }

    @Override
    public Operand evaluate(final List<Operand> operands) {
      Operand last = operands.remove(0);

      Number number = null;
      Bool bool = null;

      if (last instanceof Number) {
        number = (Number) last;
      } else {
        bool = (Bool) last;
      }

      if (descriptor() == OperatorDescriptors.NOT) {
        return new Number(~number.number);
      } else if (descriptor() == OperatorDescriptors.PLUS) {
        return new Number(+number.number);
      } else if (descriptor() == OperatorDescriptors.MINUS) {
        return new Number(-number.number);
      } else if (descriptor() == OperatorDescriptors.MUL) {
        return new Number(((Number) operands.remove(0)).number * number.number);
      } else if (descriptor() == OperatorDescriptors.DIV) {
        return new Number(((Number) operands.remove(0)).number / number.number);
      } else if (descriptor() == OperatorDescriptors.MOD) {
        return new Number(((Number) operands.remove(0)).number % number.number);
      } else if (descriptor() == OperatorDescriptors.ADD) {
        return new Number(((Number) operands.remove(0)).number + number.number);
      } else if (descriptor() == OperatorDescriptors.SUB) {
        return new Number(((Number) operands.remove(0)).number - number.number);
      } else if (descriptor() == OperatorDescriptors.BIT_AND) {
        return new Number(((Number) operands.remove(0)).number & number.number);
      } else if (descriptor() == OperatorDescriptors.BIT_OR) {
        return new Number(((Number) operands.remove(0)).number | number.number);
      } else if (descriptor() == OperatorDescriptors.BIT_XOR) {
        return new Number(((Number) operands.remove(0)).number ^ number.number);
      } else if (descriptor() == OperatorDescriptors.EQUALS_TO) {
        return new Bool(((Long) (((Number) operands.remove(0)).number)).equals(number.number));
      } else if (descriptor() == OperatorDescriptors.NOT_EQUALS_TO) {
        return new Bool(!((Long) (((Number) operands.remove(0)).number)).equals(number.number));
      } else if (descriptor() == OperatorDescriptors.IS) {
        return new Bool(((Number) operands.remove(0)).number == number.number);
      } else if (descriptor() == OperatorDescriptors.IS_NOT) {
        return new Bool(((Number) operands.remove(0)).number != number.number);
      } else if (descriptor() == OperatorDescriptors.AND) {
        return new Bool(((Bool) operands.remove(0)).bool && bool.bool);
      } else if (descriptor() == OperatorDescriptors.OR) {
        return new Bool(((Bool) operands.remove(0)).bool || bool.bool);
      } else {
        throw new IllegalArgumentException();
      }
    }
  }

  private static class Number implements Operand {

    private final long number;

    private Number(final long number) {
      this.number = number;
    }

    @Override
    public String toString() {
      return "" + number;
    }
  }

  private static class Bool implements Operand {

    private final boolean bool;

    private Bool(final boolean bool) {
      this.bool = bool;
    }

    @Override
    public String toString() {
      return ((Boolean) bool).toString();
    }
  }
}
