package net.offecka.pet.akt.analyzer.syntax;

import net.offecka.pet.akt.errors.Location;
import org.testng.annotations.Test;

import static net.offecka.pet.akt.analyzer.syntax.NameFactory.NAME_FACTORY;
import static net.offecka.pet.akt.errors.Location.NO_LOCATION;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class NameTest {
  
  @Test
  public void testName() {
    Location l = NO_LOCATION;
    
    assertThat(catchThrowable(() -> NAME_FACTORY.create(null, l))).isInstanceOf(IllegalArgumentException.class).hasMessage(null);

    assertThat(catchThrowable(() -> NAME_FACTORY.create("", l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name []");
    assertThat(catchThrowable(() -> NAME_FACTORY.create(" ", l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [ ]");
    assertThat(catchThrowable(() -> NAME_FACTORY.create(" a", l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [ a]");
    assertThat(catchThrowable(() -> NAME_FACTORY.create("a ", l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [a ]");
    assertThat(catchThrowable(() -> NAME_FACTORY.create(" a ", l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [ a ]");

    assertThat(catchThrowable(() -> NAME_FACTORY.create(".", l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [.]");
    assertThat(catchThrowable(() -> NAME_FACTORY.create(". ", l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [. ]");
    assertThat(catchThrowable(() -> NAME_FACTORY.create(". a", l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [. a]");
    assertThat(catchThrowable(() -> NAME_FACTORY.create(".a ", l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [.a ]");
    assertThat(catchThrowable(() -> NAME_FACTORY.create(". a ", l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [. a ]");

    assertThat(catchThrowable(() -> NAME_FACTORY.create(" .",  l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [ .]");
    assertThat(catchThrowable(() -> NAME_FACTORY.create(" . ",  l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [ . ]");
    assertThat(catchThrowable(() -> NAME_FACTORY.create(" . a",  l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [ . a]");
    assertThat(catchThrowable(() -> NAME_FACTORY.create(" .a ",  l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [ .a ]");
    assertThat(catchThrowable(() -> NAME_FACTORY.create(" . a ",  l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [ . a ]");

    assertThat(catchThrowable(() -> NAME_FACTORY.create(" a.",  l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [ a.]");
    assertThat(catchThrowable(() -> NAME_FACTORY.create(" a. ",  l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [ a. ]");
    assertThat(catchThrowable(() -> NAME_FACTORY.create(" a. a",  l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [ a. a]");
    assertThat(catchThrowable(() -> NAME_FACTORY.create(" a.a ",  l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [ a.a ]");
    assertThat(catchThrowable(() -> NAME_FACTORY.create(" a. a ",  l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [ a. a ]");

    assertThat(catchThrowable(() -> NAME_FACTORY.create("a .",  l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [a .]");
    assertThat(catchThrowable(() -> NAME_FACTORY.create("a . ",  l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [a . ]");
    assertThat(catchThrowable(() -> NAME_FACTORY.create("a . a",  l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [a . a]");
    assertThat(catchThrowable(() -> NAME_FACTORY.create("a .a ",  l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [a .a ]");
    assertThat(catchThrowable(() -> NAME_FACTORY.create("a. a ",  l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [a. a ]");

    assertThat(catchThrowable(() -> NAME_FACTORY.create(" a .",  l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [ a .]");
    assertThat(catchThrowable(() -> NAME_FACTORY.create(" a . ",  l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [ a . ]");
    assertThat(catchThrowable(() -> NAME_FACTORY.create(" a . a",  l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [ a . a]");
    assertThat(catchThrowable(() -> NAME_FACTORY.create(" a .a ",  l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [ a .a ]");
    assertThat(catchThrowable(() -> NAME_FACTORY.create(" a . a ",  l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [ a . a ]");

    assertThat(catchThrowable(() -> NAME_FACTORY.create("a.",  l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [a.]");
    assertThat(catchThrowable(() -> NAME_FACTORY.create("a. ",  l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [a. ]");
    assertThat(catchThrowable(() -> NAME_FACTORY.create("a. a",  l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [a. a]");
    assertThat(catchThrowable(() -> NAME_FACTORY.create("a.a ",  l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [a.a ]");
    assertThat(catchThrowable(() -> NAME_FACTORY.create("a. a ",  l))).isInstanceOf(IllegalArgumentException.class).hasMessage("Invalid name [a. a ]");

    Name n = NAME_FACTORY.create("a",  l);
    assertThat(n.name).isEqualTo("a");
    assertThat(n.child).isNull();
    assertThat(n.toString()).isEqualTo("a");
    assertThat(n).isEqualTo(NAME_FACTORY.create("a",  l));
    assertThat(n).isNotEqualTo(NAME_FACTORY.create("b",  l));
    assertThat(n).isNotEqualTo(NAME_FACTORY.create("a.b",  l));

    n = NAME_FACTORY.create("a.b",  l);
    assertThat(n.name).isEqualTo("a");
    assertThat(n.child.name).isEqualTo("b");
    assertThat(n.child.child).isNull();
    assertThat(n.toString()).isEqualTo("a.b");
    assertThat(n).isEqualTo(NAME_FACTORY.create("a.b",  l));
    assertThat(n).isNotEqualTo(NAME_FACTORY.create("b.a",  l));
    assertThat(n).isNotEqualTo(NAME_FACTORY.create("a.b.c",  l));

    n = NAME_FACTORY.create("a.b.c",  l);
    assertThat(n.name).isEqualTo("a");
    assertThat(n.child.name).isEqualTo("b");
    assertThat(n.child.child.name).isEqualTo("c");
    assertThat(n.child.child.child).isNull();
    assertThat(n.toString()).isEqualTo("a.b.c");
    assertThat(n).isEqualTo(NAME_FACTORY.create("a.b.c",  l));
    assertThat(n).isNotEqualTo(NAME_FACTORY.create("c.b.a",  l));
    assertThat(n).isNotEqualTo(NAME_FACTORY.create("a.b.c.d",  l));
  }
}
