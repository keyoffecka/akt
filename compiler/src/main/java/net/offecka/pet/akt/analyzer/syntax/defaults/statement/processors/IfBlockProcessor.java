package net.offecka.pet.akt.analyzer.syntax.defaults.statement.processors;

import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.StatementDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginIfEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.result.BlockStatementResultBatch;
import net.offecka.pet.akt.errors.Location;
import net.offecka.pet.akt.events.EventPublisher;
import net.offecka.pet.akt.expression.Expression;
import net.offecka.pet.akt.storage.MutableStorage;

import java.util.List;

public class IfBlockProcessor extends IfElseBlockProcessor {
  public IfBlockProcessor(
    final Location startLocation,
    final Location endLocation,
    final Expression expression,
    final List<StatementDetails> statementDetailsList,
    final BlockEventFactory blockEventFactory,
    final EventPublisher publisher
  ) {
    super(startLocation, endLocation, expression, statementDetailsList, blockEventFactory, publisher);
  }

  @Override
  public BlockStatementResultBatch process(final MutableStorage storage) {
    publisher.publish(new BeginIfEvent());

    return super.process(storage);
  }
}
