package net.offecka.pet.akt.generator.context;

public abstract class Context {
  public final Context parent;

  protected Context(final Context parent) {
    this.parent = parent;
  }
}
