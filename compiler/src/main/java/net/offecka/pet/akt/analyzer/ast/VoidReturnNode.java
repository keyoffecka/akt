package net.offecka.pet.akt.analyzer.ast;

import net.offecka.pet.akt.errors.Location;

public class VoidReturnNode extends TermNode {
  public final Location endLocation;

  public VoidReturnNode(final Location location, final Location endLocation) {
    super(location);

    this.endLocation = endLocation;
  }
}
