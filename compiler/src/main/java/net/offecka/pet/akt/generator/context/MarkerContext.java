package net.offecka.pet.akt.generator.context;

public class MarkerContext extends Context {
  public enum Markers {
    NEGATE
  }

  public final Markers marker;

  protected MarkerContext(final Markers marker, final Context parent) {
    super(parent);
    this.marker = marker;
  }
}
