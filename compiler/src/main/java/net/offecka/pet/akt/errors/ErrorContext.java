package net.offecka.pet.akt.errors;

import net.offecka.pet.akt.generator.Unit;

import java.util.ArrayList;
import java.util.List;

import static java.util.List.copyOf;

public final class ErrorContext {
  private static ErrorContext ERROR_CONTEXT = new ErrorContext();

  public static ErrorContext errorContext() {
    return ERROR_CONTEXT;
  }

  public static ErrorContext newErrorContext() {
    ERROR_CONTEXT = new ErrorContext();
    return ERROR_CONTEXT;
  }

  private final List<ErrorDescription> errors = new ArrayList<>();

  private final List<Unit> units = new ArrayList<>();

  private ErrorContext() {
  }

  public void begin(final Unit unit) {
    units.add(unit);
  }

  public void end() {
    units.remove(units.size() - 1);
  }

  public Unit unit() {
    return units.get(units.size() - 1);
  }

  public List<ErrorDescription> errors() {
    return copyOf(errors);
  }

  public void addError(final String message, final Location location) {
    errors.add(new ErrorDescription(message, location, null));
  }

  public void addError(final Location location, final Exception ex) {
    errors.add(new ErrorDescription(ex.getMessage(), location, ex));
  }

  public <E extends Exception> void addException(final Location location, final E ex) {
    try {
      throw ex;
    } catch (final Exception e) {
      errors.add(new ErrorDescription(ex.getMessage(), location, ex));
    }
  }

  public void addError(final String message, final Location location, final Exception ex) {
    errors.add(new ErrorDescription(message, location, ex));
  }
}
