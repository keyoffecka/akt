package net.offecka.pet.akt.analyzer.syntax.defaults.statement.processors;

import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.StatementDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginBlockEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndBlockEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.initializers.StatementInitializer;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.result.BlockStatementResultBatch;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.result.StatementResultBatchAccumulator;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.result.StatementResultBatchFactory;
import net.offecka.pet.akt.errors.Location;
import net.offecka.pet.akt.events.Event;
import net.offecka.pet.akt.events.EventPublisher;
import net.offecka.pet.akt.storage.MutableStorage;

import java.util.List;

import static java.util.List.copyOf;

public class BlockProcessor extends StatementProcessor {
  private final Location startLocation;
  private final Location endLocation;
  private final List<StatementDetails> statementDetailsList;
  private final StatementResultBatchFactory statementResultBatchFactory;
  private final BlockEventFactory blockEventFactory;

  public BlockProcessor(
    final Location startLocation,
    final Location endLocation,
    final List<StatementDetails> statementDetailsList,
    final BlockEventFactory blockEventFactory,
    final EventPublisher publisher
  ) {
    super(publisher);

    this.blockEventFactory = blockEventFactory;
    this.startLocation = startLocation;
    this.endLocation = endLocation;
    this.statementDetailsList = copyOf(statementDetailsList);

    statementResultBatchFactory = new StatementResultBatchFactory();
  }

  @Override
  public BlockStatementResultBatch process(final MutableStorage storage) {
    publisher.publish(blockEventFactory.createBeginBlockEvent());

    StatementInitializer statementInitializer = new StatementInitializer(
      storage.create(), publisher
    );

    StatementResultBatchAccumulator statementResultBatchAccumulator = new StatementResultBatchAccumulator();
    for (final StatementDetails statementDetails : statementDetailsList) {
      BlockStatementResultBatch blockStatementResultBatch = statementInitializer.init(statementDetails);
      statementResultBatchAccumulator.accumulate(blockStatementResultBatch);
    }

    publisher.publish(blockEventFactory.createEndBlockEvent());

    BlockStatementResultBatch blockStatementResultBatch = null;
    if (statementResultBatchAccumulator.lastReachableStatementResultBatch() == null) {
      blockStatementResultBatch = new BlockStatementResultBatch(startLocation)
        .add(null, endLocation, false);
    } else {
      blockStatementResultBatch = statementResultBatchFactory.create(
        statementResultBatchAccumulator.lastReachableStatementResultBatch().startLocation(),
        statementResultBatchAccumulator.lastReachableStatementResultBatch().blockStatementResults(),
        statementResultBatchAccumulator.hadUnreachableStatements()
      );
    }

    return blockStatementResultBatch;
  }

  public static class BlockEventFactory {
    public Event createBeginBlockEvent() {
      return new BeginBlockEvent();
    }
    public Event createEndBlockEvent() {
      return new EndBlockEvent();
    }
  }
}
