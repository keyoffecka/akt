package net.offecka.pet.akt.analyzer.syntax.defaults.details.statement;

import net.offecka.pet.akt.analyzer.syntax.defaults.support.ExpressionProvider;

public interface ExpressionStatementDetails extends StatementDetails, WithExpressionLocation {
  ExpressionProvider expressionProvider();
}
