package net.offecka.pet.akt.analyzer.syntax.defaults.statement.processors;

import net.offecka.pet.akt.analyzer.syntax.Call;
import net.offecka.pet.akt.analyzer.syntax.Function;
import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.Parameter;
import net.offecka.pet.akt.analyzer.syntax.Parameters;
import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginCallEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginChainEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginParameterEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndCallEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndChainEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndParameterEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.NameEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.OperatorEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.ValueEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.exceptions.CannotCast;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.exceptions.ParameterRequired;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.exceptions.TooManyParameters;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.operands.ChainOperand;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.operands.StatementOperand;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.operands.ValueOperand;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.operator.StatementOperator;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.result.BlockStatementResultBatch;
import net.offecka.pet.akt.events.EventPublisher;
import net.offecka.pet.akt.expression.CannotEvaluate;
import net.offecka.pet.akt.expression.Element;
import net.offecka.pet.akt.expression.Expression;
import net.offecka.pet.akt.storage.MutableStorage;
import net.offecka.pet.akt.storage.Storage;
import net.offecka.pet.akt.storage.exceptions.NotFound;
import net.offecka.pet.akt.storage.exceptions.StorageException;
import net.offecka.pet.akt.storage.exceptions.UnexpectedType;

import java.util.List;

import static net.offecka.pet.akt.analyzer.syntax.NameFactory.NAME_FACTORY;
import static net.offecka.pet.akt.errors.ErrorContext.errorContext;
import static net.offecka.pet.akt.storage.FinderType.LOCAL;

public abstract class StatementProcessor {
  protected final EventPublisher publisher;

  public StatementProcessor(final EventPublisher publisher) {
    this.publisher = publisher;
  }

  public abstract BlockStatementResultBatch process(final MutableStorage storage);

  protected void publishEvents(final List<Element> elements, final Storage storage) {
    for (final Element element : elements) {
      if (element instanceof ValueOperand) {
        ValueOperand operand = (ValueOperand) element;
        publisher.publish(new ValueEvent(operand.value, operand.type()));
      } else if (element instanceof ChainOperand) {
        ChainOperand operand = (ChainOperand) element;
        publishChainEvents(storage, operand.name(), operand.type());
      } else if (element instanceof StatementOperator) {
        StatementOperator operand = (StatementOperator) element;
        publisher.publish(new OperatorEvent(operand.descriptor()));
      } else {
        throw new IllegalStateException(element + " is not supported");
      }
    }
  }

  protected void publishChainEvents(
    final Storage storage,
    final Name name,
    final Type type
  ) {
    publisher.publish(new BeginChainEvent(type));

    boolean brokenChain = false;
    Name thisName = name;
    Name currentName = null;
    while (thisName != null && !brokenChain) {
      currentName = currentName == null
          ? thisName.first()
          : NAME_FACTORY.concat(currentName, thisName.first());

      publisher.publish(new NameEvent(thisName.first()));

      if (thisName instanceof Call) {
        Call call = (Call) thisName;

        publisher.publish(new BeginCallEvent());

        Function function = null;
        try {
          function = storage.find(LOCAL, currentName).item(Function.class);
        } catch (final UnexpectedType ex) {
          errorContext().addError(currentName.location(), ex);
        } catch (final NotFound ex) {
          errorContext().addError(currentName.location(), ex);
          brokenChain = true;
        } catch (final StorageException ex) {
          throw new IllegalStateException(ex);
        }
        if (function != null) {
          publishParameterEvents(storage, call.parameters, function);
        }

        publisher.publish(new EndCallEvent());
      }

      thisName = thisName.child;
    }

    publisher.publish(new EndChainEvent());
  }

  private void publishParameterEvents(
    final Storage storage,
    final Parameters parameters,
    final Function function
  ) {
    int index = 0;
    for (final Parameter parameter : parameters.parameters) {
      publisher.publish(new BeginParameterEvent(parameter.name));

      Expression expression = null;
      StatementOperand op = null;

      try {
        expression = parameter.expressionProvider.provide(storage);
      } catch (final StorageException ex) {
        errorContext().addError(ex.name.location(), ex);
      }

      if (expression != null) {
        try {
          op = (StatementOperand) expression.evaluate();
        } catch (final CannotEvaluate ex) {
          errorContext().addError(parameter.location(), ex);
        }
      }

      boolean indexOk = true;
      try {
        function.checkParameterIndex(index, parameter.name);
      } catch (final TooManyParameters ex) {
        indexOk = false;
        errorContext().addError(parameter.location(), ex);
      }

      if (op != null && indexOk) {
        try {
          function.checkParameterType(index, op.type());
        } catch (final CannotCast ex) {
          errorContext().addError(parameter.location(), ex);
        }
      }

      if (expression != null) {
        publishEvents(expression.elements, storage);
      }

      publisher.publish(new EndParameterEvent());

      index+= 1;
    }

    try {
      function.checkParameterCount(parameters.parameters.size());
    } catch (final ParameterRequired ex) {
      errorContext().addError(parameters.location(), ex);
    }
  }
}
