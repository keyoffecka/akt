package net.offecka.pet.akt.analyzer.syntax.defaults.statement.exceptions;

import net.offecka.pet.akt.analyzer.syntax.ModelException;

public class UnreachableStatement extends ModelException {
  public UnreachableStatement() {
    super("Flow never reaches the statement");
  }
}
