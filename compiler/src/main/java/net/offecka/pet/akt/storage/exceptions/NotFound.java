package net.offecka.pet.akt.storage.exceptions;

import net.offecka.pet.akt.analyzer.syntax.Name;

public class NotFound extends StorageException {
  public NotFound(final Name name) {
    super("[" + name + "] not found", name);
  }
}
