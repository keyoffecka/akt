package net.offecka.pet.akt.analyzer.ast;

import net.offecka.pet.akt.errors.Location;

import java.util.List;

import static java.util.List.copyOf;

public class ParamsNode implements Node {
  public final Location location;
  public final List<ParamNode> params;

  public ParamsNode(final Location location, final List<ParamNode> params) {
    this.location = location;
    this.params = copyOf(params);
  }
}
