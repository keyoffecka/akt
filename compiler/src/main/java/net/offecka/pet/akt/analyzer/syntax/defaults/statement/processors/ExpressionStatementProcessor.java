package net.offecka.pet.akt.analyzer.syntax.defaults.statement.processors;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginStatementEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndStatementEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.StatementType;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.result.BlockStatementResultBatch;
import net.offecka.pet.akt.errors.Location;
import net.offecka.pet.akt.events.EventPublisher;
import net.offecka.pet.akt.expression.Expression;
import net.offecka.pet.akt.storage.MutableStorage;

public class ExpressionStatementProcessor extends StatementProcessor {
  protected final Location location;
  protected final Expression expression;

  public ExpressionStatementProcessor(
    final Location location,
    final Expression expression,
    final EventPublisher publisher
  ) {
    super(publisher);

    if (expression == null) {
      throw new IllegalArgumentException();
    }

    this.location = location;
    this.expression = expression;
  }

  @Override
  public BlockStatementResultBatch process(final MutableStorage storage) {
    publisher.publish(new BeginStatementEvent(StatementType.NORMAL));
    publishEvents(expression.elements, storage);
    publisher.publish(new EndStatementEvent());
    return new BlockStatementResultBatch(location);
  }
}
