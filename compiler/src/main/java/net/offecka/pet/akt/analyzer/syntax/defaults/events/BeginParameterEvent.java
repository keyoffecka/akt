package net.offecka.pet.akt.analyzer.syntax.defaults.events;

import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.events.Event;

public class BeginParameterEvent implements Event {
  public final Name name;

  public BeginParameterEvent(final Name name) {
    this.name = name;
  }
}
