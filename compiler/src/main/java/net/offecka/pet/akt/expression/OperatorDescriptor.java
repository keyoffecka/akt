package net.offecka.pet.akt.expression;

import java.util.Objects;

import static java.lang.Integer.MAX_VALUE;
import static java.lang.Integer.MIN_VALUE;

public class OperatorDescriptor {
  public final int precedence;
  public final String name;

  public OperatorDescriptor(final int precedence, final String name) {
    this.precedence = precedence;
    this.name = name;
  }

  @Override
  public String toString() {
    return name;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == this.getClass()) {
      OperatorDescriptor operator = (OperatorDescriptor) o;

      result = Objects.equals(precedence, operator.precedence)
        && Objects.equals(name, operator.name);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(precedence)
      ^ Objects.hashCode(name);
  }

  public interface OperatorDescriptors {
    int LOWEST = MIN_VALUE;
    int HIGHEST = MAX_VALUE;
    int UNI = 0;
    int MUL_DIV = 1;
    int ADD_SUB = 2;
    int BIT = 3;
    int EQL = 4;
    int LOGIC = 5;

    OperatorDescriptor OPEN = new OperatorDescriptor(LOWEST, "(");
    OperatorDescriptor CLOSE = new OperatorDescriptor(HIGHEST, ")");
    OperatorDescriptor NOT = new OperatorDescriptor(UNI, "!");
    OperatorDescriptor MINUS = new OperatorDescriptor(UNI, "-");
    OperatorDescriptor PLUS = new OperatorDescriptor(UNI, "+");
    OperatorDescriptor MUL = new OperatorDescriptor(MUL_DIV, "*");
    OperatorDescriptor DIV = new OperatorDescriptor(MUL_DIV, "/");
    OperatorDescriptor MOD = new OperatorDescriptor(MUL_DIV, "%");
    OperatorDescriptor ADD = new OperatorDescriptor(ADD_SUB, "++");
    OperatorDescriptor SUB = new OperatorDescriptor(ADD_SUB, "--");
    OperatorDescriptor BIT_AND = new OperatorDescriptor(BIT, "&");
    OperatorDescriptor BIT_OR = new OperatorDescriptor(BIT, "|");
    OperatorDescriptor BIT_XOR = new OperatorDescriptor(BIT, "^");
    OperatorDescriptor EQUALS_TO = new OperatorDescriptor(EQL, "===");
    OperatorDescriptor NOT_EQUALS_TO = new OperatorDescriptor(EQL, "!==");
    OperatorDescriptor IS = new OperatorDescriptor(EQL, "==");
    OperatorDescriptor IS_NOT = new OperatorDescriptor(EQL, "!=");
    OperatorDescriptor AND = new OperatorDescriptor(LOGIC, "&&");
    OperatorDescriptor OR = new OperatorDescriptor(LOGIC, "||");
  }
}
