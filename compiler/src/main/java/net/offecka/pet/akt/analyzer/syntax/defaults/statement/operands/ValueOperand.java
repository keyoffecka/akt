package net.offecka.pet.akt.analyzer.syntax.defaults.statement.operands;

import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.errors.Location;

public class ValueOperand implements StatementOperand {
  public final Object value;
  public final Type type;
  public final Location location;

  public ValueOperand(final Object value, final Type type, final Location location) {
    if (value == null) {
      throw new IllegalArgumentException();
    }

    this.value = value;
    this.type = type;
    this.location = location;
  }

  @Override
  public Location location() {
    return location;
  }

  @Override
  public Type type() {
    return type;
  }
}
