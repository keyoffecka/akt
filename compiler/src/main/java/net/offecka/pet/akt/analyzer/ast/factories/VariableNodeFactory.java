package net.offecka.pet.akt.analyzer.ast.factories;

import net.offecka.pet.akt.analyzer.ast.NamespaceVariableNode;
import net.offecka.pet.akt.analyzer.ast.TypeNode;
import net.offecka.pet.akt.analyzer.ast.VariableNode;
import net.offecka.pet.akt.errors.Location;
import net.offecka.pet.akt.grammar.AktParser.NamespaceVarDefContext;
import net.offecka.pet.akt.grammar.AktParser.VarDefContext;

import static net.offecka.pet.akt.analyzer.ast.factories.NodeFactories.NODE_FACTORIES;

public class VariableNodeFactory {
  VariableNodeFactory() {
  }

  public VariableNode create(final VarDefContext varDefContext) {
    return new VariableNode(
      Location.at(varDefContext),
      varDefContext.constant() != null,
      varDefContext.name().getText(),
      varDefContext.type() == null
        ? new TypeNode(Location.after(varDefContext.name().stop), null)
        : NODE_FACTORIES.typeNodeFactory.create(varDefContext.type()),
      NODE_FACTORIES.chainNodeFactory.create(varDefContext.expression())
    );
  }

  public VariableNode create(final NamespaceVarDefContext namespaceVarDefContext) {
    return new NamespaceVariableNode(
      Location.at(namespaceVarDefContext),
      NODE_FACTORIES.scopeFactory.create(namespaceVarDefContext.namespaceMemberScope()),
      namespaceVarDefContext.constant() != null,
      namespaceVarDefContext.name().getText(),
      namespaceVarDefContext.type() == null
        ? new TypeNode(Location.after(namespaceVarDefContext.name().stop), null)
        : NODE_FACTORIES.typeNodeFactory.create(namespaceVarDefContext.type()),
      NODE_FACTORIES.chainNodeFactory.create(namespaceVarDefContext.expression())
    );
  }
}
