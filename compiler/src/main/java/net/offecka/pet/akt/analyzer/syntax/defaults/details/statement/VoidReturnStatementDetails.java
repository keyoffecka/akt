package net.offecka.pet.akt.analyzer.syntax.defaults.details.statement;

import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.returntypeproviders.ReturnTypeProvider;
import net.offecka.pet.akt.errors.Location;

public class VoidReturnStatementDetails implements StatementDetails, WithExpressionLocation, WithStartLocation, WithReturnTypeProvider {
  private final ReturnTypeProvider returnTypeProvider;
  private final Location startLocation;
  private final Location expressionLocation;

  public VoidReturnStatementDetails(
    final ReturnTypeProvider returnTypeProvider,
    final Location startLocation,
    final Location expressionLocation
  ) {
    this.returnTypeProvider = returnTypeProvider;
    this.startLocation = startLocation;
    this.expressionLocation = expressionLocation;
  }

  @Override
  public Location startLocation() {
    return startLocation;
  }

  @Override
  public Location expressionLocation() {
    return expressionLocation;
  }

  @Override
  public ReturnTypeProvider returnTypeProvider() {
    return returnTypeProvider;
  }
}
