package net.offecka.pet.akt.analyzer.ast;

import net.offecka.pet.akt.errors.Location;

public class NameNode implements ChainSegmentNode {
  public final Location location;
  public final String name;

  public NameNode(final Location location, final String name) {
    this.location = location;
    this.name = name;
  }

  @Override
  public String name() {
    return name;
  }
}
