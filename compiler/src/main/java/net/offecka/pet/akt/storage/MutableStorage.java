package net.offecka.pet.akt.storage;

import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.storage.exceptions.Exists;

public interface MutableStorage extends Storage {
  MutableStorage create();
  void put(Visibility visibility, Name name, Item item) throws Exists;
}
