package net.offecka.pet.akt.analyzer.syntax.defaults.initializers;

import net.offecka.pet.akt.analyzer.syntax.defaults.PredefinedType;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.TypeDetails;
import net.offecka.pet.akt.events.EventPublisher;
import net.offecka.pet.akt.storage.MutableStorage;
import net.offecka.pet.akt.storage.exceptions.ExistedInChild;
import net.offecka.pet.akt.storage.exceptions.Exists;
import net.offecka.pet.akt.storage.exceptions.ExistsInStorage;

import static net.offecka.pet.akt.errors.ErrorContext.errorContext;

public class TypeInitializer {
  private final MutableStorage storage;

  public TypeInitializer(final MutableStorage storage) {
    this.storage = storage;
  }

  public void init(final TypeDetails typeDetails) {
    PredefinedType newType = new PredefinedType(typeDetails.name());
    try {
      this.storage.put(typeDetails.visibility(), newType.name(), newType);
    } catch (final ExistsInStorage | ExistedInChild ex) {
      errorContext().addError(ex.name.location(), ex);
    } catch (final Exists ex) {
      throw new IllegalStateException(ex);
    }
  }
}
