package net.offecka.pet.akt.analyzer.syntax.defaults;

import net.offecka.pet.akt.analyzer.syntax.Argument;
import net.offecka.pet.akt.analyzer.syntax.Function;
import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.ArgumentDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.StatementDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.initializers.ArgumentInitializer;
import net.offecka.pet.akt.analyzer.syntax.defaults.initializers.StatementInitializer;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.exceptions.CannotCast;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.exceptions.ParameterRequired;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.exceptions.TooManyParameters;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.result.BlockStatementResultBatch;
import net.offecka.pet.akt.analyzer.syntax.support.ToBeCommitted;
import net.offecka.pet.akt.events.EventPublisher;
import net.offecka.pet.akt.storage.FinderType;
import net.offecka.pet.akt.storage.MutableStorage;
import net.offecka.pet.akt.storage.Result;
import net.offecka.pet.akt.storage.Storage;
import net.offecka.pet.akt.storage.exceptions.StorageException;

import java.util.ArrayList;
import java.util.List;

public class DefaultFunction implements Function {
  private final Name name;
  private final Type type;

  private final List<Argument> arguments = new ArrayList<>();
  private final StatementInitializer statementInitializer;
  private final ArgumentInitializer argumentInitializer;
  private final MutableStorage statementStorage;

  public DefaultFunction(
    final MutableStorage parent,
    final Name name,
    final Type type,
    final EventPublisher publisher
  ) {
    if (name.child != null) {
      throw new IllegalArgumentException();
    }

    this.name = name;
    this.type = type;

    MutableStorage argumentStorage = parent.create();
    statementStorage = argumentStorage.create();
    argumentInitializer = new ArgumentInitializer(argumentStorage, arguments, publisher);
    statementInitializer = new StatementInitializer(statementStorage, publisher);
  }

  @Override
  public Storage create() {
    return statementStorage.create();
  }

  @Override
  public Name name() {
    return name;
  }

  @Override
  public Type type() {
    return type;
  }

  @Override
  public Result find(final FinderType finderType, final Name name) throws StorageException {
    return statementStorage.find(finderType, name);
  }

  @Override
  public ToBeCommitted addArgument(final ArgumentDetails argumentDetails) {
    return argumentInitializer.init(argumentDetails);
  }

  @Override
  public BlockStatementResultBatch addStatement(final StatementDetails statementDetails) {
    return statementInitializer.init(statementDetails);
  }

  @Override
  public void checkParameterIndex(final int paramIndex, final Name paramName) throws TooManyParameters {
    if (paramIndex >= arguments.size()) {
      if (paramName == null) {
        throw new TooManyParameters(paramIndex);
      } else {
        throw new TooManyParameters(paramName);
      }
    }
  }

  @Override
  public void checkParameterType(final int paramIndex, final Type paramType) throws CannotCast {
    Argument argument = arguments.get(paramIndex);
    if (!paramType.isSubOf(argument.type())) {
      throw new CannotCast(argument.name(), paramType, argument.type());
    }
  }

  @Override
  public void checkParameterCount(final int paramCount) throws ParameterRequired {
    if (paramCount < arguments.size()) {
      throw new ParameterRequired(arguments.get(paramCount).name());
    }
  }
}
