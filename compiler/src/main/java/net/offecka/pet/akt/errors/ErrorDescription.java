package net.offecka.pet.akt.errors;

public class ErrorDescription {
  public final String message;
  public final Location location;
  public final Exception cause;

  public ErrorDescription(final String message, final Location location, final Exception cause) {
    if (message == null) {
      throw new IllegalArgumentException();
    }
    if (message.trim().isEmpty()) {
      throw new IllegalArgumentException("Message cannot be empty or blank: [" + message + "]");
    }
    if (location == null) {
      throw new IllegalArgumentException();
    }
    this.message = message;
    this.location = location;
    this.cause = cause;
  }

  public String formattedMessage() {
    String prefix = "";
    if (location.resourceName != null) {
      prefix = location.resourceName + " at " + location.line + ":" + location.column + " - ";
    }
    return prefix + message;
  }

  @Override
  public String toString() {
    return formattedMessage();
  }
}
