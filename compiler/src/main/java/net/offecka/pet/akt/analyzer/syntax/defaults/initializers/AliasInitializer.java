package net.offecka.pet.akt.analyzer.syntax.defaults.initializers;

import net.offecka.pet.akt.analyzer.syntax.defaults.details.AliasDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.AliasEvent;
import net.offecka.pet.akt.events.EventPublisher;
import net.offecka.pet.akt.storage.Item;
import net.offecka.pet.akt.storage.MutableStorage;
import net.offecka.pet.akt.storage.exceptions.ExistedInChild;
import net.offecka.pet.akt.storage.exceptions.Exists;
import net.offecka.pet.akt.storage.exceptions.ExistsInStorage;
import net.offecka.pet.akt.storage.exceptions.NotFound;
import net.offecka.pet.akt.storage.exceptions.StorageException;

import static net.offecka.pet.akt.errors.ErrorContext.errorContext;
import static net.offecka.pet.akt.storage.Visibility.PRIVATE;

public class AliasInitializer {
  private final MutableStorage storage;
  private final EventPublisher publisher;

  public AliasInitializer(final MutableStorage storage, final EventPublisher publisher) {
    this.storage = storage;
    this.publisher = publisher;
  }

  public void init(final AliasDetails aliasDetails) {
    Item item = null;
    try {
      item = aliasDetails.item();
    } catch (final NotFound ex) {
      errorContext().addError(ex.name.location(), ex);
    } catch (final StorageException ex) {
      throw new IllegalStateException(ex);
    }
    if (item != null) {
      try {
        storage.put(PRIVATE, aliasDetails.alias(), item);
      } catch (final ExistsInStorage | ExistedInChild ex) {
        errorContext().addError(ex.name.location(), ex);
        item = null;
      } catch (final Exists ex) {
        throw new IllegalStateException(ex);
      }
      if (item != null) {
        publisher.publish(new AliasEvent(aliasDetails.alias(), aliasDetails.name()));
      }
    }
  }
}
