package net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.returntypeproviders;

import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.storage.Storage;
import net.offecka.pet.akt.storage.exceptions.StorageException;

public interface ReturnTypeProvider {
  Type provide(Storage storage) throws StorageException;
}
