package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndIfEvent;

import java.io.IOException;
import java.io.Writer;

public class EndIfEventHandler implements EventHandler<EndIfEvent> {
  private final Writer writer;

  public EndIfEventHandler(final Writer writer) {
    this.writer = writer;
  }

  @Override
  public void handle(final EndIfEvent event) throws IOException {
    writer.write(")");
  }
}
