package net.offecka.pet.akt.analyzer.ast.factories;

import net.offecka.pet.akt.analyzer.ast.AssignmentNode;
import net.offecka.pet.akt.errors.Location;
import net.offecka.pet.akt.grammar.AktParser.AssignmentContext;
import net.offecka.pet.akt.grammar.AktParser.NamespaceAssignmentContext;

import static net.offecka.pet.akt.analyzer.ast.factories.NodeFactories.NODE_FACTORIES;

public class AssignmentNodeFactory {
  AssignmentNodeFactory() {
  }

  public AssignmentNode create(final AssignmentContext assignment) {
    return new AssignmentNode(
      Location.at(assignment),
      NODE_FACTORIES.chainNodeFactory.create(assignment.assignable()),
      NODE_FACTORIES.chainNodeFactory.create(assignment.expression())
    );
  }

  public AssignmentNode create(final NamespaceAssignmentContext assignment) {
    return new AssignmentNode(
      Location.at(assignment),
      NODE_FACTORIES.chainNodeFactory.create(assignment.assignable()),
      NODE_FACTORIES.chainNodeFactory.create(assignment.expression())
    );
  }
}
