package net.offecka.pet.akt.storage;

import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.storage.exceptions.StorageException;

import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.VOID;
import static net.offecka.pet.akt.storage.FinderType.LOCAL;

public class SearchingStorage implements Storage {

  private final Storage storage;

  public SearchingStorage(final Storage storage) {
    this.storage = storage;
  }

  @Override
  public Result find(final FinderType finderType, final Name name) throws StorageException {
    return storage.find(finderType, name);
  }

  @Override
  public Storage create() {
    return storage.create();
  }

  public Type findVoidType() {
    Type voidType = null;
    try {
      voidType = find(LOCAL, VOID).item(Type.class);
    } catch (final StorageException ex) {
      throw new IllegalStateException(ex);
    }
    return voidType;
  }
}
