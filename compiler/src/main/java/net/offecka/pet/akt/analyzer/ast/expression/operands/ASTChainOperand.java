package net.offecka.pet.akt.analyzer.ast.expression.operands;

import net.offecka.pet.akt.analyzer.ast.ChainNode;
import net.offecka.pet.akt.analyzer.ast.expression.ASTOperand;
import net.offecka.pet.akt.errors.Location;

public class ASTChainOperand implements ASTOperand {
  public final Location location;
  public final ChainNode chain;

  public ASTChainOperand(final Location location, final ChainNode chain) {
    this.location = location;
    this.chain = chain;
  }
}
