package net.offecka.pet.akt.analyzer.syntax.defaults.statement.operands;

import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.errors.Location;

public class TypeOperand implements StatementOperand {
  private final Type type;
  private final Location location;

  public TypeOperand(final Type type, final Location location) {
    this.type = type;
    this.location = location;
  }

  @Override
  public Location location() {
    return location;
  }

  @Override
  public Type type() {
    return type;
  }
}
