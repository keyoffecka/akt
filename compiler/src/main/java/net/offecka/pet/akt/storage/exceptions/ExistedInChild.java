package net.offecka.pet.akt.storage.exceptions;

import net.offecka.pet.akt.analyzer.syntax.Name;

public class ExistedInChild extends Exists {
  public ExistedInChild(final Name name, final Name prev) {
    super(name, prev);
  }
}
