package net.offecka.pet.akt.analyzer.ast;

import net.offecka.pet.akt.errors.Location;

public class NamespaceNode implements Node {
  public final Location location;
  public final FQNNode fqn;

  public NamespaceNode(final Location location, final FQNNode fqn) {
    this.location = location;
    this.fqn = fqn;
  }
}
