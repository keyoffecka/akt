package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndCallEvent;
import net.offecka.pet.akt.generator.context.ContextManager;

import java.io.IOException;
import java.io.Writer;

public class EndCallEventHandler implements EventHandler<EndCallEvent> {
  private final Writer writer;
  private final ContextManager contextManager;

  public EndCallEventHandler(final Writer writer, final ContextManager contextManager) {
    this.writer = writer;
    this.contextManager = contextManager;
  }

  @Override
  public void handle(final EndCallEvent event) throws IOException {
    writer.write(")");
    contextManager.closeContext();
  }
}
