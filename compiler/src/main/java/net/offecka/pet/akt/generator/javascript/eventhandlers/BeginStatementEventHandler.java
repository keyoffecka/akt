package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginStatementEvent;

import java.io.IOException;
import java.io.Writer;

public class BeginStatementEventHandler implements EventHandler<BeginStatementEvent> {
  private final Writer writer;
  private final Tab tab;

  public BeginStatementEventHandler(final Writer writer, final Tab tab) {
    this.writer = writer;
    this.tab = tab;
  }

  @Override
  public void handle(final BeginStatementEvent event) throws IOException {
    switch(event.statementType) {
      case NORMAL:
        writer.write(tab + "");
        break;
      case RETURN:
        writer.write(tab + "return ");
        break;
    }
  }
}
