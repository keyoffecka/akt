package net.offecka.pet.akt.analyzer.syntax;

import net.offecka.pet.akt.errors.Location;

public class Call extends Name {
  public final Parameters parameters;

  public Call(
    final String name,
    final Parameters parameters,
    final Name child,
    final Location location
  ) {
    super(name, child, location);

    this.parameters = parameters;
  }
}
