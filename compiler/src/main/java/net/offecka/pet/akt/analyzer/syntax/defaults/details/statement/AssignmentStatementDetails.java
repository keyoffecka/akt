package net.offecka.pet.akt.analyzer.syntax.defaults.details.statement;

import net.offecka.pet.akt.analyzer.ast.AssignmentNode;
import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.defaults.support.ExpressionProvider;
import net.offecka.pet.akt.errors.Location;

import static net.offecka.pet.akt.analyzer.syntax.NameFactory.NAME_FACTORY;

public class AssignmentStatementDetails extends DefaultExpressionStatementDetails {
  private final Name variableName;
  private final Location assignmentLocation;

  public AssignmentStatementDetails(final AssignmentNode assignmentNode) {
    super(new ExpressionProvider(assignmentNode.expression.elements()), assignmentNode.expression.location);

    this.variableName = NAME_FACTORY.create(assignmentNode.chain);
    this.assignmentLocation = assignmentNode.location;
  }

  public Name variableName() {
    return variableName;
  }

  public Location assignmentLocation() {
    return assignmentLocation;
  }
}
