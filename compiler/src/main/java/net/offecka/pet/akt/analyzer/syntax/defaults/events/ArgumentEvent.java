package net.offecka.pet.akt.analyzer.syntax.defaults.events;

import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.events.Event;

public class ArgumentEvent implements Event {
  public final Name name;
  public final Type type;

  public ArgumentEvent(final Name name, final Type type) {
    this.name = name;
    this.type = type;
  }
}
