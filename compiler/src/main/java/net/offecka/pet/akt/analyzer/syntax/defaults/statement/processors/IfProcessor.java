package net.offecka.pet.akt.analyzer.syntax.defaults.statement.processors;

import net.offecka.pet.akt.analyzer.syntax.defaults.statement.result.BlockStatementResult;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.result.BlockStatementResultBatch;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.result.StatementResultBatchFactory;
import net.offecka.pet.akt.errors.Location;
import net.offecka.pet.akt.events.EventPublisher;
import net.offecka.pet.akt.storage.MutableStorage;

import java.util.ArrayList;
import java.util.List;

import static java.util.List.copyOf;

public class IfProcessor extends StatementProcessor {
  private final List<IfElseBlockProcessor> blockProcessors;
  private final StatementResultBatchFactory statementResultBatchFactory;
  private final Location location;

  public IfProcessor(
    final Location location,
    final List<IfElseBlockProcessor> blockProcessors,
    final EventPublisher publisher
  ) {
    super(publisher);

    this.location = location;
    this.blockProcessors = copyOf(blockProcessors);
    statementResultBatchFactory = new StatementResultBatchFactory();
  }

  @Override
  public BlockStatementResultBatch process(final MutableStorage storage) {
    List<BlockStatementResult> blockStatementResults = new ArrayList<>();

    boolean hadUnreachableStatements = false;
    for (final IfElseBlockProcessor blockProcessor : blockProcessors) {
      BlockStatementResultBatch blockStatementResultBatch = blockProcessor.process(storage);
      blockStatementResults.addAll(blockStatementResultBatch.blockStatementResults());
      hadUnreachableStatements = hadUnreachableStatements
        || blockStatementResultBatch.blockStatementResults().stream().anyMatch(bsr -> bsr.hasUnreachableStatements);
 }

    return statementResultBatchFactory.create(location, blockStatementResults, hadUnreachableStatements);
  }
}
