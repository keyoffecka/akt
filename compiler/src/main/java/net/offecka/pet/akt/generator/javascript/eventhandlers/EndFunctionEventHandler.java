package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginFunctionEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndFunctionEvent;
import net.offecka.pet.akt.generator.context.ContextManager;
import net.offecka.pet.akt.generator.context.ObjectContext;

import java.io.IOException;
import java.io.Writer;

import static net.offecka.pet.akt.storage.Visibility.PUBLIC;

public class EndFunctionEventHandler implements EventHandler<EndFunctionEvent> {
  private final Writer writer;
  private final Tab tab;
  private final ContextManager contextManager;

  public EndFunctionEventHandler(final Writer writer, final Tab tab, final ContextManager contextManager) {
    this.writer = writer;
    this.tab = tab;
    this.contextManager = contextManager;
  }

  @Override
  public void handle(final EndFunctionEvent event) throws IOException {
    tab.unindent();
    writer.write(tab + "}\n");

    BeginFunctionEvent beginFunctionEvent = contextManager.<ObjectContext>context().data();
    if (beginFunctionEvent.visibility == PUBLIC) {
      writer.write(tab + "module.exports." + beginFunctionEvent.name + " = " + beginFunctionEvent.name + ";\n");
    }

    contextManager.closeContext();
  }
}
