package net.offecka.pet.akt.errors;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;

import static net.offecka.pet.akt.errors.ErrorContext.errorContext;

public final class Location {
  public static final Location NO_LOCATION = new Location(null, null, null);

  public final String resourceName;
  public final Integer line;
  public final Integer column;

  public static Location at(final ParserRuleContext context) {
    return at(context.getStart());
  }

  public static Location at(final Token token) {
    return new Location(
      errorContext().unit().resourceName(),
      token.getLine(),
      1 + token.getCharPositionInLine()
    );
  }

  public static Location before(final Token next) {
    return new Location(
      errorContext().unit().resourceName(),
      next.getLine(),
      1 + next.getCharPositionInLine() - 1
    );
  }

  public static Location after(final Token end) {
    return new Location(
      errorContext().unit().resourceName(),
      end.getLine(),
      1 + end.getCharPositionInLine() + end.getText().length()
    );
  }

  private Location(final String resourceName, final Integer line, final Integer column) {
    if (line == null && column != null) {
      throw new IllegalArgumentException();
    }
    if (line == null && resourceName != null) {
      throw new IllegalArgumentException();
    }
    if (column == null && line != null) {
      throw new IllegalArgumentException();
    }
    if (resourceName == null && line != null) {
      throw new IllegalArgumentException();
    }

    this.resourceName = resourceName;
    this.line = line;
    this.column = column;
  }
}
