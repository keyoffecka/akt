package net.offecka.pet.akt.analyzer.syntax.support;

import net.offecka.pet.akt.analyzer.syntax.Type;

public interface WithType {
  Type type();
}
