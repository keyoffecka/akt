package net.offecka.pet.akt.analyzer.syntax;

public interface Type {
  String description();
  boolean isSubOf(final Type type);
}
