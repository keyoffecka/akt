package net.offecka.pet.akt.events;

public interface EventSource {
  void addListener(Listener listener);
}
