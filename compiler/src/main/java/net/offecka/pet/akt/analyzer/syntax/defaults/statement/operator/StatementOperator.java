package net.offecka.pet.akt.analyzer.syntax.defaults.statement.operator;

import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.operands.StatementOperand;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.operands.TypeOperand;
import net.offecka.pet.akt.analyzer.syntax.support.WithName;
import net.offecka.pet.akt.errors.Location;
import net.offecka.pet.akt.expression.CannotEvaluate;
import net.offecka.pet.akt.expression.Operand;
import net.offecka.pet.akt.expression.Operator;
import net.offecka.pet.akt.expression.OperatorDescriptor;
import net.offecka.pet.akt.storage.Storage;
import net.offecka.pet.akt.storage.exceptions.StorageException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Map.of;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.ANY;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.BOOL;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.INT;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.REAL;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.STR;
import static net.offecka.pet.akt.expression.OperatorDescriptor.OperatorDescriptors.ADD;
import static net.offecka.pet.akt.expression.OperatorDescriptor.OperatorDescriptors.AND;
import static net.offecka.pet.akt.expression.OperatorDescriptor.OperatorDescriptors.BIT_AND;
import static net.offecka.pet.akt.expression.OperatorDescriptor.OperatorDescriptors.BIT_OR;
import static net.offecka.pet.akt.expression.OperatorDescriptor.OperatorDescriptors.BIT_XOR;
import static net.offecka.pet.akt.expression.OperatorDescriptor.OperatorDescriptors.DIV;
import static net.offecka.pet.akt.expression.OperatorDescriptor.OperatorDescriptors.EQUALS_TO;
import static net.offecka.pet.akt.expression.OperatorDescriptor.OperatorDescriptors.IS;
import static net.offecka.pet.akt.expression.OperatorDescriptor.OperatorDescriptors.IS_NOT;
import static net.offecka.pet.akt.expression.OperatorDescriptor.OperatorDescriptors.MOD;
import static net.offecka.pet.akt.expression.OperatorDescriptor.OperatorDescriptors.MUL;
import static net.offecka.pet.akt.expression.OperatorDescriptor.OperatorDescriptors.NOT_EQUALS_TO;
import static net.offecka.pet.akt.expression.OperatorDescriptor.OperatorDescriptors.OR;
import static net.offecka.pet.akt.expression.OperatorDescriptor.OperatorDescriptors.SUB;
import static net.offecka.pet.akt.expression.OperatorDescriptor.OperatorDescriptors.UNI;
import static net.offecka.pet.akt.storage.FinderType.LOCAL;


public class StatementOperator implements Operator {
  private static final Map<OperatorDescriptor, Map<Name, Map<Name, Name>>> BIN_MAP = new HashMap<>();

  static {
    BIN_MAP.put(DIV, of(
      INT, of(INT, INT, REAL, REAL, ANY, ANY),
      REAL, of(INT, REAL, REAL, REAL, ANY, ANY),
      STR, of(ANY, ANY),
      BOOL, of(ANY, ANY),
      ANY, of(INT, ANY, REAL, ANY, STR, ANY, BOOL, ANY, ANY, ANY)
    ));
    BIN_MAP.put(MUL, of(
      INT, of(INT, INT, REAL, REAL, ANY, ANY),
      REAL, of(INT, REAL, REAL, REAL, ANY, ANY),
      STR, of(ANY, ANY),
      BOOL, of(ANY, ANY),
      ANY, of(INT, ANY, REAL, ANY, STR, ANY, BOOL, ANY, ANY, ANY)
    ));
    BIN_MAP.put(MOD, of(
      INT, of(INT, INT, ANY, ANY),
      REAL, of(ANY, ANY),
      STR, of(ANY, ANY),
      BOOL, of(ANY, ANY),
      ANY, of(INT, ANY, REAL, ANY, STR, ANY, BOOL, ANY, ANY, ANY)
    ));
    BIN_MAP.put(ADD, of(
      INT, of(INT, INT, REAL, REAL, STR, STR, ANY, ANY),
      REAL, of(INT, REAL, REAL, REAL, STR, STR, ANY, ANY),
      STR, of(INT, STR, REAL, STR, STR, STR, BOOL, STR, ANY, ANY),
      BOOL, of(STR, STR, BOOL, BOOL, ANY, ANY),
      ANY, of(INT, ANY, REAL, ANY, STR, ANY, BOOL, ANY, ANY, ANY)
    ));
    BIN_MAP.put(SUB, of(
      INT, of(INT, INT, REAL, REAL, ANY, ANY),
      REAL, of(INT, REAL, REAL, REAL, ANY, ANY),
      STR, of(ANY, ANY),
      BOOL, of(BOOL, BOOL, ANY, ANY),
      ANY, of(INT, ANY, REAL, ANY, STR, ANY, BOOL, ANY, ANY, ANY)
    ));
    BIN_MAP.put(BIT_AND, of(
      INT, of(INT, INT, ANY, INT),
      REAL, of(),
      STR, of(),
      BOOL, of(),
      ANY, of(INT, INT, ANY, INT)
    ));
    BIN_MAP.put(BIT_OR, of(
      INT, of(INT, INT, ANY, INT),
      REAL, of(),
      STR, of(),
      BOOL, of(),
      ANY, of(INT, INT, ANY, INT)
    ));
    BIN_MAP.put(BIT_XOR, of(
      INT, of(INT, INT, ANY, INT),
      REAL, of(),
      STR, of(),
      BOOL, of(),
      ANY, of(INT, INT, ANY, INT)
    ));
    BIN_MAP.put(AND, of(
      INT, of(),
      REAL, of(),
      STR, of(),
      BOOL, of(BOOL, BOOL, ANY, BOOL),
      ANY, of(BOOL, BOOL, ANY, BOOL)
    ));
    BIN_MAP.put(OR, of(
      INT, of(),
      REAL, of(),
      STR, of(),
      BOOL, of(BOOL, BOOL, ANY, BOOL),
      ANY, of(BOOL, BOOL, ANY, BOOL)
    ));
    BIN_MAP.put(IS, of(
      INT, of(INT, BOOL, REAL, BOOL, STR, BOOL, BOOL, BOOL, ANY, BOOL),
      REAL, of(INT, BOOL, REAL, BOOL, STR, BOOL, BOOL, BOOL, ANY, BOOL),
      STR, of(INT, BOOL, REAL, BOOL, STR, BOOL, BOOL, BOOL, ANY, BOOL),
      BOOL, of(INT, BOOL, REAL, BOOL, STR, BOOL, BOOL, BOOL, ANY, BOOL),
      ANY, of(INT, BOOL, REAL, BOOL, STR, BOOL, BOOL, BOOL, ANY, BOOL)
    ));
    BIN_MAP.put(IS_NOT, of(
      INT, of(INT, BOOL, REAL, BOOL, STR, BOOL, BOOL, BOOL, ANY, BOOL),
      REAL, of(INT, BOOL, REAL, BOOL, STR, BOOL, BOOL, BOOL, ANY, BOOL),
      STR, of(INT, BOOL, REAL, BOOL, STR, BOOL, BOOL, BOOL, ANY, BOOL),
      BOOL, of(INT, BOOL, REAL, BOOL, STR, BOOL, BOOL, BOOL, ANY, BOOL),
      ANY, of(INT, BOOL, REAL, BOOL, STR, BOOL, BOOL, BOOL, ANY, BOOL)
    ));
    BIN_MAP.put(EQUALS_TO, of(
      INT, of(INT, BOOL, REAL, BOOL, STR, BOOL, BOOL, BOOL, ANY, BOOL),
      REAL, of(INT, BOOL, REAL, BOOL, STR, BOOL, BOOL, BOOL, ANY, BOOL),
      STR, of(INT, BOOL, REAL, BOOL, STR, BOOL, BOOL, BOOL, ANY, BOOL),
      BOOL, of(INT, BOOL, REAL, BOOL, STR, BOOL, BOOL, BOOL, ANY, BOOL),
      ANY, of(INT, BOOL, REAL, BOOL, STR, BOOL, BOOL, BOOL, ANY, BOOL)
    ));
    BIN_MAP.put(NOT_EQUALS_TO, of(
      INT, of(INT, BOOL, REAL, BOOL, STR, BOOL, BOOL, BOOL, ANY, BOOL),
      REAL, of(INT, BOOL, REAL, BOOL, STR, BOOL, BOOL, BOOL, ANY, BOOL),
      STR, of(INT, BOOL, REAL, BOOL, STR, BOOL, BOOL, BOOL, ANY, BOOL),
      BOOL, of(INT, BOOL, REAL, BOOL, STR, BOOL, BOOL, BOOL, ANY, BOOL),
      ANY, of(INT, BOOL, REAL, BOOL, STR, BOOL, BOOL, BOOL, ANY, BOOL)
    ));
  }

  private final Storage storage;
  private final OperatorDescriptor descriptor;
  private final Location location;

  public StatementOperator(final Storage storage, final OperatorDescriptor descriptor, final Location location) {
    this.storage = storage;
    this.descriptor = descriptor;
    this.location = location;
  }

  @Override
  public OperatorDescriptor descriptor() {
    return descriptor;
  }

  @Override
  public TypeOperand evaluate(final List<Operand> operands) throws CannotEvaluate {
    TypeOperand operand = null;

    if (descriptor().precedence == UNI) {
      StatementOperand uniOperand = (StatementOperand) operands.remove(0);
      Type uniType = uniOperand.type();
      operand = new TypeOperand(uniType, uniOperand.location());
    } else {
      StatementOperand rightOperand = (StatementOperand) operands.remove(0);
      if (!(rightOperand.type() instanceof WithName)) {
        throw new CannotEvaluate("Unsupported right operand type " + rightOperand.type().description(), rightOperand.location());
      }

      StatementOperand leftOperand = (StatementOperand) operands.remove(0);
      if (!(leftOperand.type() instanceof WithName)) {
        throw new CannotEvaluate("Unsupported left operand type " + leftOperand.type().description(), leftOperand.location());
      }

      Type leftType = leftOperand.type();
      WithName leftTypeWithName = (WithName) leftType;
      if (!leftTypeWithName.name().equals(ANY)) {
        try {
          storage.find(LOCAL, leftTypeWithName.name());
        } catch (final StorageException ex) {
          throw new IllegalStateException("Unsupported left operand type " + leftType.description(), ex);
        }
      }

      Type rightType = rightOperand.type();
      WithName rightTypeWithName = (WithName) rightType;
      try {
        storage.find(LOCAL, rightTypeWithName.name());
      } catch (final StorageException ex) {
        throw new IllegalStateException("Unsupported right operand type " + rightType.description(), ex);
      }

      Map<Name, Map<Name, Name>> leftMap = BIN_MAP.get(descriptor());
      if (leftMap == null) {
        throw new CannotEvaluate("Unsupported operator " + descriptor(), location);
      }

      Map<Name, Name> rightMap = leftMap.get(leftTypeWithName.name());
      if (rightMap == null) {
        throw new CannotEvaluate("Unsupported left operand type " + leftType.description(), leftOperand.location());
      }
      Name name = rightMap.get(rightTypeWithName.name());
      if (name == null) {
        throw new CannotEvaluate("Unsupported right operand type " + rightType.description(), rightOperand.location());
      }

      Type type = null;
      try {
        type = storage.find(LOCAL, name).item(Type.class);
      } catch (final StorageException ex) {
        throw new IllegalStateException("Unsupported operator result type " + name, ex);
      }

      operand = new TypeOperand(type, leftOperand.location());
    }

    return operand;
  }
}
