package net.offecka.pet.akt.analyzer.syntax.defaults.support;

import net.offecka.pet.akt.analyzer.ast.expression.ASTClosing;
import net.offecka.pet.akt.analyzer.ast.expression.ASTElement;
import net.offecka.pet.akt.analyzer.ast.expression.ASTOpening;
import net.offecka.pet.akt.analyzer.ast.expression.ASTOperator;
import net.offecka.pet.akt.analyzer.ast.expression.ASTOperator.ASTOperators;
import net.offecka.pet.akt.analyzer.ast.expression.operands.ASTBoolLiteralOperand;
import net.offecka.pet.akt.analyzer.ast.expression.operands.ASTChainOperand;
import net.offecka.pet.akt.analyzer.ast.expression.operands.ASTIntLiteralOperand;
import net.offecka.pet.akt.analyzer.ast.expression.operands.ASTRealLiteralOperand;
import net.offecka.pet.akt.analyzer.ast.expression.operands.ASTStrLiteralOperand;
import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.operands.ChainOperand;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.operands.ValueOperand;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.operator.StatementOperator;
import net.offecka.pet.akt.analyzer.syntax.support.WithType;
import net.offecka.pet.akt.expression.Element;
import net.offecka.pet.akt.expression.Expression;
import net.offecka.pet.akt.expression.OperatorDescriptor;
import net.offecka.pet.akt.expression.OperatorDescriptor.OperatorDescriptors;
import net.offecka.pet.akt.storage.Storage;
import net.offecka.pet.akt.storage.exceptions.NotFound;
import net.offecka.pet.akt.storage.exceptions.StorageException;
import net.offecka.pet.akt.storage.exceptions.UnexpectedType;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.BOOL;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.INT;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.REAL;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.STR;
import static net.offecka.pet.akt.analyzer.syntax.NameFactory.NAME_FACTORY;
import static net.offecka.pet.akt.storage.FinderType.LOCAL;

public class ExpressionProvider {
  private final List<ASTElement> elements;

  private static final Map<ASTOperators, OperatorDescriptor> DESCRIPTOR_MAP = new HashMap<>();

  static {
    DESCRIPTOR_MAP.put(ASTOperators.NOT, OperatorDescriptors.NOT);
    DESCRIPTOR_MAP.put(ASTOperators.PLUS, OperatorDescriptors.PLUS);
    DESCRIPTOR_MAP.put(ASTOperators.MINUS, OperatorDescriptors.MINUS);
    DESCRIPTOR_MAP.put(ASTOperators.MUL, OperatorDescriptors.MUL);
    DESCRIPTOR_MAP.put(ASTOperators.DIV, OperatorDescriptors.DIV);
    DESCRIPTOR_MAP.put(ASTOperators.MOD, OperatorDescriptors.MOD);
    DESCRIPTOR_MAP.put(ASTOperators.ADD, OperatorDescriptors.ADD);
    DESCRIPTOR_MAP.put(ASTOperators.SUB, OperatorDescriptors.SUB);
    DESCRIPTOR_MAP.put(ASTOperators.BIT_AND, OperatorDescriptors.BIT_AND);
    DESCRIPTOR_MAP.put(ASTOperators.BIT_OR, OperatorDescriptors.BIT_OR);
    DESCRIPTOR_MAP.put(ASTOperators.BIT_XOR, OperatorDescriptors.BIT_XOR);
    DESCRIPTOR_MAP.put(ASTOperators.EQUALS_TO, OperatorDescriptors.EQUALS_TO);
    DESCRIPTOR_MAP.put(ASTOperators.NOT_EQUALS_TO, OperatorDescriptors.NOT_EQUALS_TO);
    DESCRIPTOR_MAP.put(ASTOperators.IS, OperatorDescriptors.IS);
    DESCRIPTOR_MAP.put(ASTOperators.IS_NOT, OperatorDescriptors.IS_NOT);
    DESCRIPTOR_MAP.put(ASTOperators.AND, OperatorDescriptors.AND);
    DESCRIPTOR_MAP.put(ASTOperators.OR, OperatorDescriptors.OR);
  }

  public ExpressionProvider(final List<ASTElement> elements) {
    this.elements = elements;
  }

  public Expression provide(final Storage storage) throws NotFound, UnexpectedType {
    List<Element> result = new ArrayList<>();
    for (final ASTElement element : elements) {
      try {
        if (element instanceof ASTOperator) {
          ASTOperator operator = (ASTOperator) element;
          OperatorDescriptor descriptor = DESCRIPTOR_MAP.get(operator.operator);
          if (descriptor == null) {
            throw new UnsupportedOperationException("operator: " + operator.operator);
          }
          result.add(new StatementOperator(storage, descriptor, operator.location));
        } else if (element instanceof ASTChainOperand) {
          ASTChainOperand refOperand = (ASTChainOperand) element;
          Name fullName = NAME_FACTORY.create(refOperand.chain);
          WithType hasType = storage.find(LOCAL, fullName).item(WithType.class);
          result.add(new ChainOperand(fullName, hasType.type(), refOperand.location));
        } else if (element instanceof ASTIntLiteralOperand) {
          ASTIntLiteralOperand intOperand = (ASTIntLiteralOperand) element;
          long value = new BigInteger(intOperand.value).longValueExact();
          Type intType = storage.find(LOCAL, INT).item(Type.class);
          result.add(new ValueOperand(value, intType, intOperand.location));
        } else if (element instanceof ASTRealLiteralOperand) {
          ASTRealLiteralOperand realOperand = (ASTRealLiteralOperand) element;
          double value = new BigDecimal(realOperand.value).doubleValue();
          Type realType = storage.find(LOCAL, REAL).item(Type.class);
          result.add(new ValueOperand(value, realType, realOperand.location));
        } else if (element instanceof ASTStrLiteralOperand) {
          ASTStrLiteralOperand strOperand = (ASTStrLiteralOperand) element;
          Type strType = storage.find(LOCAL, STR).item(Type.class);
          result.add(new ValueOperand(strOperand.value, strType, strOperand.location));
        } else if (element instanceof ASTBoolLiteralOperand) {
          ASTBoolLiteralOperand boolOperand = (ASTBoolLiteralOperand) element;
          Type boolType = storage.find(LOCAL, BOOL).item(Type.class);
          result.add(new ValueOperand(boolOperand.value, boolType, boolOperand.location));
        } else if (element instanceof ASTOpening) {
          ASTOpening opening = (ASTOpening) element;
          result.add(new StatementOperator(storage, OperatorDescriptors.OPEN, opening.location));
        } else if (element instanceof ASTClosing) {
          ASTClosing closing = (ASTClosing) element;
          result.add(new StatementOperator(storage, OperatorDescriptors.CLOSE, closing.location));
        } else {
          throw new UnsupportedOperationException(element + " is unsupported");
        }
      } catch (final UnexpectedType | NotFound ex) {
        throw ex;
      } catch (final StorageException ex) {
        throw new IllegalStateException(ex);
      }
    }
    return new Expression(result);
  }
}
