package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.VariableEvent;

import java.io.IOException;
import java.io.Writer;

public class VariableEventHandler implements EventHandler<VariableEvent> {
  private final Writer writer;

  public VariableEventHandler(final Writer writer) {
    this.writer = writer;
  }

  @Override
  public void handle(final VariableEvent e) throws IOException {
    writer.write((e.unmodifiable ? "const" : "let") + " " + e.name + " = ");
  }
}
