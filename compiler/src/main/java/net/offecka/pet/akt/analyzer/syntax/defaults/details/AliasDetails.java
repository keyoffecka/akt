package net.offecka.pet.akt.analyzer.syntax.defaults.details;

import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.Namespace;
import net.offecka.pet.akt.analyzer.syntax.support.WithName;
import net.offecka.pet.akt.storage.Item;
import net.offecka.pet.akt.storage.exceptions.StorageException;

import static net.offecka.pet.akt.storage.FinderType.FOREIGN;

public class AliasDetails implements WithName {
  private final Name alias;
  private final Namespace root;
  private final Name name;

  public AliasDetails(final Name alias, final Namespace root, final Name name) {
    this.alias = alias;
    this.name = name;
    this.root = root;
  }

  public Item item() throws StorageException {
    return root.find(FOREIGN, name).item(Item.class);
  }

  public Name alias() {
    return alias;
  }

  @Override
  public Name name() {
    return name;
  }
}
