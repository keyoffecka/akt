package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndArgumentsEvent;
import net.offecka.pet.akt.generator.context.ContextManager;

import java.io.IOException;
import java.io.Writer;

public class EndArgumentsEventHandler implements EventHandler<EndArgumentsEvent> {
  private final Writer writer;
  private final Tab tab;
  private final ContextManager contextManager;

  public EndArgumentsEventHandler(final Writer writer, final Tab tab, final ContextManager contextManager) {
    this.writer = writer;
    this.tab = tab;
    this.contextManager = contextManager;
  }

  @Override
  public void handle(final EndArgumentsEvent event) throws IOException {
    writer.write(") {\n");
    tab.indent();
    contextManager.closeContext();
  }
}
