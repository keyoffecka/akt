package net.offecka.pet.akt.generator.context;

public class ObjectContext extends Context {
  private final Object data;

  public ObjectContext(final Context context, final Object data) {
    super(context);

    this.data = data;
  }

  public <T> T data() {
    return (T) data;
  }
}
