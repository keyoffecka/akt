package net.offecka.pet.akt.analyzer.syntax.defaults.events;

import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.events.Event;

public class BeginChainEvent implements Event {
  public final Type type;

  public BeginChainEvent(final Type type) {
    this.type = type;
  }
}
