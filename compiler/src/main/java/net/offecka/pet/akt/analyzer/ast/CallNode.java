package net.offecka.pet.akt.analyzer.ast;

import net.offecka.pet.akt.errors.Location;

public class CallNode implements ChainSegmentNode {
  public final Location location;
  public final String name;
  public final ParamsNode params;

  public CallNode(final Location location, final String name, final ParamsNode params) {
    this.location = location;
    this.name = name;
    this.params = params;
  }

  @Override
  public String name() {
    return name;
  }
}
