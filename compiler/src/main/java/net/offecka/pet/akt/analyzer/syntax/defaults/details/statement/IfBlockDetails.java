package net.offecka.pet.akt.analyzer.syntax.defaults.details.statement;

import net.offecka.pet.akt.errors.Location;

public class IfBlockDetails implements StatementDetails, WithStartLocation {
  private final ExpressionStatementDetails expressionStatementDetails;
  private final BlockDetails blockDetails;

  public IfBlockDetails(
    final ExpressionStatementDetails expressionStatementDetails,
    final BlockDetails blockDetails
  ) {
    this.expressionStatementDetails = expressionStatementDetails;
    this.blockDetails = blockDetails;
  }

  @Override
  public Location startLocation() {
    return blockDetails.startLocation();
  }

  public Location endLocation() {
    return blockDetails.endLocation();
  }

  public ExpressionStatementDetails expressionStatementDetails() {
    return expressionStatementDetails;
  }

  public BlockDetails blockDetails() {
    return blockDetails;
  }
}
