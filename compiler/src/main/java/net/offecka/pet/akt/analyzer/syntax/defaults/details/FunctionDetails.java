package net.offecka.pet.akt.analyzer.syntax.defaults.details;

import net.offecka.pet.akt.analyzer.ast.ArgumentNode;
import net.offecka.pet.akt.analyzer.ast.FunctionNode;
import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.StatementDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.StatementDetailsFactory;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.returntypeproviders.ByNameReturnTypeProvider;
import net.offecka.pet.akt.analyzer.syntax.support.WithName;
import net.offecka.pet.akt.errors.Location;
import net.offecka.pet.akt.storage.Visibility;
import net.offecka.pet.akt.storage.VisibilityFactory;

import java.util.ArrayList;
import java.util.List;

import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.VOID;
import static net.offecka.pet.akt.analyzer.syntax.NameFactory.NAME_FACTORY;

public class FunctionDetails implements WithName {
  private final Visibility visibility;
  private final Name name;
  private final Name returnTypeName;
  private final ArrayList<ArgumentDetails> argumentDetailsList;
  private final List<StatementDetails> statementDetailsList;
  private final Location endLocation;

  public FunctionDetails(final FunctionNode functionNode) {
    argumentDetailsList = new ArrayList<>();
    for (final ArgumentNode argumentNode : functionNode.arguments) {
      argumentDetailsList.add(new ArgumentDetails(argumentNode));
    }

    endLocation = functionNode.endLocation;
    visibility = new VisibilityFactory().create(functionNode.scope);
    name = NAME_FACTORY.create(functionNode.name, functionNode.location);
    returnTypeName = functionNode.returnType.fqn == null
      ? VOID : NAME_FACTORY.create(functionNode.returnType.fqn);

    StatementDetailsFactory statementDetailsFactory = new StatementDetailsFactory();
    statementDetailsList = statementDetailsFactory.create(
      new ByNameReturnTypeProvider(returnTypeName),
      functionNode.block.steps
    );
  }

  public Location endLocation() {
    return endLocation;
  }

  @Override
  public Name name() {
    return name;
  }

  public Name returnTypeName() {
    return returnTypeName;
  }

  public Visibility visibility() {
    return visibility;
  }

  public List<ArgumentDetails> argumentDetailsList() {
    return argumentDetailsList;
  }

  public List<StatementDetails> statementDetailsList() {
    return statementDetailsList;
  }
}
