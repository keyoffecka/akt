package net.offecka.pet.akt.analyzer.syntax.defaults.details;

import net.offecka.pet.akt.analyzer.ast.ArgumentNode;
import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.support.WithName;

import static net.offecka.pet.akt.analyzer.syntax.NameFactory.NAME_FACTORY;

public class ArgumentDetails implements WithName {
  private final Name name;
  private final Name typeName;

  public ArgumentDetails(final ArgumentNode argumentNode) {
    name = NAME_FACTORY.create(argumentNode.name, argumentNode.location);
    typeName = NAME_FACTORY.create(argumentNode.type.fqn);
  }

  @Override
  public Name name() {
    return name;
  }

  public Name typeName() {
    return typeName;
  }
}
