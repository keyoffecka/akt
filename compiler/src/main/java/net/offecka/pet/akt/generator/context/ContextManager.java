package net.offecka.pet.akt.generator.context;

import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.analyzer.syntax.support.WithName;
import net.offecka.pet.akt.generator.context.MarkerContext.Markers;

import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.BOOL;
import static net.offecka.pet.akt.generator.context.MarkerContext.Markers.NEGATE;

public class ContextManager {
  private Context context = null;

  public void openMarkerContext(final Markers marker) {
    this.context = new MarkerContext(marker, context);
  }

  public void openCountContext() {
    this.context = new CountContext(context);
  }

  public void openObjectContext(final Object data) {
    this.context = new ObjectContext(context, data);
  }

  public void closeContext() {
    if (context == null) {
      throw new IllegalStateException();
    }
    context = context.parent;
  }

  public String negateIfRequired(final Type type) {
    String prefix = "";
    if (context() instanceof MarkerContext && this.<MarkerContext>context().marker == NEGATE) {
      prefix = "~";
      if (type instanceof WithName) {
        WithName withName = (WithName) type;
        if (withName.name() == BOOL) {
          prefix = "!";
        }
      }
      closeContext();
    }
    return prefix;
  }

  public <T extends Context> T context() {
    return (T) context;
  }
}
