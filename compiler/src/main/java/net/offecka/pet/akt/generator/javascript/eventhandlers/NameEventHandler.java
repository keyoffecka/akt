package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.NameEvent;
import net.offecka.pet.akt.generator.context.ContextManager;
import net.offecka.pet.akt.generator.context.CountContext;

import java.io.IOException;
import java.io.Writer;

public class NameEventHandler implements EventHandler<NameEvent> {
  private final Writer writer;
  private final ContextManager contextManager;

  public NameEventHandler(final Writer writer, final ContextManager contextManager) {
    this.writer = writer;
    this.contextManager = contextManager;
  }

  @Override
  public void handle(final NameEvent event) throws IOException {
    CountContext context = contextManager.context();
    context.inc();

    if (context.count() > 1) {
      writer.write(".");
    }

    writer.write("" + event.name);
  }
}
