package net.offecka.pet.akt.analyzer.syntax.support;

import net.offecka.pet.akt.analyzer.syntax.Name;

public interface WithName {
  Name name();
}
