package net.offecka.pet.akt.analyzer.syntax.defaults.statement.processors;

import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginStatementEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndStatementEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.StatementType;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.exceptions.CannotCast;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.result.BlockStatementResultBatch;
import net.offecka.pet.akt.errors.Location;
import net.offecka.pet.akt.events.EventPublisher;
import net.offecka.pet.akt.storage.MutableStorage;
import net.offecka.pet.akt.storage.SearchingStorage;

import static net.offecka.pet.akt.errors.ErrorContext.errorContext;

public class VoidReturnProcessor extends StatementProcessor {
  private final Location startLocation;
  private final Location expressionLocation;
  private final Type returnType;

  public VoidReturnProcessor(
    final Location startLocation,
    final Location expressionLocation,
    final Type returnType,
    final EventPublisher publisher
  ) {
    super(publisher);

    if (returnType == null) {
      throw new IllegalArgumentException();
    }

    this.startLocation = startLocation;
    this.expressionLocation = expressionLocation;
    this.returnType = returnType;
  }

  @Override
  public BlockStatementResultBatch process(final MutableStorage storage){
    publisher.publish(new BeginStatementEvent(StatementType.RETURN));

    Type leftType = this.returnType;
    Type rightType = new SearchingStorage(storage).findVoidType();
    if (leftType != rightType) {
      errorContext().addException(
        expressionLocation,
        new CannotCast(rightType, leftType)
      );
    }

    publisher.publish(new EndStatementEvent());

    return new BlockStatementResultBatch(startLocation)
      .add(returnType, expressionLocation, false);
  }
}
