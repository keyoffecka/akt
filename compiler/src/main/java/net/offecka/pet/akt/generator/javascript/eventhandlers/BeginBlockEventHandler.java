package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginBlockEvent;

import java.io.IOException;
import java.io.Writer;

public class BeginBlockEventHandler implements EventHandler<BeginBlockEvent> {
  private final Writer writer;
  private final Tab tab;

  public BeginBlockEventHandler(final Writer writer, final Tab tab) {
    this.writer = writer;
    this.tab = tab;
  }

  @Override
  public void handle(final BeginBlockEvent event) throws IOException {
    writer.write(tab + "{\n");
    tab.indent();
  }
}
