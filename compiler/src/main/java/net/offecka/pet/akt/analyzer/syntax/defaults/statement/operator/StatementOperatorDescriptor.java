package net.offecka.pet.akt.analyzer.syntax.defaults.statement.operator;

import net.offecka.pet.akt.expression.Element;
import net.offecka.pet.akt.expression.OperatorDescriptor;

public class StatementOperatorDescriptor implements Element {
  public final OperatorDescriptor descriptor;

  public StatementOperatorDescriptor(final OperatorDescriptor descriptor) {
    this.descriptor = descriptor;
  }
}
