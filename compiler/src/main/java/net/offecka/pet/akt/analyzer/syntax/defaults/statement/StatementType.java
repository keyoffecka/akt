package net.offecka.pet.akt.analyzer.syntax.defaults.statement;

public enum StatementType {
  NORMAL, RETURN, CONTINUE, BREAK
}
