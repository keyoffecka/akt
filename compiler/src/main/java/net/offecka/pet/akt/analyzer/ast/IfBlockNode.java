package net.offecka.pet.akt.analyzer.ast;

public class IfBlockNode implements StepNode {
  public final ExpressionNode expressionNode;
  public final BlockBodyNode blockBodyNode;

  public IfBlockNode(final ExpressionNode expressionNode, final BlockBodyNode blockBodyNode) {
    this.expressionNode = expressionNode;
    this.blockBodyNode = blockBodyNode;
  }
}
