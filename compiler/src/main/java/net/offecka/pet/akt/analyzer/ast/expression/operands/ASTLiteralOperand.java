package net.offecka.pet.akt.analyzer.ast.expression.operands;

import net.offecka.pet.akt.analyzer.ast.expression.ASTOperand;
import net.offecka.pet.akt.errors.Location;

public abstract class ASTLiteralOperand<T> implements ASTOperand {
  public final Location location;
  public final T value;

  protected ASTLiteralOperand(final Location location, final T value) {
    this.location = location;
    this.value = value;
  }
}
