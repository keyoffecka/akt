package net.offecka.pet.akt.analyzer.syntax.defaults.statement.processors;

import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.StatementDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.ElseEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.result.BlockStatementResultBatch;
import net.offecka.pet.akt.errors.Location;
import net.offecka.pet.akt.events.EventPublisher;
import net.offecka.pet.akt.storage.MutableStorage;

import java.util.List;

public class ElseBlockProcessor extends IfElseBlockProcessor {

  public ElseBlockProcessor(
    final Location startLocation,
    final Location endLocation,
    final List<StatementDetails> statementDetailsList,
    final BlockEventFactory blockEventFactory,
    final EventPublisher publisher
  ) {
    super(startLocation, endLocation, null, statementDetailsList, blockEventFactory, publisher);
  }

  @Override
  public BlockStatementResultBatch process(final MutableStorage storage) {
    publisher.publish(new ElseEvent());

    return super.process(storage);
  }
}
