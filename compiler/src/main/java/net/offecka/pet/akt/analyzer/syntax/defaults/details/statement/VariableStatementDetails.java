package net.offecka.pet.akt.analyzer.syntax.defaults.details.statement;

import net.offecka.pet.akt.analyzer.ast.VariableNode;
import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.defaults.support.ExpressionProvider;
import net.offecka.pet.akt.analyzer.syntax.support.WithName;
import net.offecka.pet.akt.errors.Location;

import static net.offecka.pet.akt.analyzer.syntax.NameFactory.NAME_FACTORY;

public class VariableStatementDetails extends DefaultExpressionStatementDetails implements WithName {
  private final boolean unmodifiableVariable;
  private final Name name;
  private final Name variableTypeName;
  private final Location variableLocation;

  public VariableStatementDetails(final VariableNode variableNode) {
    super(new ExpressionProvider(variableNode.expression.elements()), variableNode.expression.location);

    this.unmodifiableVariable = variableNode.unmodifiable;
    this.name = NAME_FACTORY.create(variableNode.name, variableNode.location);
    this.variableTypeName = variableNode.type.fqn == null ? null : NAME_FACTORY.create(variableNode.type.fqn);
    this.variableLocation = variableNode.location;
  }

  @Override
  public Name name() {
    return name;
  }

  public Location variableLocation() {
    return variableLocation;
  }

  public boolean unmodifiableVariable() {
    return unmodifiableVariable;
  }

  public Name variableTypeName() {
    return variableTypeName;
  }
}
