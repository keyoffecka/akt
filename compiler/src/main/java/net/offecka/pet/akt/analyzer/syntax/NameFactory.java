package net.offecka.pet.akt.analyzer.syntax;

import net.offecka.pet.akt.analyzer.ast.CallNode;
import net.offecka.pet.akt.analyzer.ast.ChainNode;
import net.offecka.pet.akt.analyzer.ast.ChainSegmentNode;
import net.offecka.pet.akt.analyzer.ast.FQNNode;
import net.offecka.pet.akt.analyzer.ast.NameNode;
import net.offecka.pet.akt.analyzer.syntax.defaults.support.ExpressionProvider;
import net.offecka.pet.akt.errors.Location;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;
import static net.offecka.pet.akt.errors.Location.NO_LOCATION;

public final class NameFactory {
  public static final NameFactory NAME_FACTORY = new NameFactory();

  private NameFactory() {
  }

  public Name create(final FQNNode fqn) {
    return create(fqn.names.stream().map(NameNode::name).collect(joining(".")), fqn.location);
  }

  public Name create(final ChainNode chain) {
    Name prev = null;
    for (int i = chain.segments.size() - 1; i >= 0; i-= 1) {
      ChainSegmentNode segment = chain.segments.get(i);
      if (segment instanceof NameNode) {
        NameNode nameNode = (NameNode) segment;
        prev = create(nameNode.name, prev, chain.location);
      } else {
        CallNode callNode = (CallNode) segment;

        List<Parameter> parameters = callNode.params.params.stream().map(
          p -> new Parameter(
              p.name == null ? null : new Name(p.name, null, chain.location),
              new ExpressionProvider(p.expression.elements()),
              p.location
          )
        ).collect(Collectors.toList());

        prev = create(callNode.name, new Parameters(parameters, callNode.params.location), prev, chain.location);
      }
    }
    return prev;
  }

  public Name create(final String fqn, final Location location) {
    if (fqn == null) {
      throw new IllegalArgumentException();
    }

    String[] names = fqn.split("\\.", 2);
    Name child = null;
    if (names.length > 1) {
      try {
        child = create(names[1], location);
      } catch (final IllegalArgumentException ex) {
        throw new IllegalArgumentException("Invalid name [" + fqn + "]");
      }
    }
    return new Name(names[0], child, location);
  }

  public Name create(final String name, final Name child, final Location location) {
    return new Name(name, child, location);
  }

  public Call create(final String name, final Parameters parameters, final Name child, final Location location) {
    return new Call(name, parameters, child, location);
  }

  public Name create(final Name first, final Name postfix) {
    if (first instanceof Call) {
      Call call = (Call) first;
      return create(call.name, call.parameters, postfix, first.location());
    } else {
      return create(first.name, postfix, first.location());
    }
  }

  public Name concat(final Name... names) {
    Name prev = null;

    for (final Name name : Stream.of(names).map(n -> {
      List<Name> l = new ArrayList<>();
      while (n != null) {
        l.add(0, n.first());
        n = n.postfix();
      }
      return l;
    }).reduce(new ArrayList<>(), (r, e) -> {
      r.addAll(0, e);
      return r;
    })) {
      prev = create(name, prev);
    }

    if (prev == null) {
      throw new IllegalArgumentException();
    }

    return prev;
  }
}
