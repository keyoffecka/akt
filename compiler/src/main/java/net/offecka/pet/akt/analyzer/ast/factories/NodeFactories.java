package net.offecka.pet.akt.analyzer.ast.factories;

public final class NodeFactories {
  public final ChainNodeFactory chainNodeFactory = new ChainNodeFactory();
  public final FQNNodeFactory fqnNodeFactory = new FQNNodeFactory();
  public final TypeNodeFactory typeNodeFactory = new TypeNodeFactory();
  public final ScopeFactory scopeFactory = new ScopeFactory();
  public final TermNodeFactory termNodeFactory = new TermNodeFactory();
  public final AssignmentNodeFactory assignmentNodeFactory = new AssignmentNodeFactory();
  public final VariableNodeFactory variableNodeFactory = new VariableNodeFactory();
  public final BlockNodeFactory blockNodeFactory = new BlockNodeFactory();
  public final FunctionNodeFactory functionNodeFactory = new FunctionNodeFactory();
  public final BlockBodyNodeFactory blockBodyNodeFactory = new BlockBodyNodeFactory();
  public final IfBlockBodyNodeFactory ifBlockBodyNodeFactory = new IfBlockBodyNodeFactory();

  public static final NodeFactories NODE_FACTORIES = new NodeFactories();

  private NodeFactories() {
  }
}
