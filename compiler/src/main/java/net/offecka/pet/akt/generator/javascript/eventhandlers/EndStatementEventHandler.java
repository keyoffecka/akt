package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndStatementEvent;

import java.io.IOException;
import java.io.Writer;

public class EndStatementEventHandler implements EventHandler<EndStatementEvent> {
  private final Writer writer;

  public EndStatementEventHandler(final Writer writer) {
    this.writer = writer;
  }

  @Override
  public void handle(final EndStatementEvent event) throws IOException {
    writer.write(";\n");
  }
}
