package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginIfElseBlockEvent;

import java.io.IOException;
import java.io.Writer;

public class BeginIfElseBlockEventHandler implements EventHandler<BeginIfElseBlockEvent> {
  private final Writer writer;
  private final Tab tab;

  public BeginIfElseBlockEventHandler(final Writer writer, final Tab tab) {
    this.writer = writer;
    this.tab = tab;
  }

  @Override
  public void handle(final BeginIfElseBlockEvent event) throws IOException {
    writer.write(" {\n");
    tab.indent();
  }
}
