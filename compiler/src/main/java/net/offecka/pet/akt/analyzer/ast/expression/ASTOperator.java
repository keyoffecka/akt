package net.offecka.pet.akt.analyzer.ast.expression;

import net.offecka.pet.akt.errors.Location;

public class ASTOperator implements ASTElement {
  public final Location location;
  public final ASTOperators operator;

  public ASTOperator(final Location location, final ASTOperators operator) {
    this.location = location;
    this.operator = operator;
  }

  public enum ASTOperators {
    NOT, PLUS, MINUS,
    MUL, DIV, MOD,
    ADD, SUB,
    BIT_AND, BIT_OR, BIT_XOR,
    EQUALS_TO, NOT_EQUALS_TO, IS, IS_NOT,
    AND, OR
  }
}
