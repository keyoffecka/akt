package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.AssignmentEvent;

import java.io.IOException;
import java.io.Writer;

public class AssignmentEventHandler implements EventHandler<AssignmentEvent> {
  private final Writer writer;

  public AssignmentEventHandler(final Writer writer) {
    this.writer = writer;
  }

  @Override
  public void handle(final AssignmentEvent event) throws IOException {
    writer.write(" = ");
  }
}
