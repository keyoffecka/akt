package net.offecka.pet.akt.analyzer.ast;

import net.offecka.pet.akt.errors.Location;

import java.util.List;

public class IfBlockBodyNode implements StepNode {
  public final Location location;
  public final List<IfBlockNode> ifBlockNodes;

  public IfBlockBodyNode(final Location location, final List<IfBlockNode> ifBlockNodes) {
    this.location = location;
    this.ifBlockNodes = ifBlockNodes;
  }
}
