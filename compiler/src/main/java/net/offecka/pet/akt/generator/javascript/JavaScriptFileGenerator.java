package net.offecka.pet.akt.generator.javascript;

import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.events.DefaultEventSource;
import net.offecka.pet.akt.events.EventPublisher;
import net.offecka.pet.akt.generator.Unit;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class JavaScriptFileGenerator implements Unit {
  public static final String AKT_FILENAME_SUFFIX = ".akt";
  public static final String JS_FILENAME_SUFFIX = ".js";

  private final String sourceDirPath;
  private final String destinationDirPath;
  private final String resourceName;
  private final Name name;
  private final Writer writer;
  private final DefaultEventSource publisher;

  public JavaScriptFileGenerator(
    final String sourceDirPath,
    final String destinationDirPath,
    final Name name
  ) throws IOException {
    this.sourceDirPath = Paths.get(sourceDirPath).toFile().getCanonicalPath();
    this.destinationDirPath = Paths.get(destinationDirPath).toFile().getCanonicalPath();
    this.name = name;
    this.resourceName = buildResourceFile(null, this.name) + AKT_FILENAME_SUFFIX;

    this.publisher = new DefaultEventSource();

    String destinationResourcePath = buildResourceFile(this.destinationDirPath, this.name) + JS_FILENAME_SUFFIX;

    Files.createDirectories(Paths.get(destinationResourcePath).getParent());

    writer = new FileWriter(destinationResourcePath);
    writer.write("module.exports = {}\n");

    destinationResourcePath = destinationResourcePath.substring(this.destinationDirPath.length() + 1);

    String relativePathToTopDir = ".";
    Path path = Paths.get(destinationResourcePath).getParent();
    while (path != null) {
      relativePathToTopDir = Paths.get(relativePathToTopDir, "..").toString();
      path = path.getParent();
    }
    publisher.addListener(new JavaScriptEventListener(writer, relativePathToTopDir));
  }

  private String buildResourceFile(final String dirPath, final Name name) throws IOException {
    String resourcePath = null;

    Name current = name;
    while (current != null) {
      Path path = resourcePath == null
        ? Paths.get(current.name)
        : Paths.get(resourcePath, current.name);
      resourcePath = path.toFile().getPath();
      current = current.child;
    }
    if (dirPath != null) {
      resourcePath = Paths.get(dirPath, resourcePath).toFile().getCanonicalPath();
    }

    return resourcePath;
  }

  @Override
  public Name name() {
    return name;
  }

  @Override
  public Reader reader() throws IOException {
    return new FileReader(buildResourceFile(this.sourceDirPath, this.name) + AKT_FILENAME_SUFFIX);
  }

  @Override
  public String resourceName() {
    return resourceName;
  }

  @Override
  public EventPublisher publisher() {
    return publisher;
  }

  @Override
  public Unit unit(final Name name) throws IOException {
    return new JavaScriptFileGenerator(sourceDirPath, destinationDirPath, name);
  }

  @Override
  public void close() throws IOException {
    writer.close();
  }
}
