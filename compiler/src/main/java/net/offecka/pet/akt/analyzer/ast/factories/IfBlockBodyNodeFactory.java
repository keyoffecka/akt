package net.offecka.pet.akt.analyzer.ast.factories;

import net.offecka.pet.akt.analyzer.ast.IfBlockBodyNode;
import net.offecka.pet.akt.analyzer.ast.IfBlockNode;
import net.offecka.pet.akt.grammar.AktParser.ElifBlockBodyContext;
import net.offecka.pet.akt.grammar.AktParser.IfBlockBodyContext;

import java.util.ArrayList;
import java.util.List;

import static net.offecka.pet.akt.analyzer.ast.factories.NodeFactories.NODE_FACTORIES;
import static net.offecka.pet.akt.errors.Location.at;

public class IfBlockBodyNodeFactory {
  IfBlockBodyNodeFactory() {
  }

  public IfBlockBodyNode create(final IfBlockBodyContext ifBlockBody) {
    List<IfBlockNode> ifBlockNodes = new ArrayList<>();

    ifBlockNodes.add(
      new IfBlockNode(
        NODE_FACTORIES.chainNodeFactory.create(ifBlockBody.expression()),
        NODE_FACTORIES.blockBodyNodeFactory.create(ifBlockBody.blockBody())
      )
    );

    for (final ElifBlockBodyContext elifBlockBodyContext : ifBlockBody.elifBlockBody()) {
      ifBlockNodes.add(
        new IfBlockNode(
          NODE_FACTORIES.chainNodeFactory.create(elifBlockBodyContext.expression()),
          NODE_FACTORIES.blockBodyNodeFactory.create(elifBlockBodyContext.blockBody())
        )
      );
    }

    if (ifBlockBody.elseBlockBody() != null) {
      ifBlockNodes.add(
        new IfBlockNode(
          null,
          NODE_FACTORIES.blockBodyNodeFactory.create(ifBlockBody.elseBlockBody().blockBody())
        )
      );
    }

    return new IfBlockBodyNode(at(ifBlockBody.IF().getSymbol()), ifBlockNodes);
  }
}
