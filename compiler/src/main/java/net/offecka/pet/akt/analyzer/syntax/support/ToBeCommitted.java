package net.offecka.pet.akt.analyzer.syntax.support;

public interface ToBeCommitted {
  void commit();
}
