package net.offecka.pet.akt.analyzer.ast.expression.operands;

import net.offecka.pet.akt.errors.Location;

public abstract class ASTNumLiteralOperand<T> extends ASTLiteralOperand<T> {
  protected ASTNumLiteralOperand(final Location location, final T value) {
    super(location, value);
  }
}
