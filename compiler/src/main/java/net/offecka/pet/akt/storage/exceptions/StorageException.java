package net.offecka.pet.akt.storage.exceptions;

import net.offecka.pet.akt.analyzer.syntax.Name;

public class StorageException extends Exception {
  public final Name name;

  public StorageException(final String message, final Name name) {
    super(message);

    this.name = name;
  }
}
