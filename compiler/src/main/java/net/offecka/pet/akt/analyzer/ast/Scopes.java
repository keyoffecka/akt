package net.offecka.pet.akt.analyzer.ast;

public enum Scopes {
  PUBLIC,
  PRIVATE
}
