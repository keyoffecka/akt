package net.offecka.pet.akt.analyzer.syntax.defaults.initializers;

import net.offecka.pet.akt.analyzer.syntax.support.ToBeCommitted;

public class NothingToBeCommitted implements ToBeCommitted {
  @Override
  public void commit() {
    //nothing to commit
  }
}
