package net.offecka.pet.akt.analyzer.syntax.defaults.initializers;

import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.analyzer.syntax.Variable;
import net.offecka.pet.akt.analyzer.syntax.defaults.DefaultVariable;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.ConstantDetails;
import net.offecka.pet.akt.storage.MutableStorage;
import net.offecka.pet.akt.storage.exceptions.StorageException;

import static net.offecka.pet.akt.storage.FinderType.LOCAL;
import static net.offecka.pet.akt.storage.Visibility.PUBLIC;

public class ConstantInitializer {
  private final MutableStorage storage;

  public ConstantInitializer(final MutableStorage storage) {
    this.storage = storage;
  }

  public void init(final ConstantDetails constantDetails) {
    try {
      Type type = storage.find(LOCAL, constantDetails.typeName).item(Type.class);
      Variable variable = new DefaultVariable(true, constantDetails.name(), type);
      storage.put(PUBLIC, constantDetails.name(), variable);
    } catch (final StorageException ex) {
      throw new IllegalStateException(ex);
    }
  }
}
