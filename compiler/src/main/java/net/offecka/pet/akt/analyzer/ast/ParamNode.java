package net.offecka.pet.akt.analyzer.ast;

import net.offecka.pet.akt.errors.Location;

public class ParamNode implements Node {
  public final Location location;
  public final String name;
  public final ExpressionNode expression;

  public ParamNode(final Location location, final String name, final ExpressionNode expression) {
    this.location = location;
    this.name = name;
    this.expression = expression;
  }
}
