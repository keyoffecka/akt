package net.offecka.pet.akt.analyzer.syntax.defaults;

import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.analyzer.syntax.Variable;
import net.offecka.pet.akt.storage.FinderType;
import net.offecka.pet.akt.storage.Result;
import net.offecka.pet.akt.storage.Storage;
import net.offecka.pet.akt.storage.exceptions.StorageException;

public class DefaultVariable implements Variable {
  private final boolean unmodifiable;
  private final Name name;
  private final Type type;

  public DefaultVariable(
    final boolean unmodifiable,
    final Name name,
    final Type type
  ) {
    this.unmodifiable = unmodifiable;
    this.name = name;
    this.type = type;
  }

  @Override
  public Storage create() {
    return storage().create();
  }

  @Override
  public boolean unmodifiable() {
    return unmodifiable;
  }

  @Override
  public Name name() {
    return name;
  }

  @Override
  public Type type() {
    return type;
  }

  @Override
  public Result find(final FinderType finderType, final Name name) throws StorageException {
    return storage().find(finderType, name);
  }

  private Storage storage() {
    return (Storage) type;
  }
}
