package net.offecka.pet.akt.analyzer.ast.expression.operands;

import net.offecka.pet.akt.errors.Location;

public class ASTStrLiteralOperand extends ASTLiteralOperand<String> {
  public ASTStrLiteralOperand(final Location location, final String value) {
    super(location, value);
  }
}
