package net.offecka.pet.akt.storage;

public enum FinderType {
  LOCAL,
  SUB,
  FOREIGN
}
