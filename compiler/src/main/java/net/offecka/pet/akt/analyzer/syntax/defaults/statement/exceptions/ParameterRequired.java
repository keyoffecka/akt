package net.offecka.pet.akt.analyzer.syntax.defaults.statement.exceptions;

import net.offecka.pet.akt.analyzer.syntax.ModelException;
import net.offecka.pet.akt.analyzer.syntax.Name;

public class ParameterRequired extends ModelException {
  public final Name name;

  public ParameterRequired(final Name name) {
    super("Parameter [" + name + "] is required");

    this.name = name;
  }
}
