package net.offecka.pet.akt.storage;

import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.storage.exceptions.ExistedInChild;
import net.offecka.pet.akt.storage.exceptions.Exists;
import net.offecka.pet.akt.storage.exceptions.ExistsInStorage;
import net.offecka.pet.akt.storage.exceptions.IllegalAccess;
import net.offecka.pet.akt.storage.exceptions.NotFound;
import net.offecka.pet.akt.storage.exceptions.StorageException;
import net.offecka.pet.akt.storage.exceptions.UnexpectedType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import static net.offecka.pet.akt.analyzer.syntax.NameFactory.NAME_FACTORY;
import static net.offecka.pet.akt.storage.FinderType.FOREIGN;
import static net.offecka.pet.akt.storage.FinderType.LOCAL;
import static net.offecka.pet.akt.storage.FinderType.SUB;
import static net.offecka.pet.akt.storage.Visibility.PRIVATE;
import static net.offecka.pet.akt.storage.Visibility.PUBLIC;

public class DefaultMutableStorage implements MutableStorage {
  private final DefaultMutableStorage parent;
  private final List<DefaultMutableStorage> children = new ArrayList<>();

  private final Map<Name, Bag> map = new HashMap<>();

  public DefaultMutableStorage() {
    this(null);
  }

  private DefaultMutableStorage(final DefaultMutableStorage parent) {
    this.parent = parent;
  }

  @Override
  public MutableStorage create() {
    DefaultMutableStorage storage = new DefaultMutableStorage(this);
    children.add(storage);
    return storage;
  }

  @Override
  public Result find(
    final FinderType finderType,
    final Name name
  ) throws StorageException {
    if (name == null) {
      throw new IllegalArgumentException();
    }

    Bag bag = map.get(NAME_FACTORY.create(name.name, name.location()));
    if (bag == null) {
      if (parent == null) {
        throw new NotFound(name.first());
      }
      return parent.find(finderType, name);
    } else {
      if (finderType == SUB && bag.visibility == PRIVATE) {
        throw new IllegalAccess(bag.name, bag.visibility, finderType);
      } else if (finderType == FOREIGN && bag.visibility != PUBLIC) {
        throw new IllegalAccess(bag.name, bag.visibility, finderType);
      }
      if (name.child == null) {
        return new Result(this, bag.visibility, bag.name, bag.item);
      }
      if (bag.item instanceof Storage) {
        Storage storage = (Storage) bag.item;
        return storage.find(finderType, name.child);
      }
      throw new UnexpectedType(bag.name, bag.item.getClass(), Storage.class);
    }
  }

  @Override
  public void put(
    final Visibility visibility,
    final Name name,
    final Item item
  ) throws Exists {
    if (visibility == null) {
      throw new IllegalArgumentException();
    }
    if (item == null) {
      throw new IllegalArgumentException();
    }
    if (name.child != null) {
      throw new IllegalStateException();
    }

    Name oldName = null;
    try {
      oldName = find(LOCAL, name).name;
    } catch (final NotFound ex) {
      //nop
    } catch (final IllegalAccess ex) {
      oldName = ex.name;
    } catch (final StorageException ex) {
      throw new IllegalStateException(ex);
    }

    if (oldName != null) {
      throw new ExistsInStorage(name, oldName);
    }

    FoundBag foundBag = find(children, name);
    if (foundBag != null) {
      foundBag.child.map.remove(foundBag.bag.name);
    }

    map.put(NAME_FACTORY.create(name.name, name.location()), new Bag(name, item, visibility));

    if (foundBag != null) {
      throw new ExistedInChild(foundBag.bag.name, name);
    }
  }

  private FoundBag find(final List<DefaultMutableStorage> children, final Name name) {
    FoundBag foundBag = null;

    for (final DefaultMutableStorage child : children) {
      for (final Entry<Name, Bag> entry : child.map.entrySet()) {
        if (entry.getKey().equals(name)) {
          foundBag = new FoundBag(entry.getValue(), child);
          break;
        }
      }
      if (foundBag != null) {
        break;
      }
      foundBag = find(child.children, name);
      if (foundBag != null) {
        break;
      }
    }

    return foundBag;
  }

  private static final class FoundBag {
    private final Bag bag;
    private final DefaultMutableStorage child;

    private FoundBag(final Bag bag, final DefaultMutableStorage child) {
      this.bag = bag;
      this.child = child;
    }
  }
}
