package net.offecka.pet.akt.storage;

import net.offecka.pet.akt.analyzer.ast.Scopes;

public class VisibilityFactory {
  public final Visibility create(final Scopes scope) {
    switch (scope) {
      case PUBLIC:
        return Visibility.PUBLIC;
      case PRIVATE:
        return Visibility.PRIVATE;
      default:
        throw new IllegalStateException("[" + scope + "] unsupported");
    }
  }
}
