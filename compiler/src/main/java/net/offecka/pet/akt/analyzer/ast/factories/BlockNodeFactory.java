package net.offecka.pet.akt.analyzer.ast.factories;

import net.offecka.pet.akt.analyzer.ast.StepNode;
import net.offecka.pet.akt.grammar.AktParser.BlockContext;

import static net.offecka.pet.akt.analyzer.ast.factories.NodeFactories.NODE_FACTORIES;

public class BlockNodeFactory {
  BlockNodeFactory() {
  }

  public StepNode create(final BlockContext block) {
    StepNode blockBodyNode = null;

    if (block.blockBody() != null) {
      blockBodyNode = NODE_FACTORIES.blockBodyNodeFactory.create(block.blockBody());
    } else if (block.ifBlockBody() != null) {
      blockBodyNode = NODE_FACTORIES.ifBlockBodyNodeFactory.create(block.ifBlockBody());
    } else {
      throw new UnsupportedOperationException();
    }

    return blockBodyNode;
  }
}
