package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.AliasEvent;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class AliasEventHandler implements EventHandler<AliasEvent> {
  private final List<Path> paths = new ArrayList<>();

  private final String relativePathToTopDir;
  private final Writer writer;

  public AliasEventHandler(final String relativePathToTopDir, final Writer writer) {
    this.relativePathToTopDir = relativePathToTopDir;
    this.writer = writer;
  }

  @Override
  public void handle(final AliasEvent e) throws IOException {
    if (e.name.name.equals("browser")
      && e.name.child != null && e.name.child.name.equals("modules")
      && e.name.child.child != null
      && e.name.child.child.child == null
    ) {
      importModule(e);
    } else if (e.name.name.equals("browser")
      && e.name.child != null && e.name.child.name.equals("globals")
      && e.name.child.child != null
      && e.name.child.child.child == null
    ) {
      importGlobalVariable(e);
    } else if (e.name.name.equals("sys")) {
      importSystemVariable();
    } else {
      importNamespace(e);
    }
  }

  private void importNamespace(final AliasEvent e) throws IOException {
    Path relativePath = Paths.get(relativePathToTopDir);
    Name prev = e.name;
    Name name = prev.child;
    while (name != null) {
      relativePath = Paths.get(relativePath.toString(), prev.name);
      prev = name;
      name = name.child;
    }
    if (!paths.contains(relativePath)) {
      paths.add(relativePath);
      writer.write("const " + relativePath.getFileName() + " = require('" + relativePath + ".js');\n");
    }
    writer.write("const " + e.alias + " = " + relativePath.getFileName() + "." + e.name.last() + ";\n");
  }

  private void importGlobalVariable(final AliasEvent e) throws IOException {
    String objectName = e.name.child.child.name;
    if (!e.alias.name.equals(objectName)) {
      writer.write("const " + e.alias.name + " = " + objectName + ";\n");
    }
  }

  private void importSystemVariable() {
  }

  private void importModule(final AliasEvent e) throws IOException {
    String moduleName = e.name.child.child.name;

    String alias = moduleName;
    if (!e.alias.name.equals(moduleName)) {
      alias = e.alias.name;
    }

    writer.write("const " + alias + " = require('" + moduleName + "');\n");
  }
}
