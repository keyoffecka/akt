package net.offecka.pet.akt.analyzer.syntax.defaults.details.statement;

import net.offecka.pet.akt.errors.Location;

import java.util.List;

import static java.util.List.copyOf;

public class IfDetails implements StatementDetails {
  private final Location ifLocation;
  private final List<IfBlockDetails> ifBlockDetailsList;

  public IfDetails(
    final List<IfBlockDetails> ifBlockDetailsList,
    final Location ifLocation
  ) {
    if (ifBlockDetailsList.isEmpty()) {
      throw new IllegalArgumentException();
    }

    this.ifBlockDetailsList = copyOf(ifBlockDetailsList);
    this.ifLocation = ifLocation;
  }

  public Location ifLocation() {
    return ifLocation;
  }

  public List<IfBlockDetails> ifBlockDetailsList() {
    return ifBlockDetailsList;
  }
}
