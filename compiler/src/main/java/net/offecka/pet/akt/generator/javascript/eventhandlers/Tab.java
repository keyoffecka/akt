package net.offecka.pet.akt.generator.javascript.eventhandlers;

public class Tab {
  private static final String TAB = "  ";

  private final StringBuilder tab = new StringBuilder();

  public void indent() {
    tab.append(TAB);
  }

  public void unindent() {
    tab.delete(tab.length() - TAB.length(), tab.length());
  }

  @Override
  public String toString() {
    return tab.toString();
  }
}
