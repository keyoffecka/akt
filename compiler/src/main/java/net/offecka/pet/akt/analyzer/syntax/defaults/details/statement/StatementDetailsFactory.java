package net.offecka.pet.akt.analyzer.syntax.defaults.details.statement;

import net.offecka.pet.akt.analyzer.ast.AssignmentNode;
import net.offecka.pet.akt.analyzer.ast.BlockBodyNode;
import net.offecka.pet.akt.analyzer.ast.ExpressionNode;
import net.offecka.pet.akt.analyzer.ast.IfBlockBodyNode;
import net.offecka.pet.akt.analyzer.ast.IfBlockNode;
import net.offecka.pet.akt.analyzer.ast.NamespaceVariableNode;
import net.offecka.pet.akt.analyzer.ast.ReturnNode;
import net.offecka.pet.akt.analyzer.ast.StepNode;
import net.offecka.pet.akt.analyzer.ast.VariableNode;
import net.offecka.pet.akt.analyzer.ast.VoidReturnNode;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.returntypeproviders.ReturnTypeProvider;
import net.offecka.pet.akt.analyzer.syntax.defaults.support.ExpressionProvider;

import java.util.ArrayList;
import java.util.List;

public class StatementDetailsFactory {
  public List<StatementDetails> create(final ReturnTypeProvider returnTypeProvider, final List<StepNode> steps) {
    List<StatementDetails> result = new ArrayList<>();
    for (final StepNode stepNode : steps) {
      if (stepNode instanceof NamespaceVariableNode) {
        result.add(new NamespaceVariableStatementDetails((NamespaceVariableNode) stepNode));
      } else if (stepNode instanceof VariableNode) {
        result.add(new VariableStatementDetails((VariableNode) stepNode));
      } else if (stepNode instanceof AssignmentNode) {
        result.add(new AssignmentStatementDetails((AssignmentNode) stepNode));
      } else if (stepNode instanceof ExpressionNode) {
        ExpressionNode expressionNode = (ExpressionNode) stepNode;
        result.add(new DefaultExpressionStatementDetails(
          new ExpressionProvider(expressionNode.elements()), expressionNode.location
        ));
      } else if (stepNode instanceof ReturnNode) {
        ReturnNode returnNode = (ReturnNode) stepNode;
        result.add(new ReturnStatementDetails(
          returnTypeProvider,
          new ExpressionProvider(returnNode.expressionNode.elements()),
          returnNode.location,
          returnNode.expressionNode.location
        ));
      } else if (stepNode instanceof VoidReturnNode) {
        VoidReturnNode returnNode = (VoidReturnNode) stepNode;
        result.add(new VoidReturnStatementDetails(
          returnTypeProvider,
          returnNode.location,
          returnNode.endLocation
        ));
      } else if (stepNode instanceof BlockBodyNode) {
        BlockBodyNode innerBlockBodyNode = (BlockBodyNode) stepNode;
        result.add(new BlockDetails(
          innerBlockBodyNode.startLocation,
          innerBlockBodyNode.endLocation,
          create(returnTypeProvider, innerBlockBodyNode.steps)
        ));
      } else if (stepNode instanceof IfBlockBodyNode) {
        IfBlockBodyNode ifBlockBodyNode = (IfBlockBodyNode) stepNode;

        List<IfBlockDetails> ifBlockDetailsList = new ArrayList<>();
        for (final IfBlockNode ifBlockNode : ifBlockBodyNode.ifBlockNodes) {
          ExpressionStatementDetails expressionDetailsList = ifBlockNode.expressionNode == null
            ? null
            : new DefaultExpressionStatementDetails(
              new ExpressionProvider(ifBlockNode.expressionNode.elements()),
              ifBlockNode.expressionNode.location
            );
          BlockDetails blockDetails = new BlockDetails(
            ifBlockNode.blockBodyNode.startLocation,
            ifBlockNode.blockBodyNode.endLocation,
            create(returnTypeProvider, ifBlockNode.blockBodyNode.steps)
          );
          ifBlockDetailsList.add(new IfBlockDetails(
            expressionDetailsList,
            blockDetails
          ));
        }

        result.add(new IfDetails(
          ifBlockDetailsList,
          ifBlockBodyNode.location
        ));
      } else {
        throw new UnsupportedOperationException();
      }
    }
    return result;
  }
}
