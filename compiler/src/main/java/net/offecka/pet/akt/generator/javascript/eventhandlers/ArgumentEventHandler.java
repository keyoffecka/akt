package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.ArgumentEvent;
import net.offecka.pet.akt.generator.context.ContextManager;
import net.offecka.pet.akt.generator.context.CountContext;

import java.io.IOException;
import java.io.Writer;

public class ArgumentEventHandler implements EventHandler<ArgumentEvent> {
  private final Writer writer;
  private final ContextManager contextManager;

  public ArgumentEventHandler(final Writer writer, final ContextManager contextManager) {
    this.writer = writer;
    this.contextManager = contextManager;
  }

  @Override
  public void handle(final ArgumentEvent event) throws IOException {
    if (contextManager.<CountContext>context().count() > 0) {
      writer.write(", ");
    }
    writer.write(event.name.name);
  }
}
