package net.offecka.pet.akt.storage;

import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.storage.exceptions.StorageException;

public interface Storage {
  Result find(FinderType finderType, Name name) throws StorageException;
  Storage create();
}
