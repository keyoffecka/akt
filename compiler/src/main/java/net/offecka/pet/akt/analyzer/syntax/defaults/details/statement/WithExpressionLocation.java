package net.offecka.pet.akt.analyzer.syntax.defaults.details.statement;

import net.offecka.pet.akt.errors.Location;

public interface WithExpressionLocation {
  Location expressionLocation();
}
