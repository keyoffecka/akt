package net.offecka.pet.akt.analyzer.syntax.defaults.details;

import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.support.WithName;
import net.offecka.pet.akt.storage.Visibility;

import java.util.List;

public class TypeDetails implements WithName {
  private final Visibility visibility;
  private final Name name;
  private final List<FunctionDetails> functionDetailsList;

  public TypeDetails(
    final Visibility visibility,
    final Name name,
    final List<FunctionDetails> functionDetailsList
  ) {
    this.visibility = visibility;
    this.name = name;
    this.functionDetailsList = functionDetailsList;
  }

  public Visibility visibility() {
    return visibility;
  }

  @Override
  public Name name() {
    return name;
  }

  public List<FunctionDetails> functionDetailsList() {
    return functionDetailsList;
  }
}
