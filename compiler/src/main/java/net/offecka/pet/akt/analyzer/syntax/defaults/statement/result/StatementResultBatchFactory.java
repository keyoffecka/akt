package net.offecka.pet.akt.analyzer.syntax.defaults.statement.result;

import net.offecka.pet.akt.errors.Location;

import java.util.List;

public class StatementResultBatchFactory {
  public BlockStatementResultBatch create(
    final Location startLocation,
    final List<BlockStatementResult> blockStatementResults,
    final boolean hasUnreachableStatements
  ) {
    BlockStatementResultBatch blockStatementResultBatch = new BlockStatementResultBatch(startLocation);
    for (final BlockStatementResult blockStatementResult : blockStatementResults) {
      blockStatementResultBatch.add(
        blockStatementResult.type,
        blockStatementResult.returnStatementExpressionLocation,
        hasUnreachableStatements
      );
    }
    return blockStatementResultBatch;
  }
}
