package net.offecka.pet.akt.analyzer.syntax.defaults.events;

import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.events.Event;

public class VariableEvent implements Event {
  public final Boolean unmodifiable;
  public final Name name;
  public final Type leftType;
  public final Type rightType;

  public VariableEvent(
    final Name name,
    final Type leftType,
    final Type rightType,
    final Boolean unmodifiable
  ) {
    this.unmodifiable = unmodifiable;
    this.name = name;
    this.leftType = leftType;
    this.rightType = rightType;
  }
}
