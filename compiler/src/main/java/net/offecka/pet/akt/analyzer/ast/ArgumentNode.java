package net.offecka.pet.akt.analyzer.ast;

import net.offecka.pet.akt.errors.Location;

public class ArgumentNode implements Node {
  public final Location location;
  public final String name;
  public final TypeNode type;
  public final ExpressionNode defaultValue;

  public ArgumentNode(
    final Location location,
    final String name,
    final TypeNode type,
    final ExpressionNode defaultValue
  ) {
    this.location = location;
    this.name = name;
    this.type = type;
    this.defaultValue = defaultValue;
  }
}
