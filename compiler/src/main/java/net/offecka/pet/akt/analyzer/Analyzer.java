package net.offecka.pet.akt.analyzer;

import net.offecka.pet.akt.analyzer.collector.Collector;
import net.offecka.pet.akt.analyzer.collector.UnitParser;
import net.offecka.pet.akt.analyzer.syntax.Any;
import net.offecka.pet.akt.analyzer.syntax.Namespace;
import net.offecka.pet.akt.analyzer.syntax.defaults.DefaultNamespace;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.ConstantDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.TypeDetails;
import net.offecka.pet.akt.generator.Unit;
import net.offecka.pet.akt.grammar.AktParser.NamespaceContext;
import net.offecka.pet.akt.storage.exceptions.StorageException;

import static java.util.List.of;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.ANY;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.BOOL;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.FALSE_CONST;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.INT;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.NOTHING;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.NULL;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.NULL_CONST;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.REAL;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.STR;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.TRUE_CONST;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.VOID;
import static net.offecka.pet.akt.errors.ErrorContext.errorContext;
import static net.offecka.pet.akt.events.NoPublisher.NO_PUBLISHER;
import static net.offecka.pet.akt.storage.Visibility.PUBLIC;

public class Analyzer {
  private final UnitParser unitParser = new UnitParser();

  public Namespace compile(final Namespace oldRoot, final Unit unit) {
    Namespace root = oldRoot;

    errorContext().begin(unit);
    try {
      NamespaceContext namespaceContext = unitParser.parse(unit);

      if (root == null) {
        DefaultNamespace namespace = new DefaultNamespace(null, NO_PUBLISHER);

        try {
          DefaultNamespace sys = new DefaultNamespace(Collector.SYS, NO_PUBLISHER);
          namespace.put(PUBLIC, sys.name(), sys);

          DefaultNamespace browser = new DefaultNamespace(Collector.BROWSER, NO_PUBLISHER);
          namespace.put(PUBLIC, browser.name(), browser);

          Any browserModules = new Any(Collector.MODULES);
          browser.put(PUBLIC, Collector.MODULES, browserModules);

          Any browserGlobals = new Any(Collector.MODULES);
          browser.put(PUBLIC, Collector.GLOBALS, browserGlobals);

          DefaultNamespace node = new DefaultNamespace(Collector.NODE, NO_PUBLISHER);
          namespace.put(PUBLIC, node.name(), node);

          Any nodeModules = new Any(Collector.MODULES);
          node.put(PUBLIC, nodeModules.name(), nodeModules);

          sys.addType(new TypeDetails(PUBLIC, VOID, of()));
          sys.addType(new TypeDetails(PUBLIC, INT, of()));
          sys.addType(new TypeDetails(PUBLIC, REAL, of()));
          sys.addType(new TypeDetails(PUBLIC, STR, of()));
          sys.addType(new TypeDetails(PUBLIC, BOOL, of()));
          sys.addType(new TypeDetails(PUBLIC, ANY, of()));
          sys.addType(new TypeDetails(PUBLIC, NULL, of()));
          sys.addType(new TypeDetails(PUBLIC, NOTHING, of()));

          sys.addConstant(new ConstantDetails(TRUE_CONST, BOOL));
          sys.addConstant(new ConstantDetails(FALSE_CONST, BOOL));
          sys.addConstant(new ConstantDetails(NULL_CONST, NULL));
        } catch (final StorageException ex) {
          throw new IllegalStateException(ex);
        }

        root = namespace;
      }

      if (namespaceContext != null) {
        Collector collector = new Collector(root, unitParser);
        collector.collect(unit, namespaceContext);
      }
    } finally {
      errorContext().end();
    }

    return root;
  }
}
