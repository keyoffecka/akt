package net.offecka.pet.akt.analyzer.syntax.defaults.statement.exceptions;

import net.offecka.pet.akt.analyzer.syntax.ModelException;
import net.offecka.pet.akt.analyzer.syntax.Name;

public class CannotModify extends ModelException {
  public final Name name;

  public CannotModify(final Name name) {
    super("[" + name + "] is unmodifiable");

    this.name = name;
  }
}
