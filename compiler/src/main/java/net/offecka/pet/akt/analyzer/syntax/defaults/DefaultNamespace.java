package net.offecka.pet.akt.analyzer.syntax.defaults;

import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.Namespace;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.AliasDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.ConstantDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.FunctionDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.TypeDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.StatementDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.initializers.AliasInitializer;
import net.offecka.pet.akt.analyzer.syntax.defaults.initializers.ConstantInitializer;
import net.offecka.pet.akt.analyzer.syntax.defaults.initializers.FunctionInitializer;
import net.offecka.pet.akt.analyzer.syntax.defaults.initializers.StatementInitializer;
import net.offecka.pet.akt.analyzer.syntax.defaults.initializers.TypeInitializer;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.result.BlockStatementResultBatch;
import net.offecka.pet.akt.analyzer.syntax.support.ToBeCommitted;
import net.offecka.pet.akt.events.EventPublisher;
import net.offecka.pet.akt.storage.DefaultMutableStorage;
import net.offecka.pet.akt.storage.FinderType;
import net.offecka.pet.akt.storage.Item;
import net.offecka.pet.akt.storage.MutableStorage;
import net.offecka.pet.akt.storage.Result;
import net.offecka.pet.akt.storage.Visibility;
import net.offecka.pet.akt.storage.exceptions.Exists;
import net.offecka.pet.akt.storage.exceptions.StorageException;

public class DefaultNamespace implements Namespace, MutableStorage {
  private final MutableStorage storage = new DefaultMutableStorage();

  private final Name name;
  private final AliasInitializer aliasInitializer;
  private final ConstantInitializer constantInitializer;
  private final FunctionInitializer functionInitializer;
  private final StatementInitializer statementInitializer;
  private final TypeInitializer typeInitializer;

  public DefaultNamespace(final Name name, final EventPublisher publisher) {
    if (name != null && name.child != null) {
      throw new IllegalArgumentException();
    }

    this.name = name;
    this.constantInitializer = new ConstantInitializer(storage);
    this.aliasInitializer = new AliasInitializer(storage, publisher);
    this.functionInitializer = new FunctionInitializer(storage, publisher);
    this.statementInitializer = new StatementInitializer(storage, publisher);
    this.typeInitializer = new TypeInitializer(storage);
  }

  @Override
  public MutableStorage create() {
    return storage.create();
  }

  @Override
  public Name name() {
    return name;
  }

  @Override
  public Result find(
    final FinderType finderType,
    final Name name
  ) throws StorageException {
    return storage.find(finderType, name);
  }

  @Override
  public void put(
    final Visibility visibility,
    final Name name,
    final Item item
  ) throws Exists {
    storage.put(visibility, name, item);
  }

  @Override
  public void addAlias(final AliasDetails aliasDetails) {
    aliasInitializer.init(aliasDetails);
  }

  @Override
  public void addConstant(final ConstantDetails constantDetails) {
    constantInitializer.init(constantDetails);
  }

  @Override
  public ToBeCommitted addFunction(final FunctionDetails functionDetails) {
    return functionInitializer.init(functionDetails);
  }

  @Override
  public BlockStatementResultBatch addStatement(final StatementDetails statementDetails) {
    return statementInitializer.init(statementDetails);
  }

  @Override
  public void addType(final TypeDetails typeDetails) {
    typeInitializer.init(typeDetails);
  }
}
