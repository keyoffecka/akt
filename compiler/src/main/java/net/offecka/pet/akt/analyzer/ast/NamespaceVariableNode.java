package net.offecka.pet.akt.analyzer.ast;

import net.offecka.pet.akt.errors.Location;

public class NamespaceVariableNode extends VariableNode {
  public final Scopes scope;

  public NamespaceVariableNode(
    final Location location,
    final Scopes scope,
    final boolean immutable,
    final String name,
    final TypeNode type,
    final ExpressionNode expression
  ) {
    super(location, immutable, name, type, expression);

    this.scope = scope;
  }
}
