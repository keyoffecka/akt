package net.offecka.pet.akt.analyzer.syntax.defaults.details;

import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.support.WithName;

public class ConstantDetails implements WithName {
  public final Name name;
  public final Name typeName;

  public ConstantDetails(final Name name, final Name typeName) {
    this.name = name;
    this.typeName = typeName;
  }

  @Override
  public Name name() {
    return name;
  }
}
