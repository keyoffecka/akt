package net.offecka.pet.akt.analyzer.syntax.defaults.statement.operands;

import net.offecka.pet.akt.analyzer.syntax.support.WithLocation;
import net.offecka.pet.akt.analyzer.syntax.support.WithType;
import net.offecka.pet.akt.expression.Operand;

public interface StatementOperand extends Operand, WithType, WithLocation {
}
