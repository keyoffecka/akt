package net.offecka.pet.akt.analyzer.ast.expression;

import net.offecka.pet.akt.analyzer.ast.Node;

public interface ASTElement extends Node { }
