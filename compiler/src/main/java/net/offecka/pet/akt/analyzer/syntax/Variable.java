package net.offecka.pet.akt.analyzer.syntax;

import net.offecka.pet.akt.analyzer.syntax.support.WithName;
import net.offecka.pet.akt.analyzer.syntax.support.WithType;
import net.offecka.pet.akt.storage.Item;
import net.offecka.pet.akt.storage.Storage;

public interface Variable extends Storage, Item, WithName, WithType {
  boolean unmodifiable();
}
