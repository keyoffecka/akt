package net.offecka.pet.akt.analyzer.syntax.defaults.details.statement;

import net.offecka.pet.akt.errors.Location;

import java.util.List;

import static java.util.List.copyOf;

public class BlockDetails implements StatementDetails, WithStartLocation {
  private final Location startLocation;
  private final Location endLocation;
  private final List<StatementDetails> statementDetailsList;

  public BlockDetails(
    final Location startLocation,
    final Location endLocation,
    final List<StatementDetails> statementDetailsList
  ) {
    this.startLocation = startLocation;
    this.endLocation = endLocation;
    this.statementDetailsList = copyOf(statementDetailsList);
  }

  public List<StatementDetails> statementDetailsList() {
    return statementDetailsList;
  }

  @Override
  public Location startLocation() {
    return startLocation;
  }

  public Location endLocation() {
    return endLocation;
  }
}
