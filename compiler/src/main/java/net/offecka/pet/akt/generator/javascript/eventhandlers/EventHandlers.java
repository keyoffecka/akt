package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.generator.context.ContextManager;

import java.io.Writer;

public class EventHandlers {
  public final AliasEventHandler aliasEventHandler;
  public final VariableEventHandler variableEventHandler;
  public final BeginStatementEventHandler beginStatementEventHandler;
  public final EndStatementEventHandler endStatementEventHandler;
  public final BeginFunctionEventHandler beginFunctionEventHandler;
  public final EndFunctionEventHandler endFunctionEventHandler;
  public final BeginArgumentsEventHandler beginArgumentsEventHandler;
  public final EndArgumentsEventHandler endArgumentsEventHandler;
  public final ArgumentEventHandler argumentEventHandler;
  public final BeginChainEventHandler beginChainEventHandler;
  public final EndChainEventHandler endChainEventHandler;
  public final AssignmentEventHandler assignmentEventHandler;
  public final NameEventHandler nameEventHandler;
  public final ValueEventHandler valueEventHandler;
  public final OperatorEventHandler operatorEventHandler;
  public final BeginCallEventHandler beginCallEventHandler;
  public final EndCallEventHandler endCallEventHandler;
  public final BeginParameterEventHandler beginParameterEventHandler;
  public final BeginBlockEventHandler beginBlockEventHandler;
  public final BeginIfElseBlockEventHandler beginIfElseBlockEventHandler;
  public final EndIfElseBlockEventHandler endIfElseBlockEventHandler;
  public final EndBlockEventHandler endBlockEventHandler;
  public final EndParameterEventHandler endParameterEventHandler;
  public final BeginIfEventHandler beginIfEventHandler;
  public final ElseIfEventHandler elseIfEventHandler;
  public final ElseEventHandler elseEventHandler;
  public final EndIfEventHandler endIfEventHandler;

  public EventHandlers(final String relativePathToTopDir, final Writer writer) {
    Tab tab = new Tab();
    ContextManager contextManager = new ContextManager();

    aliasEventHandler = new AliasEventHandler(relativePathToTopDir, writer);
    variableEventHandler = new VariableEventHandler(writer);
    beginStatementEventHandler = new BeginStatementEventHandler(writer, tab);
    endStatementEventHandler = new EndStatementEventHandler(writer);
    beginFunctionEventHandler = new BeginFunctionEventHandler(writer, tab, contextManager);
    endFunctionEventHandler = new EndFunctionEventHandler(writer, tab, contextManager);
    beginArgumentsEventHandler = new BeginArgumentsEventHandler(contextManager);
    endArgumentsEventHandler = new EndArgumentsEventHandler(writer, tab, contextManager);
    argumentEventHandler = new ArgumentEventHandler(writer, contextManager);
    beginChainEventHandler = new BeginChainEventHandler(writer, contextManager);
    endChainEventHandler = new EndChainEventHandler(contextManager);
    assignmentEventHandler = new AssignmentEventHandler(writer);
    nameEventHandler = new NameEventHandler(writer, contextManager);
    valueEventHandler = new ValueEventHandler(writer, contextManager);
    operatorEventHandler = new OperatorEventHandler(writer, contextManager);
    beginCallEventHandler = new BeginCallEventHandler(writer, contextManager);
    endCallEventHandler = new EndCallEventHandler(writer, contextManager);
    beginParameterEventHandler = new BeginParameterEventHandler(writer, contextManager);
    beginBlockEventHandler = new BeginBlockEventHandler(writer, tab);
    beginIfElseBlockEventHandler = new BeginIfElseBlockEventHandler(writer, tab);
    endIfElseBlockEventHandler = new EndIfElseBlockEventHandler(writer, tab);
    endBlockEventHandler = new EndBlockEventHandler(writer, tab);
    endParameterEventHandler = new EndParameterEventHandler();
    beginIfEventHandler = new BeginIfEventHandler(writer, tab);
    elseIfEventHandler = new ElseIfEventHandler(writer);
    elseEventHandler = new ElseEventHandler(writer);
    endIfEventHandler = new EndIfEventHandler(writer);
  }
}
