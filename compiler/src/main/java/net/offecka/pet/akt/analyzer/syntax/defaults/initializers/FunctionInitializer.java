package net.offecka.pet.akt.analyzer.syntax.defaults.initializers;

import net.offecka.pet.akt.analyzer.syntax.Function;
import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.analyzer.syntax.defaults.DefaultFunction;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.ArgumentDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.FunctionDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.StatementDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginArgumentsEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginFunctionEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndArgumentsEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndFunctionEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.result.BlockStatementResultBatch;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.result.StatementResultBatchAccumulator;
import net.offecka.pet.akt.analyzer.syntax.support.ToBeCommitted;
import net.offecka.pet.akt.events.EventPublisher;
import net.offecka.pet.akt.storage.MutableStorage;
import net.offecka.pet.akt.storage.exceptions.ExistedInChild;
import net.offecka.pet.akt.storage.exceptions.Exists;
import net.offecka.pet.akt.storage.exceptions.ExistsInStorage;
import net.offecka.pet.akt.storage.exceptions.NotFound;
import net.offecka.pet.akt.storage.exceptions.StorageException;
import net.offecka.pet.akt.storage.exceptions.UnexpectedType;

import java.util.ArrayList;
import java.util.List;

import static net.offecka.pet.akt.errors.ErrorContext.errorContext;
import static net.offecka.pet.akt.storage.FinderType.LOCAL;

public class FunctionInitializer {
  private final MutableStorage storage;
  private final EventPublisher eventPublisher;

  public FunctionInitializer(final MutableStorage storage, final EventPublisher eventPublisher) {
    this.storage = storage;
    this.eventPublisher = eventPublisher;
  }

  public ToBeCommitted init(final FunctionDetails functionDetails) {
    ToBeCommitted toBeCommitted = new NothingToBeCommitted();

    Type returnType = null;
    try {
      returnType = storage.find(LOCAL, functionDetails.returnTypeName()).item(Type.class);
    } catch (final NotFound | UnexpectedType ex) {
      errorContext().addError(ex.name.location(), ex);
    } catch (final StorageException ex) {
      throw new IllegalStateException(ex);
    }

    Function function = new DefaultFunction(storage, functionDetails.name(), returnType, eventPublisher);

    try {
      storage.put(functionDetails.visibility(), function.name(), function);
    } catch (final ExistsInStorage | ExistedInChild ex) {
      errorContext().addError(ex.name.location(), ex);
      function = null;
    } catch (final Exists ex) {
      throw new IllegalStateException(ex);
    }

    if (function != null) {
      List<ToBeCommitted> argumentsToCommit = new ArrayList<>();
      for (final ArgumentDetails argumentDetails : functionDetails.argumentDetailsList()) {
        argumentsToCommit.add(function.addArgument(argumentDetails));
      }

      toBeCommitted = this.new FunctionToBeCommitted(functionDetails, function, returnType, argumentsToCommit);
    }

    return toBeCommitted;
  }

  private final class FunctionToBeCommitted implements ToBeCommitted {
    private final FunctionDetails functionDetails;
    private final Function function;
    private final Type returnType;
    private final List<ToBeCommitted> arguments;

    private FunctionToBeCommitted(
      final FunctionDetails functionDetails,
      final Function function,
      final Type returnType,
      final List<ToBeCommitted> arguments
    ) {
      this.functionDetails = functionDetails;
      this.function = function;
      this.returnType = returnType;
      this.arguments = arguments;
    }

    @Override
    public void commit() {
      eventPublisher.publish(new BeginFunctionEvent(functionDetails.visibility(), function.name()));

      eventPublisher.publish(new BeginArgumentsEvent());
      for (final ToBeCommitted toBeCommitted : arguments) {
        toBeCommitted.commit();
      }
      eventPublisher.publish(new EndArgumentsEvent());

      StatementResultBatchAccumulator statementResultBatchAccumulator = new StatementResultBatchAccumulator();
      for (final StatementDetails statementDetails : functionDetails.statementDetailsList()) {
        BlockStatementResultBatch blockStatementResultBatch = function.addStatement(statementDetails);
        statementResultBatchAccumulator.accumulate(blockStatementResultBatch);
      }

      statementResultBatchAccumulator.analyze(
        FunctionInitializer.this.storage,
        returnType,
        functionDetails.endLocation()
      );

      eventPublisher.publish(new EndFunctionEvent());
    }
  }
}
