package net.offecka.pet.akt.generator.javascript;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.AliasEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.ArgumentEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.AssignmentEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginArgumentsEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginBlockEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginCallEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginChainEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginFunctionEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginIfElseBlockEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginIfEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginParameterEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginStatementEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.ElseEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.ElseIfEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndArgumentsEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndBlockEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndCallEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndChainEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndFunctionEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndIfElseBlockEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndIfEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndParameterEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndStatementEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.NameEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.OperatorEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.ValueEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.VariableEvent;
import net.offecka.pet.akt.events.Event;
import net.offecka.pet.akt.events.Listener;
import net.offecka.pet.akt.generator.javascript.eventhandlers.EventHandlers;

import java.io.Writer;

public class JavaScriptEventListener implements Listener {
  private final EventHandlers eventHandlers;

  public JavaScriptEventListener(final Writer writer, final String relativePathToTopDir) {
    this.eventHandlers = new EventHandlers(relativePathToTopDir, writer);
  }

  @Override
  public void onEvent(final Event event) throws Exception {
    if (event instanceof AliasEvent) {
      eventHandlers.aliasEventHandler.handle((AliasEvent) event);
    } else if (event instanceof VariableEvent) {
      eventHandlers.variableEventHandler.handle((VariableEvent) event);
    } else if (event instanceof BeginStatementEvent) {
      eventHandlers.beginStatementEventHandler.handle((BeginStatementEvent) event);
    } else if (event instanceof EndStatementEvent) {
      eventHandlers.endStatementEventHandler.handle((EndStatementEvent) event);
    } else if (event instanceof BeginFunctionEvent) {
      eventHandlers.beginFunctionEventHandler.handle((BeginFunctionEvent) event);
    } else if (event instanceof EndFunctionEvent) {
      eventHandlers.endFunctionEventHandler.handle((EndFunctionEvent) event);
    } else if (event instanceof BeginArgumentsEvent) {
      eventHandlers.beginArgumentsEventHandler.handle((BeginArgumentsEvent) event);
    } else if (event instanceof EndArgumentsEvent) {
      eventHandlers.endArgumentsEventHandler.handle((EndArgumentsEvent) event);
    } else if (event instanceof ArgumentEvent) {
      eventHandlers.argumentEventHandler.handle((ArgumentEvent) event);
    } else if (event instanceof BeginChainEvent) {
      eventHandlers.beginChainEventHandler.handle((BeginChainEvent) event);
    } else if (event instanceof EndChainEvent) {
      eventHandlers.endChainEventHandler.handle((EndChainEvent) event);
    } else if (event instanceof AssignmentEvent) {
      eventHandlers.assignmentEventHandler.handle((AssignmentEvent) event);
    } else if (event instanceof NameEvent) {
      eventHandlers.nameEventHandler.handle((NameEvent) event);
    } else if (event instanceof ValueEvent) {
      eventHandlers.valueEventHandler.handle((ValueEvent) event);
    } else if (event instanceof OperatorEvent) {
      eventHandlers.operatorEventHandler.handle((OperatorEvent) event);
    } else if (event instanceof BeginCallEvent) {
      eventHandlers.beginCallEventHandler.handle((BeginCallEvent) event);
    } else if (event instanceof EndCallEvent) {
      eventHandlers.endCallEventHandler.handle((EndCallEvent) event);
    } else if (event instanceof BeginParameterEvent) {
      eventHandlers.beginParameterEventHandler.handle((BeginParameterEvent) event);
    } else if (event instanceof BeginBlockEvent) {
      eventHandlers.beginBlockEventHandler.handle((BeginBlockEvent) event);
    } else if (event instanceof BeginIfElseBlockEvent) {
      eventHandlers.beginIfElseBlockEventHandler.handle((BeginIfElseBlockEvent) event);
    } else if (event instanceof EndIfElseBlockEvent) {
      eventHandlers.endIfElseBlockEventHandler.handle((EndIfElseBlockEvent) event);
    } else if (event instanceof EndBlockEvent) {
      eventHandlers.endBlockEventHandler.handle((EndBlockEvent) event);
    } else if (event instanceof EndParameterEvent) {
      eventHandlers.endParameterEventHandler.handle((EndParameterEvent) event);
    } else if (event instanceof BeginIfEvent) {
      eventHandlers.beginIfEventHandler.handle((BeginIfEvent) event);
    } else if (event instanceof ElseIfEvent) {
      eventHandlers.elseIfEventHandler.handle((ElseIfEvent) event);
    } else if (event instanceof ElseEvent) {
      eventHandlers.elseEventHandler.handle((ElseEvent) event);
    } else if (event instanceof EndIfEvent) {
      eventHandlers.endIfEventHandler.handle((EndIfEvent) event);
    } else {
      throw new IllegalStateException(event + " is not supported");
    }
  }
}
