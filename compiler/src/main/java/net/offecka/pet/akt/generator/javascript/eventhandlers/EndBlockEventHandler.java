package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndBlockEvent;

import java.io.IOException;
import java.io.Writer;

public class EndBlockEventHandler implements EventHandler<EndBlockEvent> {
  private final Writer writer;
  private final Tab tab;

  public EndBlockEventHandler(final Writer writer, final Tab tab) {
    this.writer = writer;
    this.tab = tab;
  }

  @Override
  public void handle(final EndBlockEvent event) throws IOException {
    tab.unindent();
    writer.write(tab + "}\n");
  }
}
