package net.offecka.pet.akt.analyzer.syntax.defaults.details.statement;

import net.offecka.pet.akt.analyzer.syntax.defaults.support.ExpressionProvider;
import net.offecka.pet.akt.errors.Location;

public class DefaultExpressionStatementDetails implements ExpressionStatementDetails {
  private final ExpressionProvider expressionProvider;
  private final Location expressionLocation;

  public DefaultExpressionStatementDetails(
    final ExpressionProvider expressionProvider,
    final Location expressionLocation
  ) {
    this.expressionProvider = expressionProvider;
    this.expressionLocation = expressionLocation;
  }

  @Override
  public ExpressionProvider expressionProvider() {
    return expressionProvider;
  }

  @Override
  public Location expressionLocation() {
    return expressionLocation;
  }
}
