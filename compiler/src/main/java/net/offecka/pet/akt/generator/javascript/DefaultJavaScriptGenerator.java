package net.offecka.pet.akt.generator.javascript;

import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.events.DefaultEventSource;
import net.offecka.pet.akt.events.EventPublisher;
import net.offecka.pet.akt.generator.Unit;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.Map;

import static java.util.Map.copyOf;

public class DefaultJavaScriptGenerator implements Unit {
  private final Name name;
  private final Map<Name, ReaderWriter> map;
  private final DefaultEventSource publisher;
  private final ReaderWriter rw;

  public DefaultJavaScriptGenerator(
    final Name name,
    final Map<Name, ReaderWriter> map
  ) throws IOException {
    this.name = name;
    this.map = copyOf(map);

    this.publisher = new DefaultEventSource();

    rw = map.get(name);
    if (rw == null) {
      throw new IOException("Not found [" + name + "]");
    }
    rw.writer.write("module.exports = {}\n");
    publisher.addListener(new JavaScriptEventListener(rw.writer, "."));
  }

  @Override
  public Name name() {
    return name;
  }

  @Override
  public Reader reader() {
    return rw.reader;
  }

  @Override
  public EventPublisher publisher() {
    return publisher;
  }

  @Override
  public String resourceName() {
    return name.toString();
  }

  @Override
  public Unit unit(final Name name) throws IOException {
    return new DefaultJavaScriptGenerator(name, this.map);
  }

  @Override
  public void close() throws IOException {
    rw.writer.close();
  }

  public static class ReaderWriter {
    public final Reader reader;
    public final Writer writer;

    public ReaderWriter(final Reader reader, final Writer writer) {
      this.reader = reader;
      this.writer = writer;
    }
  }
}
