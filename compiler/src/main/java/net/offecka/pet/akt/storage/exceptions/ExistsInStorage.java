package net.offecka.pet.akt.storage.exceptions;

import net.offecka.pet.akt.analyzer.syntax.Name;

public class ExistsInStorage extends Exists {
  public ExistsInStorage(final Name name, final Name prev) {
    super(name, prev);
  }
}
