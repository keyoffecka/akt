package net.offecka.pet.akt.analyzer.ast;

import net.offecka.pet.akt.errors.Location;

public class AssignmentNode implements StepNode {
  public final Location location;
  public final ChainNode chain;
  public final ExpressionNode expression;

  public AssignmentNode(
    final Location location,
    final ChainNode chain,
    final ExpressionNode expression
  ) {
    this.location = location;
    this.chain = chain;
    this.expression = expression;
  }
}
