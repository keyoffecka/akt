package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginCallEvent;
import net.offecka.pet.akt.generator.context.ContextManager;

import java.io.IOException;
import java.io.Writer;

public class BeginCallEventHandler implements EventHandler<BeginCallEvent> {
  private final Writer writer;
  private final ContextManager contextManager;

  public BeginCallEventHandler(final Writer writer, final ContextManager contextManager) {
    this.writer = writer;
    this.contextManager = contextManager;
  }

  @Override
  public void handle(final BeginCallEvent event) throws IOException {
    writer.write("(");
    contextManager.openCountContext();
  }
}
