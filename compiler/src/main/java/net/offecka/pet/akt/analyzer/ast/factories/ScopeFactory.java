package net.offecka.pet.akt.analyzer.ast.factories;

import net.offecka.pet.akt.analyzer.ast.Scopes;
import net.offecka.pet.akt.grammar.AktParser.NamespaceMemberScopeContext;

public class ScopeFactory {
  Scopes create(final NamespaceMemberScopeContext scope) {
    if (scope == null) {
      return Scopes.PRIVATE;
    } else if (scope.pub() != null) {
      return Scopes.PUBLIC;
    } else {
      throw new UnsupportedOperationException();
    }
  }
}
