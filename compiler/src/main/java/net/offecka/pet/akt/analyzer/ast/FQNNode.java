package net.offecka.pet.akt.analyzer.ast;

import net.offecka.pet.akt.errors.Location;

import java.util.List;

import static java.util.List.copyOf;

public class FQNNode implements Node {
  public final Location location;
  public final List<NameNode> names;

  public FQNNode(final Location location, final List<NameNode> names) {
    if (names.isEmpty()) {
      throw new IllegalArgumentException();
    }

    this.location = location;
    this.names = copyOf(names);
  }
}
