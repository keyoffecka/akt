package net.offecka.pet.akt.analyzer.ast.expression;

import net.offecka.pet.akt.errors.Location;

public class ASTClosing implements ASTElement {
  public final Location location;

  public ASTClosing(final Location location) {
    this.location = location;
  }
}
