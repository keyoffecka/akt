package net.offecka.pet.akt.analyzer.syntax.defaults.initializers;

import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.AssignmentStatementDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.BlockDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.ExpressionStatementDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.IfBlockDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.IfDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.NamespaceVariableStatementDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.ReturnStatementDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.StatementDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.VariableStatementDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.VoidReturnStatementDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.WithReturnTypeProvider;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.WithStartLocation;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.processors.AssignmentProcessor;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.processors.BlockProcessor;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.processors.BlockProcessor.BlockEventFactory;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.processors.ElseBlockProcessor;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.processors.ElseIfBlockProcessor;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.processors.ExpressionStatementProcessor;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.processors.IfBlockProcessor;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.processors.IfElseBlockProcessor;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.processors.IfElseBlockProcessor.IfElseBlockEventFactory;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.processors.IfElseBlockProcessor.LastIfElseBlockEventFactory;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.processors.IfProcessor;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.processors.NamespaceVariableProcessor;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.processors.ReturnProcessor;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.processors.StatementProcessor;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.processors.VariableProcessor;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.processors.VoidReturnProcessor;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.result.BlockStatementResultBatch;
import net.offecka.pet.akt.errors.Location;
import net.offecka.pet.akt.events.EventPublisher;
import net.offecka.pet.akt.expression.Expression;
import net.offecka.pet.akt.storage.MutableStorage;
import net.offecka.pet.akt.storage.exceptions.NotFound;
import net.offecka.pet.akt.storage.exceptions.StorageException;
import net.offecka.pet.akt.storage.exceptions.UnexpectedType;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import static net.offecka.pet.akt.errors.ErrorContext.errorContext;
import static net.offecka.pet.akt.storage.FinderType.LOCAL;

public class StatementInitializer {
  private final MutableStorage storage;
  private final EventPublisher publisher;

  public StatementInitializer(final MutableStorage storage, final EventPublisher publisher) {
    this.storage = storage;
    this.publisher = publisher;
  }

  public BlockStatementResultBatch init(final StatementDetails statementDetails) {
    BlockStatementResultBatch blockStatementResultBatch = new BlockStatementResultBatch(
      getStartLocation(statementDetails)
    );

    StatementProcessor statementProcessor = createStatementProcessor(statementDetails);
    if (statementProcessor != null) {
      blockStatementResultBatch = statementProcessor.process(storage);
    }

    return blockStatementResultBatch;
  }

  private Location getStartLocation(final StatementDetails statementDetails) {
    Location location = null;
    if (statementDetails instanceof WithStartLocation) {
      WithStartLocation withStartLocation = (WithStartLocation) statementDetails;
      location = withStartLocation.startLocation();
    }
    return location;
  }

  private StatementProcessor createStatementProcessor(final StatementDetails statementDetails) {
    StatementProcessor statementProcessor = null;
    if (statementDetails instanceof NamespaceVariableStatementDetails) {
      statementProcessor = createNamespaceVariableProcessor((NamespaceVariableStatementDetails) statementDetails);
    } else if (statementDetails instanceof VariableStatementDetails) {
      statementProcessor = createVariableProcessor((VariableStatementDetails) statementDetails);
    } else if (statementDetails instanceof AssignmentStatementDetails) {
      statementProcessor = createAssignmentProcessor((AssignmentStatementDetails) statementDetails);
    } else if (statementDetails instanceof ReturnStatementDetails) {
      statementProcessor = createReturnProcessor((ReturnStatementDetails) statementDetails);
    } else if (statementDetails instanceof ExpressionStatementDetails) {
      statementProcessor = createExpressionProcessor((ExpressionStatementDetails) statementDetails);
    } else if (statementDetails instanceof VoidReturnStatementDetails) {
      statementProcessor = createVoidReturnProcessor((VoidReturnStatementDetails) statementDetails);
    } else if (statementDetails instanceof BlockDetails) {
      statementProcessor = createBlockProcessor((BlockDetails) statementDetails);
    } else if (statementDetails instanceof IfDetails) {
      statementProcessor = createIfProcessor((IfDetails) statementDetails);
    } else {
      throw new IllegalStateException("Unsupported statement details " + statementDetails);
    }
    return statementProcessor;
  }

  private BlockProcessor createBlockProcessor(final BlockDetails details) {
    return new BlockProcessor(
      details.startLocation(),
      details.endLocation(),
      details.statementDetailsList(),
      new BlockEventFactory(),
      publisher
    );
  }

  private StatementProcessor createIfProcessor(final IfDetails details) {
    List<IfElseBlockProcessor> elseBlockProcessors = new ArrayList<>();
    Iterator<IfBlockDetails> it = details.ifBlockDetailsList().iterator();
    boolean ifElse = false;
    boolean last = !it.hasNext();
    while (!last) {
      IfBlockDetails ifBlockDetails = it.next();
      last = !it.hasNext();

      elseBlockProcessors.add(createIfBlockProcessor(
        ifBlockDetails.blockDetails(),
        ifBlockDetails.expressionStatementDetails(),
        ifElse,
        last
      ));
      ifElse = true;
    }
    return new IfProcessor(details.ifLocation(), elseBlockProcessors, publisher);
  }

  private IfElseBlockProcessor createIfBlockProcessor(
    final BlockDetails details,
    final ExpressionStatementDetails expressionStatementDetails,
    final boolean ifElse,
    final boolean last
  ) {
    BlockEventFactory blockEventFactory
      = last ? new LastIfElseBlockEventFactory() : new IfElseBlockEventFactory();
    if (expressionStatementDetails == null) {
      return new ElseBlockProcessor(
        details.startLocation(),
        details.endLocation(),
        details.statementDetailsList(),
        blockEventFactory,
        publisher
      );
    } else {
      Expression expression = createExpression(expressionStatementDetails);
      if (ifElse) {
        return new ElseIfBlockProcessor(
          details.startLocation(),
          details.endLocation(),
          expression,
          details.statementDetailsList(),
          blockEventFactory,
          publisher
        );
      } else {
        return new IfBlockProcessor(
          details.startLocation(),
          details.endLocation(),
          expression,
          details.statementDetailsList(),
          blockEventFactory,
          publisher
        );
      }
    }
  }

  private StatementProcessor createExpressionProcessor(final ExpressionStatementDetails details) {
    return Optional.ofNullable(createExpression(details))
      .map(expression -> new ExpressionStatementProcessor(details.expressionLocation(), expression, publisher))
      .orElse(null);
  }

  private StatementProcessor createVoidReturnProcessor(final VoidReturnStatementDetails details) {
    Type returnType = getReturnType(details);
    return Optional.ofNullable(returnType)
      .map(rt -> new VoidReturnProcessor(details.startLocation(), details.expressionLocation(), rt, publisher))
      .orElse(null);
  }

  private StatementProcessor createReturnProcessor(final ReturnStatementDetails details) {
    Type returnType = getReturnType(details);
    if (returnType == null) {
      return null;
    }
    return Optional.ofNullable(createExpression(details))
      .map(expression -> new ReturnProcessor(
        details.startLocation(),
        details.expressionLocation(),
        returnType,
        expression,
        publisher
      )).orElse(null);
  }

  private StatementProcessor createAssignmentProcessor(final AssignmentStatementDetails details) {
    return Optional.ofNullable(createExpression(details))
      .map(expression -> new AssignmentProcessor(details.assignmentLocation(), details.variableName(), expression, publisher))
      .orElse(null);
  }

  private StatementProcessor createVariableProcessor(final VariableStatementDetails details) {
    Expression expression = createExpression(details);
    if (expression == null) {
      return null;
    }

    Type variableType = null;
    if (details.variableTypeName() != null) {
      variableType = findType(details.variableTypeName());
      if (variableType == null) {
        return null;
      }
    }

    return new VariableProcessor(
      details.variableLocation(),
      details.unmodifiableVariable(),
      details.name(),
      variableType,
      expression,
      publisher
    );
  }

  private StatementProcessor createNamespaceVariableProcessor(final NamespaceVariableStatementDetails details) {
    Expression expression = createExpression(details);
    if (expression == null) {
      return null;
    }

    Type variableType = null;
    if (details.variableTypeName() != null) {
      variableType = findType(details.variableTypeName());
      if (variableType == null) {
        return null;
      }
    }

    return new NamespaceVariableProcessor(
      details.variableLocation(),
      details.variableVisibility(),
      details.unmodifiableVariable(),
      details.name(),
      variableType,
      expression,
      publisher
    );
  }

  private Type findType(final Name name) {
    Type result = null;
    try {
      result = storage.find(LOCAL, name).item(Type.class);
    } catch (final NotFound | UnexpectedType ex) {
      errorContext().addError(ex.name.location(), ex);
    } catch (final StorageException ex) {
      throw new IllegalStateException(ex);
    }
    return result;
  }

  private Type getReturnType(final WithReturnTypeProvider returnTypeProvider) {
    Type returnType = null;
    try {
      returnType = returnTypeProvider.returnTypeProvider().provide(storage);
    } catch (final NotFound | UnexpectedType ex) {
      errorContext().addError(ex.name.location(), ex);
    } catch (final StorageException ex) {
      throw new IllegalStateException(ex);
    }
    return returnType;
  }

  private Expression createExpression(final ExpressionStatementDetails details) {
    Expression expression = null;
    try {
      expression = details.expressionProvider().provide(storage);
    } catch (final NotFound | UnexpectedType ex) {
      errorContext().addError(ex.name.location(), ex);
    }
    return expression;
  }
}
