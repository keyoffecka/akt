package net.offecka.pet.akt.analyzer.collector;

import net.offecka.pet.akt.analyzer.ast.FQNNode;
import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.Namespace;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.AliasDetails;
import net.offecka.pet.akt.generator.Unit;
import net.offecka.pet.akt.grammar.AktParser.NamespaceContext;
import net.offecka.pet.akt.grammar.AktParser.NamespaceImportContext;
import net.offecka.pet.akt.storage.exceptions.IllegalAccess;
import net.offecka.pet.akt.storage.exceptions.NotFound;
import net.offecka.pet.akt.storage.exceptions.StorageException;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.IOException;

import static net.offecka.pet.akt.analyzer.ast.factories.NodeFactories.NODE_FACTORIES;
import static net.offecka.pet.akt.analyzer.syntax.NameFactory.NAME_FACTORY;
import static net.offecka.pet.akt.errors.ErrorContext.errorContext;
import static net.offecka.pet.akt.errors.Location.at;
import static net.offecka.pet.akt.storage.FinderType.FOREIGN;

public class AliasImporter {
  private final Collector collector;
  private final UnitParser unitParser;

  public AliasImporter(final Collector collector, final UnitParser unitParser) {
    this.collector = collector;
    this.unitParser = unitParser;
  }

  public void importAliases(final Namespace namespace, final NamespaceContext nsContext, final Unit unit) {
    for (final ParseTree child : nsContext.children) {
      if (child instanceof NamespaceImportContext) {
        NamespaceImportContext namespaceImport = (NamespaceImportContext) child;
        FQNNode fqnNode = NODE_FACTORIES.fqnNodeFactory.create(namespaceImport.fqn());
        Name name = NAME_FACTORY.create(fqnNode);
        if (ensureNameExistsInRoot(unit, name)) {
          Name alias = namespaceImport.name() == null
            ? name.last()
            : NAME_FACTORY.create(
              namespaceImport.name().getText(),
              at(namespaceImport.name())
            );
          namespace.addAlias(new AliasDetails(alias, collector.root(), name));
        }
      }
    }
  }

  private boolean ensureNameExistsInRoot(final Unit unit, final Name name) {
    Boolean loaded = null;
    try {
      collector.root().find(FOREIGN, name);
      loaded = false;
    } catch (final NotFound ex) {
      loaded = tryLoadNamespace(unit, name);
      if (loaded == null) {
        errorContext().addError(ex.name.location(), ex);
      }
    } catch (final IllegalAccess ex) {
      errorContext().addError(ex.name.location(), ex);
    } catch (final StorageException ex) {
      throw new IllegalStateException(ex);
    }
    return loaded != null;
  }

  private Boolean tryLoadNamespace(final Unit unit, final Name name) {
    Boolean loaded = null;
    Name parentNamespaceName = name.prefix();
    try {
      collector.root().find(FOREIGN, parentNamespaceName).item(Namespace.class);
    } catch (final NotFound ex2) {
      Unit parentNamespaceUnit = getParentNamespaceUnit(unit, name, parentNamespaceName);
      if (parentNamespaceUnit != null) {
        loaded = collectParentNamespaceElements(parentNamespaceUnit);
      }
    } catch (final StorageException ex) {
      throw new IllegalStateException(ex);
    }
    return loaded;
  }

  private Boolean collectParentNamespaceElements(final Unit parentNamespaceUnit) {
    Boolean loaded = null;
    errorContext().begin(parentNamespaceUnit);
    try {
      NamespaceContext dependentNSContext = unitParser.parse(parentNamespaceUnit);
      if (dependentNSContext != null) {
        collector.collect(parentNamespaceUnit, dependentNSContext);
        loaded = true;
      }
    } finally {
      try {
        closeUnit(parentNamespaceUnit);
      } finally {
        errorContext().end();
      }
    }
    return loaded;
  }

  private Unit getParentNamespaceUnit(final Unit unit, final Name name, final Name parentNamespaceName) {
    Unit parentNamespaceUnit = null;
    try {
      parentNamespaceUnit = unit.unit(parentNamespaceName);
    } catch (final IOException ex) {
      errorContext().addError(
        "Cannot create unit for [" + parentNamespaceName + "]",
        name.location(), ex
      );
    }
    return parentNamespaceUnit;
  }

  private void closeUnit(final Unit unit) {
    try {
      unit.close();
    } catch (final IOException ex) {
      throw new IllegalStateException(ex);
    }
  }
}
