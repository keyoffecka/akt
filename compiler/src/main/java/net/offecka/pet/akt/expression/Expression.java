package net.offecka.pet.akt.expression;

import java.util.ArrayList;
import java.util.List;

import static java.util.List.copyOf;
import static net.offecka.pet.akt.expression.OperatorDescriptor.OperatorDescriptors.CLOSE;
import static net.offecka.pet.akt.expression.OperatorDescriptor.OperatorDescriptors.OPEN;

public class Expression {
  public final List<Element> elements;

  public Expression(final List<Element> elements) {
    this.elements = copyOf(elements);
  }

  public Operand evaluate() throws CannotEvaluate {
    List<Operand> operands = new ArrayList<>();
    List<Operator> operators = new ArrayList<>();
    for (final Element element : elements) {
      if (element instanceof Operand) {
        operands.add(0, (Operand) element);
      } else {
        Operator operator = (Operator) element;

        while (!operators.isEmpty() && operators.get(0).descriptor() != OPEN
          && operator.descriptor().precedence >= operators.get(0).descriptor().precedence
        ) {
          Operator lastOperator = operators.remove(0);
          Operand operand = lastOperator.evaluate(operands);
          operands.add(0, operand);
        }

        if (operator.descriptor() == CLOSE) {
          if (operators.remove(0).descriptor() != OPEN) {
            throw new IllegalStateException();
          }
        } else {
          operators.add(0, operator);
        }
      }
    }

    while (!operators.isEmpty()) {
      Operator operator = operators.remove(0);
      Operand operand = operator.evaluate(operands);
      operands.add(0, operand);
    }

    if (operands.size() > 1) {
      throw new IllegalStateException();
    }

    return operands.get(0);
  }
}
