package net.offecka.pet.akt.analyzer.syntax;

import net.offecka.pet.akt.analyzer.syntax.defaults.support.ExpressionProvider;
import net.offecka.pet.akt.analyzer.syntax.support.WithLocation;
import net.offecka.pet.akt.errors.Location;

public class Parameter implements WithLocation {
  public final Name name;
  public final ExpressionProvider expressionProvider;

  private final Location location;

  public Parameter(
    final Name name,
    final ExpressionProvider expressionProvider,
    final Location location
  ) {
    if (name != null && name.child != null) {
      throw new IllegalArgumentException(name.toString());
    }

    this.name = name;
    this.expressionProvider = expressionProvider;
    this.location = location;
  }

  @Override
  public Location location() {
    return location;
  }
}
