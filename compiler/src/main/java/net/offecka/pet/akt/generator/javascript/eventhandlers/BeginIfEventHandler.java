package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginIfEvent;

import java.io.IOException;
import java.io.Writer;

public class BeginIfEventHandler implements EventHandler<BeginIfEvent> {
  private final Writer writer;
  private final Tab tab;

  public BeginIfEventHandler(final Writer writer, final Tab tab) {
    this.writer = writer;
    this.tab = tab;
  }

  @Override
  public void handle(final BeginIfEvent event) throws IOException {
    writer.write(tab + "if (");
  }
}
