package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginChainEvent;
import net.offecka.pet.akt.generator.context.ContextManager;

import java.io.IOException;
import java.io.Writer;

public class BeginChainEventHandler implements EventHandler<BeginChainEvent> {
  private final Writer writer;
  private final ContextManager contextManager;

  public BeginChainEventHandler(final Writer writer, final ContextManager contextManager) {
    this.writer = writer;
    this.contextManager = contextManager;
  }

  @Override
  public void handle(final BeginChainEvent event) throws IOException {
    writer.write(contextManager.negateIfRequired(event.type));
    contextManager.openCountContext();
  }
}
