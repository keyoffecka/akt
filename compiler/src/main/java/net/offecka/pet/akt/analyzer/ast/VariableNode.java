package net.offecka.pet.akt.analyzer.ast;

import net.offecka.pet.akt.errors.Location;

public class VariableNode implements StepNode {
  public final Location location;
  public final boolean unmodifiable;
  public final String name;
  public final TypeNode type;
  public final ExpressionNode expression;

  public VariableNode(
    final Location location,
    final boolean unmodifiable,
    final String name,
    final TypeNode type,
    final ExpressionNode expression
  ) {
    this.location = location;
    this.unmodifiable = unmodifiable;
    this.name = name;
    this.type = type;
    this.expression = expression;
  }
}
