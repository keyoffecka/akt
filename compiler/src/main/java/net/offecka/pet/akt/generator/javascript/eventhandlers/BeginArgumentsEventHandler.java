package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginArgumentsEvent;
import net.offecka.pet.akt.generator.context.ContextManager;

public class BeginArgumentsEventHandler implements EventHandler<BeginArgumentsEvent> {
  private final ContextManager contextManager;

  public BeginArgumentsEventHandler(final ContextManager contextManager) {
    this.contextManager = contextManager;
  }

  @Override
  public void handle(final BeginArgumentsEvent event) {
    contextManager.openCountContext();
  }
}
