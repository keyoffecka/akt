package net.offecka.pet.akt.analyzer.ast;

import net.offecka.pet.akt.errors.Location;

import java.util.List;

import static java.util.List.copyOf;

public class ClassNode implements Node {
  public final Location location;
  public final Scopes scope;
  public final String name;
  public final ClassNode parent;
  public final List<FunctionNode> functions;

  public ClassNode(
    final Location location,
    final Scopes scope,
    final String name,
    final ClassNode parent,
    final List<FunctionNode> functions
  ) {
    this.location = location;
    this.scope = scope;
    this.name = name;
    this.parent = parent;
    this.functions = copyOf(functions);
  }
}
