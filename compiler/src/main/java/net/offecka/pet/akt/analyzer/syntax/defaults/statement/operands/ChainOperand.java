package net.offecka.pet.akt.analyzer.syntax.defaults.statement.operands;

import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.analyzer.syntax.support.WithName;
import net.offecka.pet.akt.errors.Location;

public class ChainOperand implements StatementOperand, WithName {
  private final Name fullName;
  private final Type type;
  private final Location location;

  public ChainOperand(final Name fullName, final Type type, final Location location) {
    this.fullName = fullName;
    this.type = type;
    this.location = location;
  }

  @Override
  public Location location() {
    return location;
  }

  @Override
  public Name name() {
    return fullName;
  }

  @Override
  public Type type() {
    return type;
  }
}
