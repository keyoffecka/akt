package net.offecka.pet.akt.events;

public interface Listener {
  void onEvent(Event event) throws Exception;
}
