package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndIfElseBlockEvent;

import java.io.IOException;
import java.io.Writer;

public class EndIfElseBlockEventHandler implements EventHandler<EndIfElseBlockEvent> {
  private final Writer writer;
  private final Tab tab;

  public EndIfElseBlockEventHandler(final Writer writer, final Tab tab) {
    this.writer = writer;
    this.tab = tab;
  }

  @Override
  public void handle(final EndIfElseBlockEvent event) throws IOException {
    tab.unindent();
    writer.write(tab + "} ");
  }
}
