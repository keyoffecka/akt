package net.offecka.pet.akt.analyzer.collector;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;

import java.util.BitSet;

public class ErrorListener extends BaseErrorListener {
  @Override
  public void syntaxError(
    final Recognizer<?, ?> recognizer,
    final Object offendingSymbol,
    final int line,
    final int charPositionInLine,
    final String msg,
    final RecognitionException ex
  ) {
    throw new RuntimeException("line " + line + ":" + charPositionInLine + " " + msg, ex);
  }

  @Override
  public void reportAmbiguity(final Parser recognizer, final DFA dfa, final int startIndex, final int stopIndex, final boolean exact, final BitSet ambigAlts, final ATNConfigSet configs) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void reportAttemptingFullContext(final Parser recognizer, final DFA dfa, final int startIndex, final int stopIndex, final BitSet conflictingAlts, final ATNConfigSet configs) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void reportContextSensitivity(final Parser recognizer, final DFA dfa, final int startIndex, final int stopIndex, final int prediction, final ATNConfigSet configs) {
    throw new UnsupportedOperationException();
  }
}