package net.offecka.pet.akt.analyzer.syntax.defaults;

import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.analyzer.syntax.support.WithName;
import net.offecka.pet.akt.storage.Item;

import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.ANY;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.NOTHING;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.NULL;

public class PredefinedType implements Type, WithName, Item {
  private final Name name;

  public PredefinedType(final Name name) {
    this.name = name;
  }

  @Override
  public Name name() {
    return name;
  }

  @Override
  public boolean isSubOf(final Type type) {
    if (!(type instanceof WithName)) {
      return false;
    }
    WithName withNameType = (WithName) type;

    if (name().equals(NOTHING) || withNameType.name().equals(NOTHING)) {
      return false;
    }

    return this == type
      || name().equals(NULL)
      || withNameType.name().equals(ANY);
  }

  @Override
  public String description() {
    return name().toString();
  }
}
