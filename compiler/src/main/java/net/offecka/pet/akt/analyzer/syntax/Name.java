package net.offecka.pet.akt.analyzer.syntax;

import net.offecka.pet.akt.analyzer.syntax.support.WithLocation;
import net.offecka.pet.akt.errors.Location;

import java.util.Objects;
import java.util.Optional;

import static net.offecka.pet.akt.analyzer.syntax.NameFactory.NAME_FACTORY;
import static net.offecka.pet.akt.errors.Location.NO_LOCATION;

public class Name implements WithLocation {
  public final String name;
  public final Name child;

  private final Location location;

  Name(final String name, final Name child, final Location location) {
    if (location == null) {
      throw new IllegalArgumentException();
    }

    if (name == null) {
      throw new IllegalArgumentException();
    }

    String[] names = name.split("\\.", 2);
    if (names.length != 1 || name.trim().isEmpty() || !name.equals(name.trim())) {
      throw new IllegalArgumentException("Invalid name [" + name + "]");
    }

    this.name = name;
    this.child = child;
    this.location = location;
  }

  @Override
  public Location location() {
    return location;
  }

  public Name first() {
    return NAME_FACTORY.create(this, null);
  }

  public Name last() {
    return child == null ? this : child.last();
  }

  public Name prefix() {
    if (child == null) {
      return null;
    } else {
      return NAME_FACTORY.create(this, child.prefix());
    }
  }

  public Name postfix() {
    return child;
  }

  @Override
  public boolean equals(final Object o) {
    boolean result = o == this;

    if (!result && o != null && o.getClass() == this.getClass()) {
      Name name1 = (Name) o;

      result = Objects.equals(name, name1.name)
        && Objects.equals(child, name1.child);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(name) ^ Objects.hashCode(child);
  }

  @Override
  public String toString() {
    return name + Optional.ofNullable(child).map(c -> "." + c.toString()).orElse("");
  }

  public interface StandardNames {
    Name ANY = NAME_FACTORY.create("Any", NO_LOCATION);
    Name NOTHING = NAME_FACTORY.create("Nothing", NO_LOCATION);
    Name NULL = NAME_FACTORY.create("Null", NO_LOCATION);
    Name VOID = NAME_FACTORY.create("Void", NO_LOCATION);
    Name INT = NAME_FACTORY.create("Int", NO_LOCATION);
    Name REAL = NAME_FACTORY.create("Real", NO_LOCATION);
    Name STR = NAME_FACTORY.create("Str", NO_LOCATION);
    Name BOOL = NAME_FACTORY.create("Bool", NO_LOCATION);

    Name TRUE_CONST = NAME_FACTORY.create("TRUE", NO_LOCATION);
    Name FALSE_CONST = NAME_FACTORY.create("FALSE", NO_LOCATION);
    Name NULL_CONST = NAME_FACTORY.create("NULL", NO_LOCATION);
  }
}
