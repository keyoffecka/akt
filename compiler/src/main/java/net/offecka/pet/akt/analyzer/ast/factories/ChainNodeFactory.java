package net.offecka.pet.akt.analyzer.ast.factories;

import net.offecka.pet.akt.analyzer.ast.CallNode;
import net.offecka.pet.akt.analyzer.ast.ChainNode;
import net.offecka.pet.akt.analyzer.ast.ChainSegmentNode;
import net.offecka.pet.akt.analyzer.ast.ExpressionNode;
import net.offecka.pet.akt.analyzer.ast.NameNode;
import net.offecka.pet.akt.analyzer.ast.ParamNode;
import net.offecka.pet.akt.analyzer.ast.ParamsNode;
import net.offecka.pet.akt.analyzer.ast.expression.ASTClosing;
import net.offecka.pet.akt.analyzer.ast.expression.ASTElement;
import net.offecka.pet.akt.analyzer.ast.expression.ASTOpening;
import net.offecka.pet.akt.analyzer.ast.expression.ASTOperator;
import net.offecka.pet.akt.analyzer.ast.expression.ASTOperator.ASTOperators;
import net.offecka.pet.akt.analyzer.ast.expression.operands.ASTBoolLiteralOperand;
import net.offecka.pet.akt.analyzer.ast.expression.operands.ASTChainOperand;
import net.offecka.pet.akt.analyzer.ast.expression.operands.ASTIntLiteralOperand;
import net.offecka.pet.akt.analyzer.ast.expression.operands.ASTRealLiteralOperand;
import net.offecka.pet.akt.analyzer.ast.expression.operands.ASTStrLiteralOperand;
import net.offecka.pet.akt.grammar.AktParser.AssignableContext;
import net.offecka.pet.akt.grammar.AktParser.BinOpContext;
import net.offecka.pet.akt.grammar.AktParser.ChainContext;
import net.offecka.pet.akt.grammar.AktParser.ExpressionContext;
import net.offecka.pet.akt.grammar.AktParser.LParenContext;
import net.offecka.pet.akt.grammar.AktParser.LiteralContext;
import net.offecka.pet.akt.grammar.AktParser.ParamsContext;
import net.offecka.pet.akt.grammar.AktParser.RParenContext;
import net.offecka.pet.akt.grammar.AktParser.UnOpContext;

import java.util.ArrayList;
import java.util.List;

import static java.util.List.of;
import static net.offecka.pet.akt.errors.Location.at;
import static net.offecka.pet.akt.errors.Location.before;

public class ChainNodeFactory {
  ChainNodeFactory() {
  }

  public ChainNode create(final ChainContext chain) {
    List<ChainSegmentNode> segments = new ArrayList<>();

    ChainContext ch = chain;
    do {
      segments.add(
        ch.call() == null
          ? new NameNode(at(ch.var().name()), ch.var().name().getText())
          : new CallNode(
              at(ch.call().name()),
              ch.call().name().getText(),
              new ParamsNode(
                before(ch.call().RPAREN().getSymbol()),
                ch.call().params() == null ? of() : createParams(ch.call().params())
              )
          )
      );
      ch = ch.chain();
    } while (ch != null);
    return new ChainNode(at(chain), segments);
  }

  public ChainNode create(final AssignableContext assignable) {
    List<ChainSegmentNode> segments = new ArrayList<>();
    if (assignable.chain() != null) {
      ChainNode chain = create(assignable.chain());
      segments.addAll(chain.segments);
    }
    segments.add(new NameNode(at(assignable.var()), assignable.var().name().getText()));
    return new ChainNode(at(assignable), segments);
  }

  public ExpressionNode create(final ExpressionContext expressionContext) {
    List<ASTElement> elements = new ArrayList<>();

    for (final var child : expressionContext.children) {
      if (child instanceof LiteralContext) {
        LiteralContext literalContext = (LiteralContext) child;
        if (literalContext.string() != null) {
          elements.add(new ASTStrLiteralOperand(
            at(literalContext.string()),
            literalContext.string().getText().substring(1, literalContext.string().getText().length() - 1)
          ));
        } else if (literalContext.integer() != null) {
          elements.add(new ASTIntLiteralOperand(
            at(literalContext.integer()),
            literalContext.integer().getText()
          ));
        } else if (literalContext.real() != null) {
          elements.add(new ASTRealLiteralOperand(
            at(literalContext.real()),
            literalContext.real().getText()
          ));
        } else if (literalContext.bool() != null) {
          elements.add(new ASTBoolLiteralOperand(
            at(literalContext.bool()),
            literalContext.bool().tr() != null
          ));
        }
      } else if (child instanceof ChainContext) {
        ChainContext chainContext = (ChainContext) child;
        elements.add(new ASTChainOperand(at(chainContext), create(chainContext)));
      } else if (child instanceof ExpressionContext) {
        ExpressionContext childExpressionContext = (ExpressionContext) child;
        elements.addAll(create(childExpressionContext).elements);
      } else if (child instanceof LParenContext) {
        elements.add(new ASTOpening(at((LParenContext) child)));
      } else if (child instanceof RParenContext) {
        elements.add(new ASTClosing(at((RParenContext) child)));
      } else if (child instanceof UnOpContext) {
        UnOpContext opContext = (UnOpContext) child;
        ASTOperators operator = null;
        if (opContext.plus() != null) {
          operator = ASTOperators.PLUS;
        } else if (opContext.minus() != null) {
          operator = ASTOperators.MINUS;
        } else if (opContext.inv() != null) {
          operator = ASTOperators.NOT;
        } else {
          throw new UnsupportedOperationException();
        }
        elements.add(new ASTOperator(at(opContext), operator));
      } else if (child instanceof BinOpContext) {
        BinOpContext binOpContext = (BinOpContext) child;
        ASTOperators operator = null;
        if (binOpContext.plus() != null) {
          operator = ASTOperators.ADD;
        } else if (binOpContext.minus() != null) {
          operator = ASTOperators.SUB;
        } else if (binOpContext.mult() != null) {
          operator = ASTOperators.MUL;
        } else if (binOpContext.div() != null) {
          operator = ASTOperators.DIV;
        } else if (binOpContext.mod() != null) {
          operator = ASTOperators.MOD;
        } else if (binOpContext.bitAnd() != null) {
          operator = ASTOperators.BIT_AND;
        } else if (binOpContext.bitOr() != null) {
          operator = ASTOperators.BIT_OR;
        } else if (binOpContext.bitXor() != null) {
          operator = ASTOperators.BIT_XOR;
        } else if (binOpContext.equalsTo() != null) {
          operator = ASTOperators.EQUALS_TO;
        } else if (binOpContext.notEqualsTo() != null) {
          operator = ASTOperators.NOT_EQUALS_TO;
        } else if (binOpContext.is() != null) {
          operator = ASTOperators.IS;
        } else if (binOpContext.isNot() != null) {
          operator = ASTOperators.IS_NOT;
        } else if (binOpContext.and() != null) {
          operator = ASTOperators.AND;
        } else if (binOpContext.or() != null) {
          operator = ASTOperators.OR;
        } else {
          throw new UnsupportedOperationException();
        }
        elements.add(new ASTOperator(at(binOpContext), operator));
      } else {
        throw new UnsupportedOperationException();
      }
    }

    return new ExpressionNode(at(expressionContext), elements);
  }

  private List<ParamNode> createParams(final ParamsContext paramsContext) {
    List<ParamNode> params = new ArrayList<>();

    params.add(new ParamNode(
      at(paramsContext.param()),
      paramsContext.param().name() == null ? null : paramsContext.param().name().getText(),
      create(paramsContext.param().expression())
    ));
    if (paramsContext.params() != null) {
      params.addAll(createParams(paramsContext.params()));
    }

    return params;
  }
}
