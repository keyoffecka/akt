package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.OperatorEvent;
import net.offecka.pet.akt.generator.context.ContextManager;

import java.io.IOException;
import java.io.Writer;

import static net.offecka.pet.akt.expression.OperatorDescriptor.OperatorDescriptors.ADD;
import static net.offecka.pet.akt.expression.OperatorDescriptor.OperatorDescriptors.CLOSE;
import static net.offecka.pet.akt.expression.OperatorDescriptor.OperatorDescriptors.MINUS;
import static net.offecka.pet.akt.expression.OperatorDescriptor.OperatorDescriptors.NOT;
import static net.offecka.pet.akt.expression.OperatorDescriptor.OperatorDescriptors.OPEN;
import static net.offecka.pet.akt.expression.OperatorDescriptor.OperatorDescriptors.SUB;
import static net.offecka.pet.akt.generator.context.MarkerContext.Markers.NEGATE;

public class OperatorEventHandler implements EventHandler<OperatorEvent> {
  private final Writer writer;
  private final ContextManager contextManager;

  public OperatorEventHandler(final Writer writer, final ContextManager contextManager) {
    this.writer = writer;
    this.contextManager = contextManager;
  }

  @Override
  public void handle(final OperatorEvent event) throws IOException {
    if (event.descriptor == OPEN || event.descriptor == CLOSE) {
      writer.write(event.descriptor.name);
    } else {
      if (event.descriptor == MINUS) {
        writer.write("-");
      } else if (event.descriptor == ADD) {
        writer.write(" + ");
      } else if (event.descriptor == SUB) {
        writer.write(" - ");
      } else if (event.descriptor == NOT) {
        contextManager.openMarkerContext(NEGATE);
      } else {
        writer.write(" " + event.descriptor.name + " ");
      }
    }
  }
}
