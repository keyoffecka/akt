package net.offecka.pet.akt.analyzer.ast.factories;

import net.offecka.pet.akt.analyzer.ast.ArgumentNode;
import net.offecka.pet.akt.analyzer.ast.ExpressionNode;
import net.offecka.pet.akt.analyzer.ast.FunctionNode;
import net.offecka.pet.akt.analyzer.ast.TypeNode;
import net.offecka.pet.akt.grammar.AktParser.ArgumentsContext;
import net.offecka.pet.akt.grammar.AktParser.FunctionContext;

import java.util.ArrayList;
import java.util.List;

import static java.util.List.of;
import static net.offecka.pet.akt.analyzer.ast.factories.NodeFactories.NODE_FACTORIES;
import static net.offecka.pet.akt.errors.Location.after;
import static net.offecka.pet.akt.errors.Location.at;

public class FunctionNodeFactory {
  FunctionNodeFactory() {
  }

  public FunctionNode create(final FunctionContext functionContext) {
    return new FunctionNode(
      at(functionContext),
      at(functionContext.name()),
      at(functionContext.blockBody().RBRACE().getSymbol()),
      NODE_FACTORIES.scopeFactory.create(functionContext.namespaceMemberScope()),
      functionContext.name().getText(),
      functionContext.type() == null
        ? new TypeNode(after(functionContext.RPAREN().getSymbol()), null)
        : NODE_FACTORIES.typeNodeFactory.create(functionContext.type()),
      createArguments(functionContext.arguments()),
      NODE_FACTORIES.blockBodyNodeFactory.create(functionContext.blockBody())
    );
  }

  private List<ArgumentNode> createArguments(final ArgumentsContext argument) {
    List<ArgumentNode> arguments = new ArrayList<>();

    if (argument != null) {
      arguments.add(new ArgumentNode(
        at(argument),
        argument.argument().name().getText(),
        new TypeNode(
          after(argument.argument().COLON().getSymbol()),
          NODE_FACTORIES.fqnNodeFactory.create(argument.argument().type().fqn())
        ),
        argument.argument().expression() == null ? new ExpressionNode(
          after(argument.argument().type().stop),
          of()
        ) : NODE_FACTORIES.chainNodeFactory.create(argument.argument().expression())
      ));
      arguments.addAll(createArguments(argument.arguments()));
    }

    return arguments;
  }
}
