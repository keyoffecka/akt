package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginFunctionEvent;
import net.offecka.pet.akt.generator.context.ContextManager;

import java.io.IOException;
import java.io.Writer;

public class BeginFunctionEventHandler implements EventHandler<BeginFunctionEvent> {
  private final Writer writer;
  private final Tab tab;
  private final ContextManager contextManager;

  public BeginFunctionEventHandler(
    final Writer writer,
    final Tab tab,
    final ContextManager contextManager
  ) {
    this.writer = writer;
    this.tab = tab;
    this.contextManager = contextManager;
  }

  @Override
  public void handle(final BeginFunctionEvent event) throws IOException {
    contextManager.openObjectContext(event);

    writer.write(tab + "function " + event.name + "(");
  }
}
