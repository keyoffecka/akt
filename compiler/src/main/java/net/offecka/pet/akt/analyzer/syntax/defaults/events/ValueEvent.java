package net.offecka.pet.akt.analyzer.syntax.defaults.events;

import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.events.Event;

public class ValueEvent implements Event {
  public final Object value;
  public final Type type;

  public ValueEvent(final Object value, final Type type) {
    this.value = value;
    this.type = type;
  }
}
