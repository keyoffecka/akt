package net.offecka.pet.akt.analyzer.syntax;

import net.offecka.pet.akt.analyzer.syntax.defaults.details.ArgumentDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.StatementDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.exceptions.CannotCast;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.exceptions.ParameterRequired;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.exceptions.TooManyParameters;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.result.BlockStatementResultBatch;
import net.offecka.pet.akt.analyzer.syntax.support.ToBeCommitted;
import net.offecka.pet.akt.analyzer.syntax.support.WithName;
import net.offecka.pet.akt.analyzer.syntax.support.WithType;
import net.offecka.pet.akt.storage.Item;
import net.offecka.pet.akt.storage.Storage;

public interface Function extends Item, WithName, WithType, Storage {
  ToBeCommitted addArgument(ArgumentDetails argumentDetails);
  BlockStatementResultBatch addStatement(StatementDetails statementDetails);
  void checkParameterIndex(int paramIndex, Name paramName) throws TooManyParameters;
  void checkParameterType(int paramIndex, Type paramType) throws CannotCast;
  void checkParameterCount(int paramCount) throws ParameterRequired;
}
