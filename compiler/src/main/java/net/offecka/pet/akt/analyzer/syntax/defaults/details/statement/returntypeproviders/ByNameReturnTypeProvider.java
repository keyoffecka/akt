package net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.returntypeproviders;

import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.storage.Storage;
import net.offecka.pet.akt.storage.exceptions.StorageException;

import static net.offecka.pet.akt.storage.FinderType.LOCAL;

public class ByNameReturnTypeProvider implements ReturnTypeProvider {
  private final Name returnTypeName;

  public ByNameReturnTypeProvider(final Name returnTypeName) {
    this.returnTypeName = returnTypeName;
  }

  @Override
  public Type provide(final Storage storage) throws StorageException {
    return storage.find(LOCAL, returnTypeName).item(Type.class);
  }
}
