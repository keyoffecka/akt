package net.offecka.pet.akt.analyzer.syntax;

import net.offecka.pet.akt.analyzer.syntax.defaults.details.AliasDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.ConstantDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.FunctionDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.TypeDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.StatementDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.result.BlockStatementResultBatch;
import net.offecka.pet.akt.analyzer.syntax.support.ToBeCommitted;
import net.offecka.pet.akt.analyzer.syntax.support.WithName;
import net.offecka.pet.akt.storage.Item;
import net.offecka.pet.akt.storage.Storage;

public interface Namespace extends Storage, Item, WithName {
  void addAlias(AliasDetails aliasDetails);
  void addConstant(ConstantDetails constantDetails);
  ToBeCommitted addFunction(FunctionDetails functionDetails);
  BlockStatementResultBatch addStatement(StatementDetails statementDetails);
  void addType(final TypeDetails typeDetails);
}
