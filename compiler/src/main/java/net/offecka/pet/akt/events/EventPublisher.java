package net.offecka.pet.akt.events;

public interface EventPublisher {
  void publish(Event event);
}
