package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.ValueEvent;
import net.offecka.pet.akt.analyzer.syntax.support.WithName;
import net.offecka.pet.akt.generator.context.ContextManager;

import java.io.IOException;
import java.io.Writer;
import java.util.Locale;

import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.BOOL;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.INT;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.REAL;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.STR;

public class ValueEventHandler implements EventHandler<ValueEvent> {
  private final Writer writer;
  private final ContextManager contextManager;

  public ValueEventHandler(final Writer writer, final ContextManager contextManager) {
    this.writer = writer;
    this.contextManager = contextManager;
  }

  @Override
  public void handle(final ValueEvent event) throws IOException {
    if (!(event.type instanceof WithName)) {
      throw new IllegalStateException(event.type + " unsupported");
    }

    writer.write(contextManager.negateIfRequired(event.type));

    WithName withName = (WithName) event.type;
    if (withName.name() == INT) {
      writer.write(String.format(Locale.US, "%d", event.value));
    } else if (withName.name() == REAL) {
      writer.write(String.format(Locale.US, "%g", event.value));
    } else if (withName.name() == STR) {
      writer.write("\"" + event.value + "\"");
    } else if (withName.name() == BOOL) {
      writer.write(((Boolean) event.value).toString().toLowerCase());
    } else {
      throw new IllegalStateException(event.type + " unsupported");
    }
  }
}
