package net.offecka.pet.akt.analyzer.syntax.defaults.statement.exceptions;

import net.offecka.pet.akt.analyzer.syntax.ModelException;
import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.Type;

public class CannotCast extends ModelException {
  public final Name name;
  public final Type actual;
  public final Type expected;

  public CannotCast(final Type actual, final Type expected) {
    super("[" + actual.description() + "] cannot be cast to [" + expected.description() + "]");

    this.name = null;
    this.actual = actual;
    this.expected = expected;
  }

  public CannotCast(final Name name, final Type actual, final Type expected) {
    super("[" + name + "] of type [" + expected.description() + "] cannot be assigned a value of type [" + actual.description() + "]");

    this.name = name;
    this.actual = actual;
    this.expected = expected;
  }
}
