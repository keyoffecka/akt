package net.offecka.pet.akt.analyzer.ast.expression.operands;

import net.offecka.pet.akt.errors.Location;

public class ASTBoolLiteralOperand extends ASTLiteralOperand<Boolean> {
  public ASTBoolLiteralOperand(final Location location, final Boolean value) {
    super(location, value);
  }
}
