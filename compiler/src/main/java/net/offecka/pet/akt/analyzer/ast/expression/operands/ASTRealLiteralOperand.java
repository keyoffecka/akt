package net.offecka.pet.akt.analyzer.ast.expression.operands;

import net.offecka.pet.akt.errors.Location;

public class ASTRealLiteralOperand extends ASTNumLiteralOperand<String> {
  public ASTRealLiteralOperand(final Location location, final String value) {
    super(location, value);
  }
}
