package net.offecka.pet.akt.analyzer.ast;

import net.offecka.pet.akt.errors.Location;

import java.util.List;

import static java.util.List.copyOf;

public class ChainNode implements Node {
  public final Location location;
  public final List<ChainSegmentNode> segments;

  public ChainNode(final Location location, final List<ChainSegmentNode> segments) {
    this.location = location;
    this.segments = copyOf(segments);
  }
}
