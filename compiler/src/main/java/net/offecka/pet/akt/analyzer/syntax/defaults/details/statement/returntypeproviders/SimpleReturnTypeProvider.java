package net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.returntypeproviders;

import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.storage.Storage;

public class SimpleReturnTypeProvider implements ReturnTypeProvider {
  private final Type type;

  public SimpleReturnTypeProvider(final Type type) {
    this.type = type;
  }

  @Override
  public Type provide(final Storage storage) {
    return type;
  }
}
