package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.ElseEvent;

import java.io.IOException;
import java.io.Writer;

public class ElseEventHandler implements EventHandler<ElseEvent> {
  private final Writer writer;

  public ElseEventHandler(final Writer writer) {
    this.writer = writer;
  }

  @Override
  public void handle(final ElseEvent event) throws IOException {
    writer.write("else");
  }
}
