package net.offecka.pet.akt.analyzer.syntax.defaults.statement.processors;

import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginStatementEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndStatementEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.StatementType;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.exceptions.CannotCast;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.operands.StatementOperand;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.result.BlockStatementResultBatch;
import net.offecka.pet.akt.errors.Location;
import net.offecka.pet.akt.events.EventPublisher;
import net.offecka.pet.akt.expression.CannotEvaluate;
import net.offecka.pet.akt.expression.Expression;
import net.offecka.pet.akt.storage.MutableStorage;

import static net.offecka.pet.akt.errors.ErrorContext.errorContext;

public class ReturnProcessor extends ExpressionStatementProcessor {
  private final Type returnType;
  private final Location startLocation;

  public ReturnProcessor(
    final Location startLocation,
    final Location expressionLocation,
    final Type returnType,
    final Expression expression,
    final EventPublisher publisher
  ) {
    super(expressionLocation, expression, publisher);

    if (returnType == null) {
      throw new IllegalArgumentException();
    }

    this.startLocation = startLocation;
    this.returnType = returnType;
  }

  @Override
  public BlockStatementResultBatch process(final MutableStorage storage) {
    publisher.publish(new BeginStatementEvent(StatementType.RETURN));

    Type leftType = returnType;

    StatementOperand operand = null;
    try {
      operand = (StatementOperand) expression.evaluate();
    } catch (final CannotEvaluate ex) {
      errorContext().addError(ex.location, ex);
    }
    if (operand != null) {
      Type rightType = operand.type();

      if (!rightType.isSubOf(leftType)) {
        errorContext().addException(location, new CannotCast(rightType, leftType));
      }
    }

    publishEvents(expression.elements, storage);

    publisher.publish(new EndStatementEvent());

    return new BlockStatementResultBatch(startLocation)
      .add(returnType, location, false);
  }
}
