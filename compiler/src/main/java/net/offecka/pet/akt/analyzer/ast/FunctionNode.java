package net.offecka.pet.akt.analyzer.ast;

import net.offecka.pet.akt.errors.Location;

import java.util.List;

public class FunctionNode implements Node {
  public final Location location;
  public final Location nameLocation;
  public final Location endLocation;
  public final Scopes scope;
  public final String name;
  public final TypeNode returnType;
  public final List<ArgumentNode> arguments;
  public final BlockBodyNode block;

  public FunctionNode(
    final Location location,
    final Location nameLocation,
    final Location endLocation,
    final Scopes scope,
    final String name,
    final TypeNode returnType,
    final List<ArgumentNode> arguments,
    final BlockBodyNode block
  ) {
    this.location = location;
    this.nameLocation = nameLocation;
    this.endLocation = endLocation;
    this.scope = scope;
    this.name = name;
    this.returnType = returnType;
    this.arguments = arguments;
    this.block = block;
  }
}
