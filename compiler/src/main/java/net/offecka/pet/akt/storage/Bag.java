package net.offecka.pet.akt.storage;

import net.offecka.pet.akt.analyzer.syntax.Name;

public class Bag {
  public final Name name;
  public final Item item;
  public final Visibility visibility;

  public Bag(final Name name, final Item item, final Visibility visibility) {
    this.name = name;
    this.item = item;
    this.visibility = visibility;
  }
}
