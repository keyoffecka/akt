package net.offecka.pet.akt.analyzer.ast;

public interface ChainSegmentNode extends Node {
  String name();
}
