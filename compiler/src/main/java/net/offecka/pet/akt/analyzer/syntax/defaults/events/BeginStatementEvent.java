package net.offecka.pet.akt.analyzer.syntax.defaults.events;

import net.offecka.pet.akt.analyzer.syntax.defaults.statement.StatementType;
import net.offecka.pet.akt.events.Event;

public class BeginStatementEvent implements Event {
  public final StatementType statementType;

  public BeginStatementEvent(final StatementType statementType) {
    this.statementType = statementType;
  }
}
