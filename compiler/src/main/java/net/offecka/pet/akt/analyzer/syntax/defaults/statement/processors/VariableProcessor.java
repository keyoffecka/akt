package net.offecka.pet.akt.analyzer.syntax.defaults.statement.processors;

import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.analyzer.syntax.Variable;
import net.offecka.pet.akt.analyzer.syntax.defaults.DefaultVariable;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginStatementEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndStatementEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.VariableEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.StatementType;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.exceptions.CannotCast;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.operands.StatementOperand;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.result.BlockStatementResultBatch;
import net.offecka.pet.akt.errors.Location;
import net.offecka.pet.akt.events.EventPublisher;
import net.offecka.pet.akt.expression.CannotEvaluate;
import net.offecka.pet.akt.expression.Expression;
import net.offecka.pet.akt.storage.MutableStorage;
import net.offecka.pet.akt.storage.exceptions.ExistedInChild;
import net.offecka.pet.akt.storage.exceptions.Exists;
import net.offecka.pet.akt.storage.exceptions.ExistsInStorage;
import net.offecka.pet.akt.storage.exceptions.NotFound;
import net.offecka.pet.akt.storage.exceptions.StorageException;
import net.offecka.pet.akt.storage.exceptions.UnexpectedType;

import static net.offecka.pet.akt.errors.ErrorContext.errorContext;
import static net.offecka.pet.akt.storage.FinderType.LOCAL;
import static net.offecka.pet.akt.storage.Visibility.PRIVATE;

public class VariableProcessor extends AssignmentProcessor {
  protected final boolean unmodifiableVariable;
  protected final Type variableType;

  public VariableProcessor(
    final Location location,
    final boolean unmodifiableVariable,
    final Name variableName,
    final Type variableType,
    final Expression expression,
    final EventPublisher publisher
  ) {
    super(location, variableName, expression, publisher);

    this.variableType = variableType;
    this.unmodifiableVariable = unmodifiableVariable;
  }

  @Override
  public BlockStatementResultBatch process(final MutableStorage storage) {
    publisher.publish(new BeginStatementEvent(StatementType.NORMAL));

    Type rightType = null;
    Type leftType = variableType;

    StatementOperand operand = null;
    try {
      operand = (StatementOperand) expression.evaluate();
    } catch (final CannotEvaluate ex) {
      errorContext().addError(ex.location, ex);
    }

    if (operand != null) {
      rightType = operand.type();

      if (leftType == null) {
        leftType = rightType;
      } else {
        if (!rightType.isSubOf(leftType)) {
          errorContext().addException(
            variableName.location(),
            new CannotCast(variableName, rightType, leftType)
          );
        }
      }
    }

    try {
      Variable oldVariable = storage.find(LOCAL, variableName).item(Variable.class);
      errorContext().addException(
        variableName.location(),
        new ExistsInStorage(variableName, oldVariable.name())
      );
    } catch (final NotFound ex) {
      if (leftType != null) {
        Variable variable = new DefaultVariable(unmodifiableVariable, variableName, leftType);
        try {
          storage.put(PRIVATE, variable.name(), variable);
        } catch (final ExistsInStorage | ExistedInChild ex2) {
          errorContext().addError(ex2.name.location(), ex2);
        } catch (final Exists ex2) {
          throw new IllegalStateException(ex2);
        }
      }
    } catch (final UnexpectedType ex) {
      errorContext().addError(ex.name.location(), ex);
    } catch (final StorageException ex) {
      throw new IllegalStateException(ex);
    }

    if (leftType != null && rightType != null) {
      publisher.publish(new VariableEvent(variableName, leftType, rightType, unmodifiableVariable));
      publishEvents(expression.elements, storage);
    }

    publisher.publish(new EndStatementEvent());

    return new BlockStatementResultBatch(location);
  }
}
