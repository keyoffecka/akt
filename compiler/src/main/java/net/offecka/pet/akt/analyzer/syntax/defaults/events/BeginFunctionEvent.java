package net.offecka.pet.akt.analyzer.syntax.defaults.events;

import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.events.Event;
import net.offecka.pet.akt.storage.Visibility;

public class BeginFunctionEvent implements Event {
  public final Visibility visibility;
  public final Name name;

  public BeginFunctionEvent(final Visibility visibility, final Name name) {
    this.visibility = visibility;
    this.name = name;
  }
}
