package net.offecka.pet.akt.storage;

import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.support.WithName;
import net.offecka.pet.akt.storage.exceptions.UnexpectedType;

public class Result implements WithName {
  public final Name name;
  public final Storage owner;
  public final Visibility visibility;
  public final Item item;

  public Result(final Storage owner, final Visibility visibility, final Name name, final Item item) {
    this.visibility = visibility;
    if (owner == null) {
      throw new IllegalArgumentException();
    }
    if (item == null) {
      throw new IllegalArgumentException();
    }

    this.owner = owner;
    this.name = name;
    this.item = item;
  }

  @Override
  public Name name() {
    return name;
  }

  public <T> T item(final Class<T> type) throws UnexpectedType {
    if (!type.isInstance(item)) {
      throw new UnexpectedType(name, item.getClass(), type);
    }
    return (T) item;
  }
}
