package net.offecka.pet.akt.analyzer.syntax.defaults.statement.result;

import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.errors.Location;

import java.util.ArrayList;
import java.util.List;

import static java.util.List.copyOf;

public class BlockStatementResultBatch {
  private final List<BlockStatementResult> batch = new ArrayList<>();
  private final Location startLocation;

  public BlockStatementResultBatch(final Location startLocation) {
    this.startLocation = startLocation;
  }

  public BlockStatementResultBatch add(
    final Type type,
    final Location returnStatementExpressionLocation,
    final boolean hasUnreachableStatements
  ) {
    batch.add(new BlockStatementResult(
      type, returnStatementExpressionLocation, hasUnreachableStatements
    ));
    return this;
  }

  public Location startLocation() {
    return startLocation;
  }

  public List<BlockStatementResult> blockStatementResults() {
    return copyOf(batch);
  }
}
