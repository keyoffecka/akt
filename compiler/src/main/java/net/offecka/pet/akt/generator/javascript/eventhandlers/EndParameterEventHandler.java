package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndParameterEvent;

public class EndParameterEventHandler implements EventHandler<EndParameterEvent> {
  @Override
  public void handle(final EndParameterEvent event) {
  }
}
