package net.offecka.pet.akt.analyzer.collector;

import net.offecka.pet.akt.analyzer.ast.ExpressionNode;
import net.offecka.pet.akt.analyzer.ast.StepNode;
import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.Namespace;
import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.analyzer.syntax.defaults.DefaultNamespace;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.AliasDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.FunctionDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.AssignmentStatementDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.DefaultExpressionStatementDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.StatementDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.StatementDetailsFactory;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.VariableStatementDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.returntypeproviders.SimpleReturnTypeProvider;
import net.offecka.pet.akt.analyzer.syntax.defaults.support.ExpressionProvider;
import net.offecka.pet.akt.analyzer.syntax.support.ToBeCommitted;
import net.offecka.pet.akt.events.EventPublisher;
import net.offecka.pet.akt.generator.Unit;
import net.offecka.pet.akt.grammar.AktParser.BlockContext;
import net.offecka.pet.akt.grammar.AktParser.FunctionContext;
import net.offecka.pet.akt.grammar.AktParser.NamespaceAssignmentContext;
import net.offecka.pet.akt.grammar.AktParser.NamespaceCallContext;
import net.offecka.pet.akt.grammar.AktParser.NamespaceContext;
import net.offecka.pet.akt.grammar.AktParser.NamespaceVarDefContext;
import net.offecka.pet.akt.storage.exceptions.ExistedInChild;
import net.offecka.pet.akt.storage.exceptions.Exists;
import net.offecka.pet.akt.storage.exceptions.ExistsInStorage;
import net.offecka.pet.akt.storage.exceptions.IllegalAccess;
import net.offecka.pet.akt.storage.exceptions.NotFound;
import net.offecka.pet.akt.storage.exceptions.StorageException;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static net.offecka.pet.akt.analyzer.ast.factories.NodeFactories.NODE_FACTORIES;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.ANY;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.BOOL;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.FALSE_CONST;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.INT;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.NOTHING;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.NULL;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.NULL_CONST;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.REAL;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.STR;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.TRUE_CONST;
import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.VOID;
import static net.offecka.pet.akt.analyzer.syntax.NameFactory.NAME_FACTORY;
import static net.offecka.pet.akt.errors.ErrorContext.errorContext;
import static net.offecka.pet.akt.errors.Location.NO_LOCATION;
import static net.offecka.pet.akt.storage.FinderType.FOREIGN;
import static net.offecka.pet.akt.storage.Visibility.PUBLIC;

public class Collector {
  public static final Name SYS = NAME_FACTORY.create("sys", NO_LOCATION);
  public static final Name SYS__FALSE_CONST = NAME_FACTORY.create(SYS, FALSE_CONST);
  public static final Name SYS__TRUE_CONST = NAME_FACTORY.create(SYS, TRUE_CONST);
  public static final Name SYS__NULL_CONST = NAME_FACTORY.create(SYS, NULL_CONST);
  public static final Name SYS__ANY = NAME_FACTORY.create(SYS, ANY);
  public static final Name SYS__BOOL = NAME_FACTORY.create(SYS, BOOL);
  public static final Name SYS__STR = NAME_FACTORY.create(SYS, STR);
  public static final Name SYS__REAL = NAME_FACTORY.create(SYS, REAL);
  public static final Name SYS__INT = NAME_FACTORY.create(SYS, INT);
  public static final Name SYS__NOTHING = NAME_FACTORY.create(SYS, NOTHING);
  public static final Name SYS__VOID = NAME_FACTORY.create(SYS, VOID);
  public static final Name SYS__NULL = NAME_FACTORY.create(SYS, NULL);
  public static final Name BROWSER = NAME_FACTORY.create("browser", NO_LOCATION);
  public static final Name NODE = NAME_FACTORY.create("node", NO_LOCATION);
  public static final Name MODULES = NAME_FACTORY.create("modules", NO_LOCATION);
  public static final Name GLOBALS = NAME_FACTORY.create("globals", NO_LOCATION);

  private final Namespace root;
  private final AliasImporter aliasImporter;
  private final StatementDetailsFactory statementDetailsFactory;
  private final Type nothing;

  public Collector(final Namespace root, final UnitParser unitParser) {
    this.root = root;

    try {
      nothing = root.find(FOREIGN, SYS__NOTHING).item(Type.class);
    } catch (StorageException ex) {
      throw new IllegalStateException(ex);
    }
    statementDetailsFactory = new StatementDetailsFactory();
    aliasImporter = new AliasImporter(this, unitParser);
  }

  public Namespace root() {
    return root;
  }

  public void collect(final Unit unit, final NamespaceContext nsContext) {
    Namespace namespace = createMissingParentNamespaces(unit.name(), root, unit.publisher());

    importGlobalDefaults(root, namespace);

    List<ToBeCommitted> toBeCommittedList = new ArrayList<>();
    if (nsContext.children != null) {
      aliasImporter.importAliases(namespace, nsContext, unit);
      toBeCommittedList = addFunctions(namespace, nsContext);
      addStatements(namespace, nsContext);
    }
    toBeCommittedList.forEach(ToBeCommitted::commit);
  }

  private List<ToBeCommitted> addFunctions(final Namespace namespace, final NamespaceContext nsContext) {
    List<ToBeCommitted> toBeCommittedList = new ArrayList<>();
    for (final ParseTree child : nsContext.children) {
      if (child instanceof FunctionContext) {
        toBeCommittedList.add(namespace.addFunction(new FunctionDetails(NODE_FACTORIES.functionNodeFactory.create((FunctionContext) child))));
      }
    }
    return toBeCommittedList;
  }

  private void addStatements(final Namespace namespace, final NamespaceContext nsContext) {
    for (final ParseTree child : nsContext.children) {
      if (child instanceof NamespaceVarDefContext) {
        namespace.addStatement(new VariableStatementDetails(NODE_FACTORIES.variableNodeFactory.create((NamespaceVarDefContext) child)));
      } else if (child instanceof NamespaceAssignmentContext) {
        namespace.addStatement(new AssignmentStatementDetails(NODE_FACTORIES.assignmentNodeFactory.create((NamespaceAssignmentContext) child)));
      } else if (child instanceof NamespaceCallContext) {
        addCallStatement(namespace, (NamespaceCallContext) child);
      } else if (child instanceof BlockContext) {
        addBlockStatement(namespace, (BlockContext) child);
      }
    }
  }

  private void addBlockStatement(final Namespace namespace, final BlockContext child) {
    List<StepNode> steps = Collections.singletonList(
      NODE_FACTORIES.blockNodeFactory.create(child)
    );
    List<StatementDetails> statementDetailsList = statementDetailsFactory.create(
      new SimpleReturnTypeProvider(nothing),
      steps
    );
    for (final StatementDetails statementDetails : statementDetailsList) {
      namespace.addStatement(statementDetails);
    }
  }

  private void addCallStatement(final Namespace namespace, final NamespaceCallContext namespaceCallContext) {
    ExpressionNode expressionNode = NODE_FACTORIES.chainNodeFactory.create(namespaceCallContext.expression());
    namespace.addStatement(new DefaultExpressionStatementDetails(
      new ExpressionProvider(expressionNode.elements()),
      expressionNode.location
    ));
  }

  private void importGlobalDefaults(final Namespace root, final Namespace namespace) {
    namespace.addAlias(new AliasDetails(NULL, root, SYS__NULL));
    namespace.addAlias(new AliasDetails(VOID, root, SYS__VOID));
    namespace.addAlias(new AliasDetails(STR, root, SYS__STR));
    namespace.addAlias(new AliasDetails(INT, root, SYS__INT));
    namespace.addAlias(new AliasDetails(REAL, root, SYS__REAL));
    namespace.addAlias(new AliasDetails(BOOL, root, SYS__BOOL));
    namespace.addAlias(new AliasDetails(ANY, root, SYS__ANY));
    namespace.addAlias(new AliasDetails(TRUE_CONST, root, SYS__TRUE_CONST));
    namespace.addAlias(new AliasDetails(FALSE_CONST, root, SYS__FALSE_CONST));
    namespace.addAlias(new AliasDetails(NULL_CONST, root, SYS__NULL_CONST));
  }

  private Namespace createMissingParentNamespaces(
    final Name name,
    final Namespace parent,
    final EventPublisher publisher
  ) {
    DefaultNamespace p = (DefaultNamespace) parent;

    Name current = name;
    while (current != null) {
      DefaultNamespace child = null;
      Name namespaceName = NAME_FACTORY.create(current.name, name.location());
      try {
        child = (DefaultNamespace) p.find(FOREIGN, namespaceName).item(Namespace.class);
      } catch (final NotFound ex) {
        child = new DefaultNamespace(namespaceName, publisher);
        try {
          p.put(PUBLIC, child.name(), child);
        } catch (final ExistsInStorage | ExistedInChild ex2) {
          errorContext().addError(ex2.name.location(), ex2);
        } catch (final Exists ex2) {
          throw new IllegalStateException(ex2);
        }
      } catch (final IllegalAccess ex) {
        errorContext().addError(ex.name.location(), ex);
      } catch (final StorageException ex) {
        throw new IllegalStateException(ex);
      }
      p = child;
      current = current.child;
    }
    return p;
  }
}
