package net.offecka.pet.akt.analyzer.ast;

import net.offecka.pet.akt.errors.Location;

public class ReturnNode extends TermNode {
  public final ExpressionNode expressionNode;

  public ReturnNode(final ExpressionNode expressionNode, final Location location) {
    super(location);

    this.expressionNode = expressionNode;
  }
}
