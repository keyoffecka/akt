package net.offecka.pet.akt.generator;

import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.events.EventPublisher;

import java.io.Closeable;
import java.io.IOException;
import java.io.Reader;

public interface Unit extends Closeable {
  Name name();
  String resourceName();
  Reader reader() throws IOException;
  EventPublisher publisher();
  Unit unit(Name name) throws IOException;
}
