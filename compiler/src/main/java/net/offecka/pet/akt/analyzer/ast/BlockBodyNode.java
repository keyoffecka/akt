package net.offecka.pet.akt.analyzer.ast;

import net.offecka.pet.akt.errors.Location;

import java.util.List;

import static java.util.List.copyOf;

public class BlockBodyNode implements StepNode {
  public final Location startLocation;
  public final Location endLocation;
  public final List<StepNode> steps;

  public BlockBodyNode(
    final Location startLocation,
    final Location endLocation,
    final List<StepNode> steps
  ) {
    this.startLocation = startLocation;
    this.endLocation = endLocation;
    this.steps = copyOf(steps);
  }
}
