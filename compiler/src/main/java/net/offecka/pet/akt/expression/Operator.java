package net.offecka.pet.akt.expression;

import java.util.List;

public interface Operator extends Element {
  OperatorDescriptor descriptor();
  Operand evaluate(List<Operand> operands) throws CannotEvaluate;
}
