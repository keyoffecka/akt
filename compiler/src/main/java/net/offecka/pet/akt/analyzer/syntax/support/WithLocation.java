package net.offecka.pet.akt.analyzer.syntax.support;

import net.offecka.pet.akt.errors.Location;

public interface WithLocation {
  Location location();
}
