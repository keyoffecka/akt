package net.offecka.pet.akt.events;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;

public class DefaultEventSource implements EventSource, EventPublisher {
  private static final Logger LOGGER = Logger.getLogger(DefaultEventSource.class.getName());

  private final Set<Listener> listeners = new HashSet<>();

  @Override
  public void addListener(final Listener listener) {
    listeners.add(listener);
  }

  @Override
  public void publish(final Event event) {
    for (final Listener listener : listeners) {
      try {
        listener.onEvent(event);
      } catch (final Exception ex) {
        LOGGER.log(SEVERE, "Unhandled error", ex);
      }
    }
  }
}
