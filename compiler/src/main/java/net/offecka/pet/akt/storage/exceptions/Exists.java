package net.offecka.pet.akt.storage.exceptions;

import net.offecka.pet.akt.analyzer.syntax.Name;

public class Exists extends StorageException{
  public Exists(final Name name, final Name prev) {
    super(
      "[" + name + "] exists"
        + (prev.location().resourceName == null
            ? "" : (" at " + prev.location().line + ":" + prev.location().column)
        ),
      name
    );
  }
}
