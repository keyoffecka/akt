package net.offecka.pet.akt.analyzer.syntax.defaults.statement.exceptions;

import net.offecka.pet.akt.analyzer.syntax.ModelException;
import net.offecka.pet.akt.analyzer.syntax.Name;

public class TooManyParameters extends ModelException {
  public TooManyParameters(final int index) {
    super("Unexpected parameter " + index);
  }

  public TooManyParameters(final Name name) {
    super("Unexpected parameter [" + name + "]");
  }
}
