package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndChainEvent;
import net.offecka.pet.akt.generator.context.ContextManager;

public class EndChainEventHandler implements EventHandler<EndChainEvent> {
  private final ContextManager contextManager;

  public EndChainEventHandler(final ContextManager contextManager) {
    this.contextManager = contextManager;
  }

  @Override
  public void handle(final EndChainEvent event) {
    contextManager.closeContext();
  }
}
