package net.offecka.pet.akt.analyzer.syntax;

public class ModelException extends Exception {
  public ModelException(final String message) {
    super(message);
  }
}
