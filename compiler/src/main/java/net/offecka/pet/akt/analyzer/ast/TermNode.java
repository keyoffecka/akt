package net.offecka.pet.akt.analyzer.ast;

import net.offecka.pet.akt.errors.Location;

public class TermNode implements StepNode {
  public final Location location;

  public TermNode(final Location location) {
    this.location = location;
  }
}
