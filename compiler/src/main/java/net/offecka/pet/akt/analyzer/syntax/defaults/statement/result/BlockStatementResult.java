package net.offecka.pet.akt.analyzer.syntax.defaults.statement.result;

import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.errors.Location;

public final class BlockStatementResult {
  public final Type type;
  public final Location returnStatementExpressionLocation;
  public final boolean hasUnreachableStatements;

  BlockStatementResult(
    final Type type,
    final Location returnStatementExpressionLocation,
    final boolean hasUnreachableStatements
  ) {
    this.type = type;
    this.returnStatementExpressionLocation = returnStatementExpressionLocation;
    this.hasUnreachableStatements = hasUnreachableStatements;
  }
}
