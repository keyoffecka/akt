package net.offecka.pet.akt.analyzer.ast.factories;

import net.offecka.pet.akt.analyzer.ast.FQNNode;
import net.offecka.pet.akt.analyzer.ast.NameNode;
import net.offecka.pet.akt.errors.Location;
import net.offecka.pet.akt.grammar.AktParser.FqnContext;

import java.util.ArrayList;
import java.util.List;

public class FQNNodeFactory {
  FQNNodeFactory() {
  }

  public FQNNode create(final FqnContext fqn) {
    List<NameNode> names = new ArrayList<>();

    FqnContext f = fqn;
    do {
      names.add(new NameNode(Location.at(f.name()), f.name().getText()));
      f = f.fqn();
    } while (f != null);
    return new FQNNode(Location.at(fqn), names);
  }
}
