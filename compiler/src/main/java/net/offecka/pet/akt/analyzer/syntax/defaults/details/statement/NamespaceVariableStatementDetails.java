package net.offecka.pet.akt.analyzer.syntax.defaults.details.statement;

import net.offecka.pet.akt.analyzer.ast.NamespaceVariableNode;
import net.offecka.pet.akt.storage.Visibility;
import net.offecka.pet.akt.storage.VisibilityFactory;

public class NamespaceVariableStatementDetails extends VariableStatementDetails {
  private final boolean unmodifiable;
  private final Visibility variableVisibility;

  public NamespaceVariableStatementDetails(final NamespaceVariableNode namespaceVariableNode) {
    super(namespaceVariableNode);

    unmodifiable = namespaceVariableNode.unmodifiable;
    variableVisibility = new VisibilityFactory().create(namespaceVariableNode.scope);
  }

  public Visibility variableVisibility() {
    return variableVisibility;
  }

  public boolean unmodifiable() {
    return unmodifiable;
  }
}
