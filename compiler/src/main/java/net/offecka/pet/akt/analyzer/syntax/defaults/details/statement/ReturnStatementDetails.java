package net.offecka.pet.akt.analyzer.syntax.defaults.details.statement;

import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.returntypeproviders.ReturnTypeProvider;
import net.offecka.pet.akt.analyzer.syntax.defaults.support.ExpressionProvider;
import net.offecka.pet.akt.errors.Location;

public class ReturnStatementDetails implements ExpressionStatementDetails, WithStartLocation, WithReturnTypeProvider {
  private final ReturnTypeProvider returnTypeProvider;
  private final ExpressionProvider expressionProvider;
  private final Location startLocation;
  private final Location expressionLocation;

  public ReturnStatementDetails(
    final ReturnTypeProvider returnTypeProvider,
    final ExpressionProvider expressionProvider,
    final Location startLocation,
    final Location expressionLocation
  ) {
    this.returnTypeProvider = returnTypeProvider;
    this.expressionProvider = expressionProvider;
    this.startLocation = startLocation;
    this.expressionLocation = expressionLocation;
  }

  @Override
  public Location startLocation() {
    return startLocation;
  }

  @Override
  public Location expressionLocation() {
    return expressionLocation;
  }

  @Override
  public ExpressionProvider expressionProvider() {
    return expressionProvider;
  }

  @Override
  public ReturnTypeProvider returnTypeProvider() {
    return returnTypeProvider;
  }
}
