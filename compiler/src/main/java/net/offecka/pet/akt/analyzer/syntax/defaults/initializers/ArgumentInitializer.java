package net.offecka.pet.akt.analyzer.syntax.defaults.initializers;

import net.offecka.pet.akt.analyzer.syntax.Argument;
import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.ArgumentDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.ArgumentEvent;
import net.offecka.pet.akt.analyzer.syntax.support.ToBeCommitted;
import net.offecka.pet.akt.events.EventPublisher;
import net.offecka.pet.akt.storage.MutableStorage;
import net.offecka.pet.akt.storage.exceptions.ExistedInChild;
import net.offecka.pet.akt.storage.exceptions.Exists;
import net.offecka.pet.akt.storage.exceptions.ExistsInStorage;
import net.offecka.pet.akt.storage.exceptions.NotFound;
import net.offecka.pet.akt.storage.exceptions.StorageException;
import net.offecka.pet.akt.storage.exceptions.UnexpectedType;

import java.util.List;

import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.NOTHING;
import static net.offecka.pet.akt.errors.ErrorContext.errorContext;
import static net.offecka.pet.akt.storage.FinderType.LOCAL;
import static net.offecka.pet.akt.storage.Visibility.PRIVATE;

public class ArgumentInitializer {
  private final MutableStorage storage;
  private final List<Argument> arguments;
  private final EventPublisher eventPublisher;

  public ArgumentInitializer(
    final MutableStorage storage,
    final List<Argument> arguments,
    final EventPublisher eventPublisher
  ) {
    this.storage = storage;
    this.arguments = arguments;
    this.eventPublisher = eventPublisher;
  }

  public ToBeCommitted init(final ArgumentDetails argumentDetails) {
    Type argumentType = null;
    try {
      argumentType = storage.find(LOCAL, argumentDetails.typeName()).item(Type.class);
    } catch (final NotFound | UnexpectedType ex) {
      try {
        argumentType = storage.find(LOCAL, NOTHING).item(Type.class);
      } catch (final StorageException ex2) {
        throw new IllegalStateException(ex2);
      }
    } catch (final StorageException ex) {
      throw new IllegalStateException(ex);
    }

    Argument argument = new Argument(argumentDetails.name(), argumentType);
    arguments.add(argument);
    return this.new ArgumentToBeCommitted(argument);
  }

  private final class ArgumentToBeCommitted implements ToBeCommitted {
    private final Argument argument;

    private ArgumentToBeCommitted(final Argument argument) {
      this.argument = argument;
    }

    @Override
    public void commit() {
      try {
        storage.put(PRIVATE, argument.name(), argument);
      } catch (final ExistsInStorage | ExistedInChild ex) {
        errorContext().addError(ex.name.location(), ex);
      } catch (final Exists ex) {
        throw new IllegalStateException(ex);
      }
      eventPublisher.publish(new ArgumentEvent(argument.name(), argument.type()));
    }
  }
}
