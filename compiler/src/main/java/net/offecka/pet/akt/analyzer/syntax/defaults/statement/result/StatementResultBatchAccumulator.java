package net.offecka.pet.akt.analyzer.syntax.defaults.statement.result;

import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.exceptions.CannotCast;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.exceptions.UnreachableStatement;
import net.offecka.pet.akt.errors.Location;
import net.offecka.pet.akt.storage.SearchingStorage;
import net.offecka.pet.akt.storage.Storage;

import java.util.List;

import static net.offecka.pet.akt.errors.ErrorContext.errorContext;

public class StatementResultBatchAccumulator {
  private BlockStatementResultBatch lastReachableBlockStatementResultBatch = null;
  public BlockStatementResultBatch lastReachableStatementResultBatch() {return lastReachableBlockStatementResultBatch;}

  private boolean hadUnreachableStatements = false;
  public boolean hadUnreachableStatements() {return hadUnreachableStatements;}

  private boolean hadReturnStatement = false;

  public void accumulate(final BlockStatementResultBatch newBlockStatementResultBatch) {
    if (hadReturnStatement && !hadUnreachableStatements) {
      hadUnreachableStatements = true;
      errorContext().addException(
        newBlockStatementResultBatch.startLocation(),
        new UnreachableStatement()
      );
    }

    if (lastReachableBlockStatementResultBatch == null || !hadReturnStatement) {
      lastReachableBlockStatementResultBatch = newBlockStatementResultBatch;
    }

    List<BlockStatementResult> blockStatementResults = newBlockStatementResultBatch.blockStatementResults();

    hadReturnStatement = hadReturnStatement
      || blockStatementResults.stream().anyMatch(rs -> rs.type != null);

    hadUnreachableStatements = hadUnreachableStatements
      || blockStatementResults.stream().anyMatch(bsr -> bsr.hasUnreachableStatements);
  }

  public void analyze(
    final Storage storage,
    final Type returnType,
    final Location functionEndLocation
  ) {
    SearchingStorage searchingStorage = new SearchingStorage(storage);

    if (lastReachableStatementResultBatch() == null
      || lastReachableStatementResultBatch().blockStatementResults().isEmpty()) {
      checkReturnTypeIsVoid(
        searchingStorage,
        returnType,
        functionEndLocation
      );
    } else {
      for (final BlockStatementResult blockStatementResult : lastReachableStatementResultBatch().blockStatementResults()) {
        if (blockStatementResult.type == null) {
          checkReturnTypeIsVoid(searchingStorage, returnType, functionEndLocation);
        } else {
          if (returnType == null) {
            Type voidType = searchingStorage.findVoidType();
            errorContext().addException(
              blockStatementResult.returnStatementExpressionLocation,
              new CannotCast(blockStatementResult.type, voidType)
            );
          } else {
            if (!blockStatementResult.type.isSubOf(returnType)) {
              errorContext().addException(
                blockStatementResult.returnStatementExpressionLocation,
                new CannotCast(blockStatementResult.type, returnType)
              );
            }
          }
        }
      }
    }
  }

  private void checkReturnTypeIsVoid(
    final SearchingStorage searchingStorage,
    final Type returnType,
    final Location location
  ) {
    if (returnType != null) {
      Type voidType = searchingStorage.findVoidType();
      if (returnType != voidType) {
        errorContext().addException(
          location,
          new CannotCast(voidType, returnType)
        );
      }
    }
  }
}
