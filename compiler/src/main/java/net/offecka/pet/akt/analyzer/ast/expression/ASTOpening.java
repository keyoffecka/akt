package net.offecka.pet.akt.analyzer.ast.expression;

import net.offecka.pet.akt.errors.Location;

public class ASTOpening implements ASTElement {
  public final Location location;

  public ASTOpening(final Location location) {
    this.location = location;
  }
}
