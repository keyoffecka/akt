package net.offecka.pet.akt.analyzer.syntax.defaults.events;

import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.events.Event;

public class AliasEvent implements Event {
  public final Name alias;
  public final Name name;

  public AliasEvent(final Name alias, final Name name) {
    this.alias = alias;
    this.name = name;
  }
}
