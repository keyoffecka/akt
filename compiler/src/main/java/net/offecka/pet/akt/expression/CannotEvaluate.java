package net.offecka.pet.akt.expression;

import net.offecka.pet.akt.errors.Location;

public class CannotEvaluate extends Exception {
  public final Location location;

  public CannotEvaluate(final String message, final Location location) {
    super(message);

    this.location = location;
  }
}
