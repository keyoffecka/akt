package net.offecka.pet.akt;

import net.offecka.pet.akt.analyzer.Analyzer;
import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.Namespace;
import net.offecka.pet.akt.errors.ErrorDescription;
import net.offecka.pet.akt.generator.javascript.JavaScriptFileGenerator;
import net.offecka.pet.akt.storage.Item;
import net.offecka.pet.akt.storage.exceptions.NotFound;
import net.offecka.pet.akt.storage.exceptions.StorageException;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Map.copyOf;
import static net.offecka.pet.akt.analyzer.syntax.NameFactory.NAME_FACTORY;
import static net.offecka.pet.akt.errors.ErrorContext.errorContext;
import static net.offecka.pet.akt.errors.Location.NO_LOCATION;
import static net.offecka.pet.akt.generator.javascript.JavaScriptFileGenerator.AKT_FILENAME_SUFFIX;
import static net.offecka.pet.akt.storage.FinderType.FOREIGN;

public final class Application {
  private final String sourceBaseDirPath;
  private final String destinationBaseDirPath;

  public static void main(final String[] args) throws Exception {
    String sourceBaseDirPath = Paths.get(args[0]).toFile().getCanonicalPath();
    String destinationBaseDirPath = Paths.get(args[1]).toFile().getCanonicalPath();

    Application application = new Application(sourceBaseDirPath, destinationBaseDirPath);
    application.run();
  }

  private Application(final String sourceBaseDirPath, final String destinationBaseDirPath) {
    this.sourceBaseDirPath = sourceBaseDirPath;
    this.destinationBaseDirPath = destinationBaseDirPath;
  }

  private void run() throws Exception {
    Visitor visitor = new Visitor(sourceBaseDirPath);

    Files.walkFileTree(Paths.get(sourceBaseDirPath), visitor);

    analyzeAndGenerate(sourceBaseDirPath, destinationBaseDirPath, visitor.filePaths());

    sortErrors().forEach(System.err::println);
  }

  private void analyzeAndGenerate(
    final String sourceBaseDirPath,
    final String destinationBaseDirPath,
    final Map<String, Name> filePaths
  ) throws StorageException, IOException {
    Analyzer analyzer = new Analyzer();
    Namespace root = null;
    while (!filePaths.isEmpty()) {
      Name name = filePaths.remove(filePaths.keySet().iterator().next());

      Item item = null;
      if (root != null) {
        try {
          item = root.find(FOREIGN, name).item;
        } catch (final NotFound ex) {
          //ignore
        }
      }
      if (item == null) {
        try (
          JavaScriptFileGenerator generator = new JavaScriptFileGenerator(
            sourceBaseDirPath, destinationBaseDirPath, name
          );
        ) {
          root = analyzer.compile(root, generator);
        }
      }
    }
  }

  private List<String> sortErrors() {
    return errorContext().errors().stream().sorted(
        (a, b) -> {
          if (a.location.line == null) {
            if (b.location.line == null) {
              return 0;
            } else {
              return -1;
            }
          } else {
            if (b.location.line == null) {
              return 1;
            } else {
              int c = a.location.line.compareTo(b.location.line);
              if (c == 0) {
                c = a.location.column.compareTo(b.location.column);
              }
              return c;
            }
          }
        }
      ).map(ErrorDescription::formattedMessage).collect(Collectors.toList());
  }
}

class Visitor extends SimpleFileVisitor<Path> {
  private final Map<String, Name> filePaths;
  private final String sourceBaseDirPath;

  Visitor(final String sourceBaseDirPath) {
    this.sourceBaseDirPath = sourceBaseDirPath;

    filePaths = new HashMap<>();
  }

  public Map<String, Name> filePaths() {
    return copyOf(filePaths);
  }

  @Override
  public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
    String filePath = file.toFile().getCanonicalPath();
    if (attrs.isRegularFile()
      && filePath.endsWith(AKT_FILENAME_SUFFIX)
      && filePath.startsWith(sourceBaseDirPath)) {

      filePath = filePath.substring(sourceBaseDirPath.length() + 1);

      Name name = null;
      Path path = Paths.get(filePath.substring(0, filePath.length() - AKT_FILENAME_SUFFIX.length()));
      while (path != null) {
        name = NAME_FACTORY.create(path.getFileName().toString(), name, NO_LOCATION);
        path = path.getParent();
      }

      filePaths.put(filePath, name);
    }
    return FileVisitResult.CONTINUE;
  }
}

