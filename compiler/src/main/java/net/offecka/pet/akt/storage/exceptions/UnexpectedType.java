package net.offecka.pet.akt.storage.exceptions;

import net.offecka.pet.akt.analyzer.syntax.Name;

public class UnexpectedType extends StorageException {
  public final Class<?> actualType;
  public final Class<?> expectedType;

  public UnexpectedType(final Name name, final Class<?> actualType, final Class<?> expectedType) {
    super("[" + name + "] of [" + actualType.getName() + "] should be of [" + expectedType.getName() + "] instead", name);

    this.actualType = actualType;
    this.expectedType = expectedType;
  }
}
