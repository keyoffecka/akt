package net.offecka.pet.akt.analyzer.ast.factories;

import net.offecka.pet.akt.analyzer.ast.ReturnNode;
import net.offecka.pet.akt.analyzer.ast.TermNode;
import net.offecka.pet.akt.analyzer.ast.VoidReturnNode;
import net.offecka.pet.akt.grammar.AktParser.TermContext;

import static net.offecka.pet.akt.analyzer.ast.factories.NodeFactories.NODE_FACTORIES;
import static net.offecka.pet.akt.errors.Location.after;
import static net.offecka.pet.akt.errors.Location.at;

public class TermNodeFactory {
  TermNodeFactory() {
  }

  public TermNode create(final TermContext termContext) {
    if (termContext.ret() != null) {
      return termContext.expression() == null
          ? new VoidReturnNode(at(termContext.ret().start), after(termContext.ret().stop))
          : new ReturnNode(
              NODE_FACTORIES.chainNodeFactory.create(termContext.expression()),
              at(termContext.ret().start)
          );
    } else {
      throw new UnsupportedOperationException();
    }
  }
}
