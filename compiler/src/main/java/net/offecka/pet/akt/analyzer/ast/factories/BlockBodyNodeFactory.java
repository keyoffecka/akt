package net.offecka.pet.akt.analyzer.ast.factories;

import net.offecka.pet.akt.analyzer.ast.BlockBodyNode;
import net.offecka.pet.akt.analyzer.ast.StepNode;
import net.offecka.pet.akt.grammar.AktParser.AssignmentContext;
import net.offecka.pet.akt.grammar.AktParser.BlockBodyContext;
import net.offecka.pet.akt.grammar.AktParser.BlockContext;
import net.offecka.pet.akt.grammar.AktParser.ExpressionContext;
import net.offecka.pet.akt.grammar.AktParser.TermContext;
import net.offecka.pet.akt.grammar.AktParser.VarDefContext;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.ArrayList;
import java.util.List;

import static net.offecka.pet.akt.analyzer.ast.factories.NodeFactories.NODE_FACTORIES;
import static net.offecka.pet.akt.errors.Location.at;

public class BlockBodyNodeFactory {
  BlockBodyNodeFactory() {
  }

  public BlockBodyNode create(final BlockBodyContext blockBody) {
    List<StepNode> steps = new ArrayList<>();

    for (final ParseTree child : blockBody.children) {
      if (child instanceof BlockContext) {
        steps.add(NODE_FACTORIES.blockNodeFactory.create((BlockContext) child));
      } else if (child instanceof VarDefContext) {
        steps.add(NODE_FACTORIES.variableNodeFactory.create((VarDefContext) child));
      } else if (child instanceof AssignmentContext) {
        steps.add(NODE_FACTORIES.assignmentNodeFactory.create((AssignmentContext) child));
      } else if (child instanceof ExpressionContext) {
        steps.add(NODE_FACTORIES.chainNodeFactory.create((ExpressionContext) child));
      } else if (child instanceof TermContext) {
        steps.add(NODE_FACTORIES.termNodeFactory.create((TermContext) child));
      } else if (child instanceof ParserRuleContext){
        throw new UnsupportedOperationException();
      }
    }

    return new BlockBodyNode(
      at(blockBody.LBRACE().getSymbol()),
      at(blockBody.RBRACE().getSymbol()),
      steps
    );
  }
}
