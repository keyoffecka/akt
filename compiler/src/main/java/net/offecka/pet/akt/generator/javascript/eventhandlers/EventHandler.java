package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.events.Event;

import java.io.IOException;

public interface EventHandler<T extends Event> {
  void handle(T event) throws IOException;
}
