package net.offecka.pet.akt.analyzer.ast;

import net.offecka.pet.akt.analyzer.ast.expression.ASTElement;
import net.offecka.pet.akt.errors.Location;

import java.util.List;

public class ExpressionNode implements StepNode {
  public final Location location;
  public final List<ASTElement> elements;

  public ExpressionNode(final Location location, final List<ASTElement> elements) {
    this.location = location;
    this.elements = elements;
  }

  public List<ASTElement> elements() {
    return elements;
  }
}
