package net.offecka.pet.akt.analyzer.syntax.defaults.details.statement;

import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.returntypeproviders.ReturnTypeProvider;

public interface WithReturnTypeProvider {
  ReturnTypeProvider returnTypeProvider();
}
