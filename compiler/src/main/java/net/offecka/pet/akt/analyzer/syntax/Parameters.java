package net.offecka.pet.akt.analyzer.syntax;

import net.offecka.pet.akt.analyzer.syntax.support.WithLocation;
import net.offecka.pet.akt.errors.Location;

import java.util.List;

import static java.util.List.copyOf;

public class Parameters implements WithLocation {
  public final List<Parameter> parameters;

  private final Location location;

  public Parameters(final List<Parameter> parameters, final Location location) {
    this.parameters = copyOf(parameters);
    this.location = location;
  }

  @Override
  public Location location() {
    return location;
  }
}
