package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.ElseIfEvent;

import java.io.IOException;
import java.io.Writer;

public class ElseIfEventHandler implements EventHandler<ElseIfEvent> {
  private final Writer writer;

  public ElseIfEventHandler(final Writer writer) {
    this.writer = writer;
  }

  @Override
  public void handle(final ElseIfEvent event) throws IOException {
    writer.write("else if (");
  }
}
