package net.offecka.pet.akt.generator.javascript.eventhandlers;

import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginParameterEvent;
import net.offecka.pet.akt.generator.context.ContextManager;
import net.offecka.pet.akt.generator.context.CountContext;

import java.io.IOException;
import java.io.Writer;

public class BeginParameterEventHandler implements EventHandler<BeginParameterEvent> {
  private final Writer writer;
  private final ContextManager contextManager;

  public BeginParameterEventHandler(final Writer writer, final ContextManager contextManager) {
    this.writer = writer;
    this.contextManager = contextManager;
  }

  @Override
  public void handle(final BeginParameterEvent event) throws IOException {
    CountContext context = contextManager.context();
    if (context.count() > 0) {
      writer.write(", ");
    }
    context.inc();
  }
}
