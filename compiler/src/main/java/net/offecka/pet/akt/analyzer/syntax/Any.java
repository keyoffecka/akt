package net.offecka.pet.akt.analyzer.syntax;

import net.offecka.pet.akt.analyzer.syntax.defaults.details.AliasDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.ArgumentDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.ConstantDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.FunctionDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.TypeDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.StatementDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.result.BlockStatementResultBatch;
import net.offecka.pet.akt.analyzer.syntax.support.ToBeCommitted;
import net.offecka.pet.akt.analyzer.syntax.support.WithName;
import net.offecka.pet.akt.analyzer.syntax.support.WithType;
import net.offecka.pet.akt.storage.FinderType;
import net.offecka.pet.akt.storage.Item;
import net.offecka.pet.akt.storage.Result;
import net.offecka.pet.akt.storage.Storage;

import static net.offecka.pet.akt.analyzer.syntax.Name.StandardNames.ANY;
import static net.offecka.pet.akt.storage.Visibility.PUBLIC;

public class Any implements Item, WithType, WithName, Type, Storage, Namespace, Function, Variable {
  private static final Any TYPE = new Any(ANY);

  private final Name name;

  public Any(final Name name) {
    if (name.child != null) {
      throw new IllegalArgumentException();
    }
    this.name = name;
  }

  @Override
  public Storage create() {
    throw new UnsupportedOperationException();
  }

  @Override
  public Type type() {
    return TYPE;
  }

  @Override
  public boolean isSubOf(final Type type) {
    return true;
  }

  @Override
  public ToBeCommitted addFunction(final FunctionDetails functionDetails) {
    throw new UnsupportedOperationException();
  }

  @Override
  public Name name() {
    return name;
  }

  @Override
  public Result find(final FinderType finderType, final Name name) {
    return new Result(this, PUBLIC, name.last(), new Any(name.last()));
  }

  @Override
  public void addAlias(final AliasDetails aliasDetails) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void addConstant(final ConstantDetails constantDetails) {
    throw new UnsupportedOperationException();
  }

  @Override
  public BlockStatementResultBatch addStatement(final StatementDetails statementDetails) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void addType(final TypeDetails typeDetails) {
    throw new UnsupportedOperationException();
  }

  @Override
  public ToBeCommitted addArgument(final ArgumentDetails argumentDetails) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void checkParameterIndex(final int paramIndex, final Name paramName) {
  }

  @Override
  public void checkParameterType(final int paramIndex, final Type paramType) {
  }

  @Override
  public void checkParameterCount(final int paramCount) {
  }

  @Override
  public boolean unmodifiable() {
    return false;
  }

  @Override
  public String description() {
    return toString();
  }
}
