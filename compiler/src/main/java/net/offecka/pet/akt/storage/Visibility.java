package net.offecka.pet.akt.storage;

public enum Visibility {
  PRIVATE,
  PUBLIC
}
