package net.offecka.pet.akt.analyzer.syntax.defaults.events;

import net.offecka.pet.akt.events.Event;
import net.offecka.pet.akt.expression.OperatorDescriptor;

public class OperatorEvent implements Event {
  public final OperatorDescriptor descriptor;

  public OperatorEvent(final OperatorDescriptor descriptor) {
    this.descriptor = descriptor;
  }
}
