package net.offecka.pet.akt.analyzer.syntax.defaults.statement.processors;

import net.offecka.pet.akt.analyzer.syntax.defaults.details.statement.StatementDetails;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginIfElseBlockEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndIfElseBlockEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndIfEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.result.BlockStatementResultBatch;
import net.offecka.pet.akt.errors.Location;
import net.offecka.pet.akt.events.Event;
import net.offecka.pet.akt.events.EventPublisher;
import net.offecka.pet.akt.expression.Expression;
import net.offecka.pet.akt.storage.MutableStorage;

import java.util.List;

public class IfElseBlockProcessor extends BlockProcessor {

  private final Expression expression;

  public IfElseBlockProcessor(
    final Location startLocation,
    final Location endLocation,
    final Expression expression,
    final List<StatementDetails> statementDetailsList,
    final BlockEventFactory blockEventFactory,
    final EventPublisher publisher
  ) {
    super(startLocation, endLocation, statementDetailsList, blockEventFactory, publisher);

    this.expression = expression;
  }

  @Override
  public BlockStatementResultBatch process(final MutableStorage storage) {
    if (expression != null) {
      publishEvents(expression.elements, storage);
      publisher.publish(new EndIfEvent());
    }

    return super.process(storage);
  }

  public static class IfElseBlockEventFactory extends LastIfElseBlockEventFactory {
    @Override
    public Event createEndBlockEvent() {
      return new EndIfElseBlockEvent();
    }
  }

  public static class LastIfElseBlockEventFactory extends BlockEventFactory {
    @Override
    public Event createBeginBlockEvent() {
      return new BeginIfElseBlockEvent();
    }
  }
}
