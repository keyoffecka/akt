package net.offecka.pet.akt.analyzer.syntax.defaults.statement.processors;

import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.analyzer.syntax.Type;
import net.offecka.pet.akt.analyzer.syntax.Variable;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.AssignmentEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.BeginStatementEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.events.EndStatementEvent;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.StatementType;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.exceptions.CannotCast;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.exceptions.CannotModify;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.operands.StatementOperand;
import net.offecka.pet.akt.analyzer.syntax.defaults.statement.result.BlockStatementResultBatch;
import net.offecka.pet.akt.errors.Location;
import net.offecka.pet.akt.events.EventPublisher;
import net.offecka.pet.akt.expression.CannotEvaluate;
import net.offecka.pet.akt.expression.Expression;
import net.offecka.pet.akt.storage.MutableStorage;
import net.offecka.pet.akt.storage.exceptions.NotFound;
import net.offecka.pet.akt.storage.exceptions.StorageException;
import net.offecka.pet.akt.storage.exceptions.UnexpectedType;

import static net.offecka.pet.akt.errors.ErrorContext.errorContext;
import static net.offecka.pet.akt.storage.FinderType.LOCAL;

public class AssignmentProcessor extends ExpressionStatementProcessor {
  protected final Name variableName;

  public AssignmentProcessor(
    final Location location,
    final Name variableName,
    final Expression expression,
    final EventPublisher publisher
  ) {
    super(location, expression, publisher);

    this.variableName = variableName;
  }

  @Override
  public BlockStatementResultBatch process(final MutableStorage storage) {
    publisher.publish(new BeginStatementEvent(StatementType.NORMAL));

    Type leftType = null;
    Type rightType = null;

    StatementOperand operand = null;
    try {
      operand = (StatementOperand) expression.evaluate();
    } catch (final CannotEvaluate ex) {
      errorContext().addError(ex.location, ex);
    }

    if (operand != null) {
      rightType = operand.type();
    }

    Variable oldVariable = null;
    try {
      oldVariable = storage.find(LOCAL, variableName).item(Variable.class);
    } catch (final UnexpectedType | NotFound ex) {
      errorContext().addError(ex.name.location(), ex);
    } catch (final StorageException ex) {
      throw new IllegalStateException(ex);
    }
    if (oldVariable != null) {
      if (oldVariable.unmodifiable()) {
        errorContext().addException(variableName.location(), new CannotModify(variableName));
      }
      leftType = oldVariable.type();
    }

    if (leftType != null && rightType != null) {
      if (!rightType.isSubOf(leftType)) {
        errorContext().addException(
          variableName.location(),
          new CannotCast(variableName, rightType, leftType)
        );
      }
    }

    publishChainEvents(storage, variableName, rightType);

    publisher.publish(new AssignmentEvent());

    publishEvents(expression.elements, storage);

    publisher.publish(new EndStatementEvent());

    return new BlockStatementResultBatch(location);
  }
}
