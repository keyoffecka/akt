package net.offecka.pet.akt.analyzer.ast.factories;

import net.offecka.pet.akt.analyzer.ast.TypeNode;
import net.offecka.pet.akt.errors.Location;
import net.offecka.pet.akt.grammar.AktParser.TypeContext;

import static net.offecka.pet.akt.analyzer.ast.factories.NodeFactories.NODE_FACTORIES;

public class TypeNodeFactory {
  TypeNodeFactory() {
  }

  public TypeNode create(final TypeContext type) {
    return new TypeNode(Location.at(type), NODE_FACTORIES.fqnNodeFactory.create(type.fqn()));
  }
}
