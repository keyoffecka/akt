package net.offecka.pet.akt.analyzer.collector;

import net.offecka.pet.akt.generator.Unit;
import net.offecka.pet.akt.grammar.AktLexer;
import net.offecka.pet.akt.grammar.AktParser;
import net.offecka.pet.akt.grammar.AktParser.NamespaceContext;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Token;

import java.io.IOException;
import java.io.Reader;

import static net.offecka.pet.akt.errors.ErrorContext.errorContext;
import static net.offecka.pet.akt.errors.Location.at;

public class UnitParser {
  private final ErrorListener errorListener;

  public UnitParser() {
    this.errorListener = new ErrorListener();
  }

  public NamespaceContext parse(final Unit unit) {
    NamespaceContext namespace = null;

    try (
      Reader reader = unit.reader();
    ) {
      AktLexer lexer = new AktLexer(CharStreams.fromReader(reader, unit.name().toString()));
      lexer.removeErrorListeners();
      lexer.addErrorListener(errorListener);

      AktParser parser = new AktParser(new CommonTokenStream(lexer));
      parser.removeErrorListeners();
      parser.addErrorListener(errorListener);
      namespace = parser.namespace();
      Token currentToken = parser.getCurrentToken();
      if (currentToken.getType() != Token.EOF) {
        errorContext().addError("Unrecognized content", at(currentToken));
      }
    } catch (final IOException ex) {
      errorContext().addError("Error while reading resource [" + unit.resourceName() + "]", unit.name().location());
    }

    return namespace;
  }
}
