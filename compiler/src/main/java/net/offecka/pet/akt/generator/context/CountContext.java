package net.offecka.pet.akt.generator.context;

public class CountContext extends Context {
  private int elementCount = 0;

  public CountContext(final Context parent) {
    super(parent);
  }

  public void inc() {
    elementCount += 1;
  }

  public int count() {
    return elementCount;
  }
}
