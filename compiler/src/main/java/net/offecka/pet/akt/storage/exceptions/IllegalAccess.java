package net.offecka.pet.akt.storage.exceptions;

import net.offecka.pet.akt.analyzer.syntax.Name;
import net.offecka.pet.akt.storage.FinderType;
import net.offecka.pet.akt.storage.Visibility;

public class IllegalAccess extends StorageException {
  public final Visibility visibility;
  public final FinderType finderType;

  public IllegalAccess(final Name name, final Visibility visibility, final FinderType finderType) {
    super(visibility + " [" + name + "] is not visible to " + finderType, name);

    this.visibility = visibility;
    this.finderType = finderType;
  }
}
