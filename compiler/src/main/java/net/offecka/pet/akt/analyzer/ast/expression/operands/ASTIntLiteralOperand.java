package net.offecka.pet.akt.analyzer.ast.expression.operands;

import net.offecka.pet.akt.errors.Location;

public class ASTIntLiteralOperand extends ASTNumLiteralOperand<String> {
  public ASTIntLiteralOperand(final Location location, final String value) {
    super(location, value);
  }
}
