package net.offecka.pet.akt.analyzer.syntax;

import net.offecka.pet.akt.analyzer.syntax.support.WithName;
import net.offecka.pet.akt.analyzer.syntax.support.WithType;
import net.offecka.pet.akt.storage.Item;

public class Argument implements WithName, WithType, Item {
  private final Name name;
  private final Type type;

  public Argument(final Name name, final Type type) {
    if (name.child != null) {
      throw new IllegalStateException();
    }

    this.name = name;
    this.type = type;
  }

  @Override
  public Name name() {
    return name;
  }

  @Override
  public Type type() {
    return type;
  }
}
