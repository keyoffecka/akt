package net.offecka.pet.akt.events;

public final class NoPublisher implements EventPublisher {
  public final static NoPublisher NO_PUBLISHER = new NoPublisher();

  public NoPublisher() {
  }

  @Override
  public void publish(final Event event) {
  }
}
