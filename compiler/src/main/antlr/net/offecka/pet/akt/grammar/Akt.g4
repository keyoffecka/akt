grammar Akt;

namespace:
  ( namespaceImport | function | namespaceVarDef | namespaceAssignment | namespaceCall | block | SEMICOLON )*
  ;

namespaceImport:
  IMPORT fqn (AS name)? SEMICOLON
  ;

namespaceVarDef:
  namespaceMemberScope? ( constant | variable) name ( COLON type )? assign expression SEMICOLON
  ;

namespaceAssignment:
  assignable assign expression SEMICOLON
  ;

namespaceCall:
  expression SEMICOLON
  ;

function:
  namespaceMemberScope? FUNCTION name LPAREN arguments? RPAREN COLON type blockBody
  ;

namespaceMemberScope:
  pub | shared
  ;

arguments:
  argument ( COMMA arguments )?
  ;

argument:
  name COLON type ( ASSIGN expression )?
  ;

params:
  param ( COMMA params )?
  ;

param:
  ( name ASSIGN )? expression
  ;

expression:
  literal
  | lambda
  | chain
  | unOp expression
  | expression binOp expression
  | expression triIf expression triElse expression
  | lParen expression rParen
  ;

lParen:
  LPAREN
  ;

rParen:
  RPAREN
  ;

binOp:
  ( plus | minus | mult | div | mod | bitAnd | bitOr | bitXor | equalsTo | is | notEqualsTo | isNot | and | or )
  ;

unOp:
  ( inv | plus | minus )
  ;

block:
  ( ifBlockBody | blockBody )
  ;

ifBlockBody:
  (IF LPAREN expression RPAREN blockBody)
  (elifBlockBody*)
  (elseBlockBody?)
  ;

elifBlockBody:
  ELIF LPAREN expression RPAREN blockBody
  ;

elseBlockBody:
  ELSE blockBody
  ;

blockBody:
  LBRACE ( block | ( ( term | varDef | assignment | expression )? SEMICOLON ) )* RBRACE
  ;

varDef:
  ( constant | variable ) name ( COLON type )? assign expression
  ;

assignment:
  assignable assign expression
  ;

literal:
  integer | real | string | bool
  ;

bool:
  tr | fl
  ;

lambdaDef:
  LPAREN lambdaParam? RPAREN LAMBDA name
  ;

lambda:
  LPAREN lambdaParam? RPAREN LAMBDA blockBody
  ;

lambdaParam:
  name (COMMA name)*
  ;

term:
  ( ( ret expression? ) | brk | cont )
  ;

type:
  typeName | lambdaDef
  ;

typeName:
  name
  ;

fqn:
  name (DOT fqn)?
  ;

chain:
  (var | call) (DOT chain)?
  ;

assignable:
  (chain DOT)? var
  ;

var:
  name
  ;

call:
  name LPAREN params? RPAREN
  ;

ret: RET ;
brk: BRK ;
cont: CONT ;
inv: INV ;
plus: PLUS ;
minus: MINUS ;
div: DIV ;
mod: MOD ;
mult: MULT ;
or: OR ;
and: AND ;
triIf: QUESTION ;
triElse: COLON ;
equalsTo: EQUALS_TO ;
notEqualsTo: NOT_EQUALS_TO ;
is: IS ;
isNot: IS_NOT ;
bitOr: BIT_OR ;
bitAnd: BIT_AND ;
bitXor: BIT_XOR ;
assign: ASSIGN ;
pub: PUB ;
shared: SHARED ;
constant: CONST ;
variable: VARIABLE ;
name: NAME ;
integer: INTEGER ;
real: REAL ;
string: STRING ;
tr: TRUE;
fl: FALSE;

IMPORT: 'import';
FUNCTION: 'function';
CLASS: 'class';
PUB: 'public';
PROT: 'protected';
SHARED: 'shared';
LOCAL: 'local';
PROPERTY: 'property';
GETTER: 'get';
SETTER: 'set';
CONSTRUCTOR: 'constructor';
IF: 'if';
ELIF: 'elif';
ELSE: 'else';
FOR: 'for';
IN: 'in';
RET: 'return';
CONT: 'continue';
BRK: 'break';
CONST: 'const';
VARIABLE: 'var';
IMMUTABLE: 'immutable';
CLOSED: 'closed';
TRUE: 'TRUE';
FALSE: 'FALSE';
THAT: 'this';
UBER: 'super';
LAMBDA: '->';
MULT: '*';
DIV: '/';
MOD: '%';
PLUS: '+';
MINUS: '-';
QUESTION: '?';
EQUALS_TO: '===';
NOT_EQUALS_TO: '!==';
IS: '==';
IS_NOT: '!=';
INV: '!';
ASSIGN: '=';
AND: '&&';
OR: '||';
BIT_OR: '|';
BIT_AND: '&';
BIT_XOR: '^';
LPAREN: '(';
RPAREN: ')';
LBRACE: '{';
RBRACE: '}';
COLON: ':';
SEMICOLON: ';';
DOT: '.';
COMMA: ',';
AS: 'as';

REAL: [0-9]+ ( ( '.' [0-9]+ ( [eE] [+-]? [0-9]+ )? ) | ( [eE] [+-]? [0-9]+ ) ) ;
INTEGER: [0-9]+ ( '.' [0-9]+ )? ;
STRING: '"' ( '\\' ["\\] | ~["\\\r\n] )* '"' ;
NAME: [a-zA-Z_][a-zA-Z0-9_]* ;
WS : [ \t\r\n]+ -> skip ;