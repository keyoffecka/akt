import express from 'express';
import compression from 'compression';
import ejs from 'ejs';

import {port} from './cli';
import {commonHeadersFilter, notFoundFilter} from './filters';

const app = express();
app.set("etag", false);
app.engine("html", ejs.__express);

app.use(compression());
app.use(express.json());
app.use(commonHeadersFilter);
app.use("/", notFoundFilter);

app.listen(port);
