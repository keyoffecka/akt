import path from 'path';
import yargs from 'yargs';

const DEFAULT_PORT = 3000;
const DEFAULT_DIST_DIR_PATH = "dist";

const args = yargs
  .option("port", {
    demandOption: false,
    type: "integer",
    requiresArg: true,
    desc: "port number"
  })
  .option("dist", {
    demandOption: false,
    type: "array",
    requiresArg: true,
    desc: "path to the static assets to server"
  })
  .version(false)
  .showHidden(false)
  .help("help")
  .strict(true)
  .parse();

export const distDirPaths = args.dist
    ? [...new Set(args.dist)].map(d => path.resolve(d))
    : [path.resolve(DEFAULT_DIST_DIR_PATH)];

export const port = args.port ? args.port : DEFAULT_PORT;