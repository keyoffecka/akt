import path from 'path';
import fs from 'fs';
import express from 'express';
import status from 'http-status';
import {headers} from 'http-constants';

import {distDirPaths} from './cli';

const distDirPathMap = new Map();
for(const i in distDirPaths) {
  const p = distDirPaths[i];
  distDirPathMap.set(p, express.static(p, {etag: false}));
}

export function notFoundFilter(req, res, next) {
  const distDirPath = distDirPaths.find(d => fs.existsSync(path.join(d, req.path)));
  if (distDirPath) {
    const st = distDirPathMap.get(distDirPath);
    st(req, res, next);
  } else {
    res.render(path.join(distDirPaths[0], "index.html"), {}, (err, html) => {
      const s = req.path.match(/^\/+$/) == null
          ? status.NOT_FOUND
          : status.OK;

      res.status(s).send(html);
    });
  }
}

export function commonHeadersFilter(req, resp, next) {
  resp.set(headers.response.CONNECTION, "keep-alive");

  next();
}
